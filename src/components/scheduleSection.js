import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'
import { Link } from 'gatsby'
import ReactMarkdown from 'react-markdown'
import { Container, Row, Col, CardDeck, Card, Collapse } from 'react-bootstrap'
export default function ScheduleSection({
  schedule,
  setSchedule,
  event,
  speakers,
}) {
  const [currentScheduleTab, setCurrentScheduleTab] = React.useState(2)
  const [scheduleQuery, setScheduleQuery] = React.useState('')
  const [openDesc1, setOpenDesc1] = React.useState(false)
  console.log('ssss', schedule)
  const getTags = (slot) => {
    //console.log('slot', slot)
    if (slot.tags?.indexOf('track1') > -1) {
      return '[Track 1]'
    }
    if (slot.tags?.indexOf('track2') > -1) {
      return '[Track 2]'
    }
    return ''
  }
  const getTagsTrack = (slot) => {
    //console.log('slot', slot)
    if (slot.tags?.indexOf('track1') > -1) {
      return 'A'
    }
    if (slot.tags?.indexOf('track2') > -1) {
      return 'B'
    }
    return ''
  }

  const slots = schedule[0].slots.sort((a, b) =>
    a.id + getTagsTrack(a) < b.id + getTagsTrack(a) ? 1 : -1
  )

  const getDayActivity = (i) => {
    switch (i) {
      case 0:
        return 'NETWORKING EVENTS'
        break
      case 1:
        return 'WORKSHOPS AND HACKATHON'
      case 2:
        return 'CONFERENCE'
      case 3:
        'NETWORKING EVENTS'
      default:
        break
    }
  }
  const getDayActivityDate = (i) => {
    switch (i) {
      case 0:
        return '29'
        break
      case 1:
        return '30'
      case 2:
        return '31'
      case 2:
        return '1st'
      default:
        break
    }
  }
  const data = useStaticQuery(graphql`
    {
      scedual: file(relativePath: { eq: "scedual.png" }) {
        childImageSharp {
          fixed(width: 80, height: 80) {
            ...GatsbyImageSharpFixed_withWebp
          }
        }
      }
    }
  `)

  React.useEffect(() => {
    if (typeof window !== 'undefined') {
      setTimeout(() => {
        const hash = document.location.hash
        const slot = hash.split('#slot-')
        if (slot && slot[1]) {
          let dayd = document.getElementById(slot[1]).offsetTop
          let scrolldiv = document.getElementById('schedule-scroll')
          scrolldiv.scrollIntoView()
          scrolldiv.scrollTop = dayd - 200
        } else {
          let dayd = document.getElementById('day-3').offsetTop
          let scrolldiv = document.getElementById('schedule-scroll')
          scrolldiv.scrollTop = dayd - 120
        }
      }, 1000)
    }
  }, [])

  return (
    <section className="schedule" id="schedule">
      <div className="container">
        <div className="headings">
          <Img fixed={data.scedual.childImageSharp.fixed} />
          <h2>2022 Edition Schedule</h2>
          <h3
            style={{
              marginTop: '30px',
              textDecoration: 'underline dashed',
              textUnderlineOffset: '5px',
            }}
            id="day1"
          >
            March 26th
          </h3>
          <h4 style={{ margin: '30px' }}>ETHDubai Hackathon</h4>
          <p>
            The hackathon starts with more than $50k&nbsp;in prize from&nbsp;
            <a
              data-user-name="1inch"
              href="https://twitter.com/1inch/"
              rel="user"
              target="_blank"
            >
              @1inch
            </a>
            ,{' '}
            <a
              data-user-name="graphprotocol"
              href="https://twitter.com/graphprotocol/"
              rel="user"
              target="_blank"
            >
              @graphprotocol
            </a>{' '}
            ,{' '}
            <a
              data-user-name="SushiSwap"
              href="https://twitter.com/SushiSwap/"
              rel="user"
              target="_blank"
            >
              @SushiSwap
            </a>{' '}
            ,{' '}
            <a
              data-user-name="0xPolygon"
              href="https://twitter.com/0xPolygon/"
              rel="user"
              target="_blank"
            >
              @0xPolygon
            </a>{' '}
            ,{' '}
            <a
              data-user-name="Nexo"
              href="https://twitter.com/Nexo/"
              rel="user"
              target="_blank"
            >
              @Nexo
            </a>{' '}
            ,{' '}
            <a
              data-user-name="MoralisWeb3"
              href="https://twitter.com/MoralisWeb3/"
              rel="user"
              target="_blank"
            >
              @MoralisWeb3
            </a>{' '}
            and{' '}
            <a
              data-user-name="SpheronHQ"
              href="https://twitter.com/SpheronHQ/"
              rel="user"
              target="_blank"
            >
              @SpheronHQ
            </a>
            &nbsp;❤️
            <br />
            <br />
            <strong>
              Here is the{' '}
              <a
                href="https://docs.google.com/document/d/1JdO02UgeUcI0tbk4nvUsswgCJCj-MmM5HADo0PeTsgA/edit?usp=sharing"
                target="_blank"
              >
                detailed list of bounties from our generous sponsors
              </a>
              .
            </strong>
            <br />
            <br />
            <strong>
              <a href="https://forms.gle/PxZJZo1uYs8idtb37" target="_blank">
                You need to register your team here
              </a>
              &nbsp;by March 30th or as soon as possible so we can start
              reviewing early.
            </strong>
            <br />
            <br />
            It is ok to participate as a team, prize will be split between each
            participant.
            <br />
            <br />
            The hackathon&nbsp;starts remotely, you can participate remotely
            even if you do not have a ticket. However, without a combo ticket
            you will not be able to hack at the venue at all.
            <br />
            <br />
            Winners will be able to present at the venue on the 31st at 7pm, if
            you don't have a ticket to the conference you can send us a 2 minute
            video presentation we will show. The hackathon ends on March 31st at
            1pm UTC+4.
            <br />
            <br />
            Prize will be deposited by sponsors in our permissionless hackathon
            vault &nbsp;<a href="https://hackathon.ethdubaiconf.org/">here</a>.
            Contract by&nbsp;
            <a href="https://twitter.com/patcito">@patcito</a>
            &nbsp;and the amazing&nbsp;
            <a href="https://twitter.com/financefeeder">@financefeeder</a>
            &nbsp;with contribs by the great&nbsp;
            <a href="https://twitter.com/qdqd___">@qdqd___</a>
            (make sure to follow them!)
            <br />
          </p>
          <h3
            style={{
              marginTop: '30px',
              textDecoration: 'underline dashed',
              textUnderlineOffset: '5px',
            }}
            id="day1"
          >
            March 29th
          </h3>
          <h4 style={{ margin: '30px' }}>
            Brunch (11am-1:30pm): "Gnosis Chain: Accelerating Ethereum through
            the Gnosisverse!"{' '}
            <a
              href="https://www.eventbrite.com/e/gnosis-chain-accelerating-ethereum-through-the-gnosisverse-tickets-301516673487?aff=ebdssbdestsearch"
              target="_blank"
            >
              Register here
            </a>{' '}
          </h4>
          <h4 style={{ margin: '30px' }}>
            Special Yacht Meet and Greet Party Sponsored by{' '}
            <a href="https://syscoin.org/" target="_blank">
              Syscoin
            </a>{' '}
            and <a href="https://www.unore.io/">Uno Re</a> (2:30pm-6pm)
          </h4>
          <h4 style={{ margin: '30px' }}>
            Opening Bar Night{' '}
            <a href="https://gton.capital/" target="_blank">
              GTON Capital Party
            </a>{' '}
            (7pm-11:30pm)
          </h4>
          <hr />
          <h3
            style={{
              textDecoration: 'underline dashed',
              textUnderlineOffset: '5px',
            }}
            id="day2"
          >
            March 30th
          </h3>
          <h4 style={{ margin: '30px' }}>
            Learn from the best to level-up your web3 skills with our awesome
            workshops (9:30am - 4:30pm | Note: some workshops start at 9:30am
            and others at 1:30pm).
          </h4>
          <h4 style={{ margin: '30px' }}>
            Conference Pre-Party Sponsored by{' '}
            <a href="https://ezil.me/" target={'_blank'}>
              Ezil
            </a>{' '}
            and{' '}
            <a href="https://octusbridge.io/" target={'_blank'}>
              Octus Bridge
            </a>{' '}
            (6pm-11pm) .
          </h4>

          <Container>
            <Row>
              <Col>
                <CardDeck className="carddeskMobile">
                  <Card>
                    <Card.Img
                      style={{
                        height: '80px',
                        margin: 'auto',
                        width: '80px',
                      }}
                      src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB2ZXJzaW9uPSIxLjEiCiAgIGlkPSJzdmc1MjkwNSIKICAgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIKICAgd2lkdGg9IjI5MzMuMzMzMyIKICAgaGVpZ2h0PSIyOTMzLjMzMzMiCiAgIHZpZXdCb3g9IjAgMCAyOTMzLjMzMzMgMjkzMy4zMzMzIgogICBzb2RpcG9kaTpkb2NuYW1lPSIxaW5jaF9sb2dvX2Zvcl9jb2xvci5zdmciCiAgIGlua3NjYXBlOnZlcnNpb249IjEuMS4yICgxOjEuMSsyMDIyMDIwNTA5NTArMGEwMGNmNTMzOSkiCiAgIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIgogICB4bWxuczpzb2RpcG9kaT0iaHR0cDovL3NvZGlwb2RpLnNvdXJjZWZvcmdlLm5ldC9EVEQvc29kaXBvZGktMC5kdGQiCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnMKICAgICBpZD0iZGVmczUyOTA5Ij48Y2xpcFBhdGgKICAgICAgIGNsaXBQYXRoVW5pdHM9InVzZXJTcGFjZU9uVXNlIgogICAgICAgaWQ9ImNsaXBQYXRoNTI5MTkiPjxwYXRoCiAgICAgICAgIGQ9Ik0gMCwyMjAwIEggMjIwMCBWIDAgSCAwIFoiCiAgICAgICAgIGlkPSJwYXRoNTI5MTciIC8+PC9jbGlwUGF0aD48L2RlZnM+PHNvZGlwb2RpOm5hbWVkdmlldwogICAgIGlkPSJuYW1lZHZpZXc1MjkwNyIKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMS4wIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAuMCIKICAgICBpbmtzY2FwZTpwYWdlY2hlY2tlcmJvYXJkPSIwIgogICAgIHNob3dncmlkPSJmYWxzZSIKICAgICBpbmtzY2FwZTp6b29tPSIwLjI5NjU5MDkyIgogICAgIGlua3NjYXBlOmN4PSIxNDY2LjY2NjYiCiAgICAgaW5rc2NhcGU6Y3k9IjE0NjguMzUyNCIKICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjE4NDgiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iMTAxNiIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iNzIiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9IjI3IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0iZzUyOTExIiAvPjxnCiAgICAgaWQ9Imc1MjkxMSIKICAgICBpbmtzY2FwZTpncm91cG1vZGU9ImxheWVyIgogICAgIGlua3NjYXBlOmxhYmVsPSIxaW5jaF9sb2dvX2Zvcl9jb2xvciIKICAgICB0cmFuc2Zvcm09Im1hdHJpeCgxLjMzMzMzMzMsMCwwLC0xLjMzMzMzMzMsMCwyOTMzLjMzMzMpIj48ZwogICAgICAgaWQ9Imc1MjkxMyI+PGcKICAgICAgICAgaWQ9Imc1MjkxNSIKICAgICAgICAgY2xpcC1wYXRoPSJ1cmwoI2NsaXBQYXRoNTI5MTkpIj48ZwogICAgICAgICAgIGlkPSJnNTI5MjEiCiAgICAgICAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoOTM3LjUxMzcsMjEyOS42ODYpIj48cGF0aAogICAgICAgICAgICAgZD0ibSAwLDAgaCAtMC42MzIgYyAtMzcuMTk5LC0wLjA2NCAtMTExLjEyNCwtNS45ODQgLTE4OC4wMTQsLTQ0LjkxMSAtNDMuOTI2LC0yMi4yMzcgLTgyLjY5NywtNTIuMjI2IC0xMTUuMjM3LC04OS4xMzMgLTExLjQxNSwtMTIuOTQ3IC0yMi4wNzcsLTI2Ljc2NyAtMzEuOTcsLTQxLjQyOCAtNC41MSwxNi4yMTMgLTcuMjUzLDI2LjkxOSAtNy4zMywyNy4yMTggLTQuMjk3LDE2Ljg0NCAtMTcuMDM3LDMwLjI1MSAtMzMuNjQxLDM1LjM5OSAtNC44NjIsMS41MDcgLTkuODUzLDIuMjQ0IC0xNC44MDYsMi4yNDQgLTExLjk2NCwwLjAwMSAtMjMuNzE1LC00LjI5NSAtMzIuOTYsLTEyLjM5OSAtNC4zNTUsLTMuODE3IC00My40MTgsLTM5LjEyMSAtNzMuNjE4LC0xMDIuNDQzIC0xNy45ODIsLTM3LjcwNSAtMjkuMTIsLTc3LjY5NyAtMzMuMTA0LC0xMTguODY2IC0yLjAzNywtMjEuMDU2IC0yLjE5MywtNDIuMzg1IC0wLjQ3NywtNjMuODg4IC01NC45MTksMTkuODI1IC0xMTUuMTY5LDQxLjM3OSAtMTcyLjI4MSw2MS42MiAtMTM4LjM1Nyw0OS4wMzIgLTE0NC4zMzEsNTAuMjI4IC0xNDguNjkzLDUxLjEgLTAuOTA4LDAuMTgyIC0xLjgyMSwwLjMzOCAtMi43MzgsMC40NjkgLTMuOTYzLDAuNTY2IC03LjkwNSwwLjg1MyAtMTEuNzE0LDAuODUzIC0yMS45NTYsMC4wMDEgLTQxLjU1OCwtOS43NDMgLTUyLjQzMywtMjYuMDY1IC0xMy42NTEsLTIwLjQ3NiAtMTQuMzI1LC01NC4yMTcgMTYuNDc0LC03OS44ODQgMC4xMTYsLTAuMDk3IDAuMjMzLC0wLjE5MyAwLjM1LC0wLjI4OSAxMS4yODEsLTkuMjI5IDExMi44NjYsLTg0LjQ3NyAyMDkuMDMxLC0xNTQuODM3IDYyLjYxNywtNDUuODE0IDE2NS40MDIsLTEyMC42MzYgMjI0LjYzNiwtMTYxLjcyIC05LjkzMywtNDcuMzc4IC04Ljg0MywtODUuMzQ2IDEzLjgwMywtMTIzLjg0NCAwLjEyNywtMC4yMTcgMC4yNTYsLTAuNDMzIDAuMzg3LC0wLjY0OCAxOC41NzUsLTMwLjUxNSAxNy40NDYsLTQwLjExMiAxNS4zOTcsLTU3LjUzNiAtMC4yOTIsLTIuNDc3IC0wLjc4NCwtNi41ODMgLTEuNDQ0LC0xMi4wNCBsIC0wLjM4MSwtMy4xNDcgLTAuODU1LC03LjAzIC0wLjQwOSwtMy4zNTUgLTAuMDY2LC0wLjUzNyAtMC4xMTIsLTAuOTIzIC0wLjEzNiwtMS4xMDMgYyAtMTUuMTY1LC0xMjQuMTMyIC0zMi42NDksLTI1OS4zNzggLTM2LjkyLC0yNzkuMzAxIC0yLjUwMiwtOC4zNzYgLTE2Ljg5NywtMzUuNTk4IC0yNi40NDksLTUzLjY1OSAtMTkuOTc5LC0zNy43ODEgLTQwLjYzOCwtNzYuODQ1IC00Ny42NzEsLTEwNi4yMjYgLTAuOTQ1LC0zLjk1MiAtMS42NjcsLTcuODAyIC0yLjE0NCwtMTEuNDQxIC0wLjM4NiwtMi45NDUgLTAuNjI1LC01Ljg1NiAtMC43MTIsLTguNjUzIC0wLjEwNiwtMy40MjYgMC4wMTQsLTYuODEgMC4zNTUsLTEwLjA2IDIuNDc4LC0yMy41MzggMTYuOTU2LC03Mi40MDEgMzMuNjgsLTExMy42NjUgMTAuMjQ0LC0yNS4yNzggMjAuNzk5LC00Ni43MDIgMzEuMzcxLC02My42NzUgMTYuOTQ3LC0yNy4yMDggMzQuMTQsLTQzLjUyIDU0LjEwOSwtNTEuMzM0IDAuNTI1LC0wLjIwNiAxLjA1NCwtMC40MDMgMS41ODQsLTAuNTkgMTYuODA2LC01LjkzIDEwMy43OTYsLTM1LjUwOSAxNjYuMTY5LC0zNS41MDkgMjIuMjI3LDAgNTIuNzQ3LDQuMTIzIDczLjM3OSwyMy40OTMgMTEuMjI3LDkuODg4IDE4LjU1NSwxNS44MjYgMjEuODIsMTcuNjgyIDAuNTM2LDAuMDM3IDEuNDE5LDAuMDc1IDIuNzcsMC4wNzUgaCA1LjE1NiBjIDEuODQxLDAgNS4zMzEsLTAuMjkzIDguNzA3LC0wLjU3NyA2LjA2NiwtMC41MDkgMTMuNjE1LC0xLjE0MyAyMi4yMzEsLTEuMTQzIDIxLjAxMywwIDQwLjcxMSwyLjQ3OCA1OC41NDYsNy4zNjUgMjUuNDczLDYuOTggNDcuMzI0LDE4Ljg1NyA2NC45NDQsMzUuMyAwLjQyLDAuMzkzIDAuODMzLDAuNzkyIDEuMjQsMS4xOTkgMTYuNjE5LDE2LjYxOCAzOS41NjIsMzYuNjM0IDYxLjc1LDU1Ljk5MSAyNy41NSwyNC4wMzQgNTEuMzQzLDQ0Ljc5MiA2NC42MDYsNjAuMjY1IDAuNDM5LDAuNTExIDAuODY3LDEuMDMyIDEuMjg1LDEuNTYxIDE3LjA1MywyMS42MDYgMzAuMDE5LDQ3LjIxOSAzNy40OTUsNzQuMDczIDcuODg4LDI4LjMzNSA5LjcyOCw1Ny4xNDkgNS40Nyw4NS42NjEgLTIuNDMyLDIxLjQ0NiAxMC41MTQsNDQuNTcxIDMzLjY3OCw3Ny4wNTUgMTYuNzgxLDIyLjA3NyA0MC44OTYsNTMuMzIyIDYwLjIyOSw3OC4yOTQgODYuMjI4LC04MC43NiAxMzQuODIxLC0xODkuNDU3IDEzNC44MjEsLTMwNC43MzQgMCwtNTguMzM3IC0xMi4yOTMsLTExNC45IC0zNi41MzYsLTE2OC4xMTYgLTIzLjU0MiwtNTEuNjc3IC01Ny4zMzEsLTk4LjE0MiAtMTAwLjQyOSwtMTM4LjEwNSAtNDMuMzU4LC00MC4yMDQgLTkzLjkzMSwtNzEuNzg4IC0xNTAuMzE2LC05My44NzcgLTU4LjU3NywtMjIuOTQ3IC0xMjAuODkzLC0zNC41ODIgLTE4NS4yMjEsLTM0LjU4MiAtNDAuNTcsMCAtNzkuNDc5LDQuNjA4IC0xMTguOTQ4LDE0LjA4NyAtMy45MTYsMC45NCAtNy44NDQsMS4zOTMgLTExLjcwOSwxLjM5MyAtMjEuNjkyLDEwZS00IC00MS41NDMsLTE0LjIwMiAtNDcuOTMsLTM1Ljg4IC03LjUyNSwtMjUuNTQzIDYuMjg5LC01Mi41MDEgMzEuNDE3LC02MS4zMTMgMTIyLjQxOSwtNDIuOTMgMjQwLjc0OSwtNjQuNjk3IDM1MS43MDIsLTY0LjY5NyA0Ny4yNDEsMCA5My4zOTgsMy45NjcgMTM3LjE5LDExLjc5IDUuMzU3LDAuOTU3IDEwLjY4MywxLjk3MyAxNS45NzIsMy4wNDUgMC40OSwtNC4wODggMS40OTEsLTguMTU4IDMuMDI1LC0xMi4xMTggNy40NTgsLTE5LjI0OSAyNS45OCwtMzEuOTM3IDQ2LjYyMywtMzEuOTM3IGggMS43MiBjIDIuMzA0LDAgNC42MDQsMC4xNTkgNi44ODYsMC40NzcgNzguOTY5LDEwLjk3OSAxNTUuMTQ4LDM1LjMyMSAyMjYuNDE4LDcyLjM0NyA1Ni43NTgsMjkuNDg4IDExMC40ODMsNjcuMDIxIDE1OS42ODQsMTExLjU1NiAzLjkyNSwzLjU1MiA3Ljc1Nyw3LjA5MiAxMS40OTgsMTAuNjE0IDMuNzE3LC04LjUxNiA5LjgxNSwtMTYuMDI0IDE3Ljg5MywtMjEuNDY2IDguNDU5LC01LjY5NiAxOC4yMDIsLTguNTI4IDI3LjkyOCwtOC41MjggMTAuNTg2LDAgMjEuMTU0LDMuMzU0IDMwLjAyOSwxMC4wMiAxMDAuMTEyLDc1LjE4NSAxODIuMTIzLDE2MC45OTQgMjQzLjc1NSwyNTUuMDQzIDUwLjI4Myw3Ni43MzMgODcuMjY0LDE1OS4yMzEgMTA5LjkxNSwyNDUuMjAyIDE0LjE0NSw1My42OTEgMjEuMTcsMTAyLjg3IDI0LjEyOSwxNDUuMTY3IDQuODI5LC0xLjUxOCA5Ljg4NywtMi4zMDUgMTUsLTIuMzA1IDQuODEsMCA5LjY2NywwLjY5NSAxNC40MjgsMi4xMzIgMTkuNDE2LDUuODUzIDMzLjM3MywyMi44NTUgMzUuMzMzLDQzLjAzOSA4LjU1Myw4OC4wOTkgNy43NjMsMTczLjc1NiAtMi4zNDgsMjU0LjU5NSAtOS4yNSw3My45NTEgLTI2LjQ5NywxNDUuNDkzIC01MS4yNjQsMjEyLjY0IC00MS44MDcsMTEzLjM0OCAtMTA1LjQyNiwyMTYuMTgzIC0xODkuMDg4LDMwNS42NSAtNDAuOTc1LDQzLjgyIC04Mi42NTIsODAuMDQ5IC0xMjEuMzYyLDEwOS40NzEgMTEuMjA5LDUuMTUxIDIwLjQyOCwxNC40MDEgMjUuMzcsMjYuNDAzIDguNTE4LDIwLjY4NyAyLjI1OCw0NC41MTQgLTE1LjMyOCw1OC4zNDIgLTk3LjY0LDc2Ljc3OCAtMjAwLjQxOCwxMzMuODM3IC0zMDUuNDgxLDE2OS41OTIgLTg4Ljk3NywzMC4yODEgLTE4MC40ODYsNDUuNjM1IC0yNzEuOTg1LDQ1LjYzNSAtOTguNjYxLDAgLTE4My41MTEsLTE3LjU3NiAtMjQ5LjEwMiwtMzguMTg5IDAuMzE2LDguMTY2IC0xLjM2NiwxNi40MTYgLTUuMDg4LDIzLjk5IEMgNTQuNjU1LC0xMi45OSAzOS40ODIsLTIuNDk5IDIyLjIxNCwtMC44MzkgMjAuNzU2LC0wLjY5OSAxMi44MzIsMCAwLDAgbSAwLC01MCBjIDExLjA4NCwwIDE3LjQzMSwtMC42MSAxNy40MzEsLTAuNjEgbCAtNjcuMDMyLC0xMDYuNTYyIGMgOC41OTQsNS4xNTYgMTUyLjk2Nyw5Mi44MTIgMzY2LjA5Nyw5Mi44MTIgMTU0LjY4LDAgMzQ1LjQ3LC00Ni40MDYgNTQ2LjU2LC0yMDQuNTMxIC04OS4zOCwxMC4zMTIgLTE3NS4zMSwxMy43NSAtMTc1LjMxLDEzLjc1IDAsMCA1NzkuMjIsLTIyOC41OTQgNTEwLjQ3LC05MzYuNzE1IC0yMi4zNSwyNy41IC02NS4zMiw3OS4wNiAtNjguNzUsODQuMjEgMS43MSwtMTMuNzUgNzMuOSwtNDE1LjkzIC0zNTQuMDcsLTczNy4zNCAxNS40Nyw1OC40NCAyNy41LDE0NC4zOCAyNy41LDE0NC4zOCAwLDAgLTE1Ni40LC0yNjQuNjkgLTQ3Ny44MSwtMzA5LjM4IGggLTEuNzIgYyA1OC40NCw1My4yOCA5NC41Myw5OS42OSA5NC41Myw5OS42OSAwLDAgLTEwMy4xMiwtNzAuNDcgLTI5Ny4zNCwtNzAuNDcgLTkxLjA5NCwwIC0yMDIuODEzLDE1LjQ3IC0zMzUuMTU3LDYxLjg4IDQyLjk2OSwtMTAuMzIgODUuOTM4LC0xNS40NyAxMzAuNjI1LC0xNS40NyAyODguNzUyLDAgNTIyLjUwMiwyMTYuNTYgNTIyLjUwMiw0ODQuNjggMCwxNTEuMjUgLTczLjkxLDI4Ny4wMyAtMTkyLjUsMzc2LjQxIDAsMCAtNjAuMTYsLTc3LjM0IC05Mi44MSwtMTIwLjMxIC0yNS43OSwtMzYuMSAtNDguMTMsLTcyLjE5IC00Mi45NjksLTExMy40NCA2Ljg2OSwtNDQuNjkgLTYuODgxLC04OS4zNyAtMzIuNjYxLC0xMjIuMDMgLTIwLjYyNCwtMjQuMDYgLTg1LjkzNywtNzUuNjMgLTEyMy43NDksLTExMy40NCAtMjUuNzgxLC0yNC4wNiAtNjEuODc1LC0yOS4yMiAtODkuMzc1LC0yOS4yMiAtMTIuMDMxLDAgLTIyLjM0NCwxLjcyIC0zMC45MzgsMS43MiBoIC01LjE1NiBjIC0yMC42MjUsMCAtMjkuMjE5LC01LjE1IC01OC40MzcsLTMwLjk0IC02Ljg3NSwtNi44NyAtMjIuMzQ0LC0xMC4zMSAtMzkuNTMyLC0xMC4zMSAtNDguMTI1LDAgLTEyMC4zMTIsMjIuMzUgLTE0OS41MzEsMzIuNjYgLTM5LjUzMSwxNS40NyAtODQuMjE5LDE1NC42OSAtODcuNjU2LDE4Ny4zNCAtMC4xMDcsMS4wMjEgLTAuMTQxLDIuMTE3IC0wLjEwNSwzLjI4MyAwLjAzNywxLjE2NyAwLjE0MiwyLjQwNSAwLjMxMiwzLjcwOSAwLjI1NywxLjk1NSAwLjY1OSw0LjA1OSAxLjE5NSw2LjI5NSAxMC4xNyw0Mi40OSA2OC4xLDEzMi43NTcgNzQuMjIzLDE2MC4zMTMgNS44MDEsMjYuMDk3IDI4LjczMiwyMTAuMDQ3IDM3LjgyMSwyODQuNDQ0IDAuMDg0LDAuNjg5IDAuMTY3LDEuMzY5IDAuMjQ5LDIuMDM5IDAuMTY0LDEuMzM5IDAuMzIyLDIuNjQgMC40NzYsMy45MDIgMC4zMDcsMi41MjIgMC41OTUsNC44ODQgMC44Niw3LjA3MiAwLjEzMiwxLjA5NCAwLjI1OSwyLjE0NCAwLjM4MiwzLjE1IDAuNjY4LDUuNTI5IDEuMTY3LDkuNjg5IDEuNDYyLDEyLjIgMy40MzcsMjkuMjE5IDEuNzE5LDQ5Ljg0NCAtMjIuMzQ0LDg5LjM3NSAtMTcuMTg3LDI5LjIxOSAtMTcuMTg3LDU4LjQzOCAwLDEyMC4zMTMgLTc5LjA2Miw1MS41NjIgLTQ0MCwzMTcuOTY5IC00NTguOTA2LDMzMy40MzcgLTEwLjMxMyw4LjU5NCAtNi44NzUsMTMuNzUgLTYuODc1LDEzLjc1IDAsMCAyLjUyNCwzLjc4OSAxMC44MjMsMy43ODkgMS4zODMsMCAyLjkyNywtMC4xMDYgNC42NDYsLTAuMzUxIDguNTkzLC0xLjcxOSAyMzcuMTg3LC04Mi41IDM5Ny4wMzEsLTE0MC45MzggLTY1LjMxMywyMDIuODEzIDczLjkwNiwzMjQuODQ0IDczLjkwNiwzMjQuODQ0IDAsMCAyMi4zNDQsLTg3LjY1NiA0Ni40MDYsLTE0Ny44MTIgNzkuNzYxLDIzOS4yODMgMjgzLjEwNywyNTguMzE2IDM0NC43MDQsMjU4LjQyMiAwLjE3NSwwIDAuMzQ5LDAgMC41MjIsMCIKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmUiCiAgICAgICAgICAgICBpZD0icGF0aDUyOTIzIiAvPjwvZz48ZwogICAgICAgICAgIGlkPSJnNTI5MjUiCiAgICAgICAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNTU0LjQ3NzEsOTk5LjcwMDIpIj48cGF0aAogICAgICAgICAgICAgZD0iTSAwLDAgNTUsNDE0LjIyMiAtNDIyLjgxMyw3NDUuOTQxIDEzLjc1LDU5OC4xMjkgbCAxMDMuMTI1LDE1OC4xMjUgMzc2LjQwOCwyMzMuNzUgODMwLjE1LC00NTcuMTg4IDQyLjk3LC02OTcuODE2IC0zNjkuNTMsLTUxMy45IC0yOTIuMTksLTQ0LjY5IDE1MS4yNSwyNzYuNzIgdiAyNjYuNCBsIC0xMTAsMjA3Ljk3IC0xMTEuNzEsNzMuOTEgLTE3MS44OCwtMTc3LjAzIFYgLTI2Mi45NyBMIDMyOC4yODEsLTM4OC40MzEgMTU4LjEyNSwtNDA5LjA2MSA4Mi41LC00NTIuMDMgLTQxLjI1LC00MTIuNSAtOTIuODEzLC0yMjYuODcgMCwtOTYuMjUgWiIKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmUiCiAgICAgICAgICAgICBpZD0icGF0aDUyOTI3IiAvPjwvZz48ZwogICAgICAgICAgIGlkPSJnNTI5MjkiCiAgICAgICAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTMxNy41OTk2LDE3NDkuMDgwMSkiPjxwYXRoCiAgICAgICAgICAgICBkPSJtIDAsMCBjIC05MS4wOSwxOC45MDYgLTE5MC43NzksMTMuNzUgLTE5MC43NzksMTMuNzUgMCwwIC0zMi42NTEsLTE1MS4yNSAtMjM1LjQ2NiwtMTkwLjc4MSAxLjcyLDAgMjY4LjEyNiwtOTEuMDk0IDQyNi4yNDUsMTc3LjAzMSIKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiM4OTlkYzA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmUiCiAgICAgICAgICAgICBpZD0icGF0aDUyOTMxIiAvPjwvZz48ZwogICAgICAgICAgIGlkPSJnNTI5MzMiCiAgICAgICAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTc2NC40ODA1LDEwNDYuMTEwNCkiPjxwYXRoCiAgICAgICAgICAgICBkPSJtIDAsMCBjIC02MS44ODEsMzUwLjYyNCAtNDE5LjM4MSw1MjkuMzc0IC00NDAsNTM3Ljk2OCA3Mi4xODksLTIzNy4xODcgLTg3LjY2LC0zNjkuNTMxIC0xOTUuOTQsLTM3MS4yNSAtNjAuMTYxLDAgLTE0MC45MzYsLTM5LjUyOCAtMTQwLjkzNiwtMzkuNTI4IGwgLTgyLjUsLTIwMi44MiAtMzQuMzc1LC0xOC45IDQ2LjQwNSwxMjIuMDMgYyAwLDAgLTEzOS4yMTgsLTczLjkxIC0xNDkuNTMsLTgwLjc4IC0xMC4zMTMsLTYuODggLTQ2LjQwNywtNzkuMDYxIC03My45MDcsLTE1NC42OTEgLTE1LjQ2OCw0OC4xMyAtNDkuODQzLDc3LjM0MSAtNTEuNTYzLDc5LjA2MSAxLjcyLC04LjU5IDM3LjgxMywtMTUxLjI1IDI1Ljc4MiwtMTk0LjIyIC0xMi4wMzIsLTQ0LjY4MSAtODcuNjU3LC04NC4yMTEgLTg3LjY1NywtODQuMjExIGwgMTIzLjc1LC0yNy41IDguNTk1LC0xLjcyIDc1LjYyNSw5NC41MzEgMTUuNDY4LDEyNy4xODkgMzYuMDk0LDM0LjM3IGMgMCwwIDk5LjY4OCwyNS43ODEgOTcuOTY4LC02Ny4wMjkgMCwtNDQuNjkgLTQ5Ljg0MywtNDguMTMgLTk5LjY4NywtODkuMzggLTI5LjIxOCwtMjQuMDYgLTY3LjAzMSwtNjAuMTUgLTg1LjkzOCwtODQuMjExIGwgNjMuNTk1LC0zNC4zOCBjIDAsMCA2OC43NSwyMC42MyAxNTYuNDA1LDExMy40NDEgODkuMzc2LDk0LjUzIDI1Ljc4MiwxNzUuMzA5IDI1Ljc4MiwxNzUuMzA5IGwgMTM1Ljc4NCwyMjAgMTIzLjc1LDE3LjE5MSAyMTguMjgsLTIxMS40MSA2Ljg2OSwtMjU0LjM3MSBDIC0zMTkuNjksLTQ3NC4zOCAtMzkwLjE2LC02MzAuNzggLTQxMi41LC02NzguOTEgYyAxMjUuNDcsODIuNSAyMjAsMjA5LjY4OSAyNjEuMjUsMzU1Ljc4IDMuNDM5LDEzLjc1IDE4Ljg5OSwyMi4zNSAzNi4wOSwzMC45NCAxMi4wMjksNi44NzkgMjcuNSwxMy43NSAyOS4yMiwyMC42MjkgNi44NzksMzkuNTMxIDEwLjMwOSw3OS4wNjEgMTAuMzA5LDExOC41OSAwLDYuODggLTEzLjc1LDIyLjM0MSAtMjUuNzc5LDMyLjY2IC0xMy43NSwxMy43NSAtMjcuNSwyNy41IC0yOS4yMjEsNDEuMjUgQyAtMTQ2LjEwMSw3Mi4xOSAtMjE4LjI4LDIxNC44NDMgLTMzMCwzMTkuNjg3IGwgMTIuMDI5LDEyLjAzMSBjIDExNS4xNiwtMTA2LjU2MiAxODcuMzQsLTI1Mi42NTggMjA0LjUzMSwtNDA5LjA1OSAwLC04LjU5OSAxMi4wMywtMjAuNjMgMjQuMDU5LC0zMC45MzkgMTUuNDcxLC0xNS40NyAzMC45NDEsLTMwLjk0MSAzMC45NDEsLTQ2LjQxIDAsLTQxLjI1IC0zLjQ0MSwtODIuNSAtMTAuMzEsLTEyMi4wMzEgLTMuNDQsLTEzLjc1IC0yMC42MzEsLTIyLjM0IC0zNy44MTEsLTMyLjY1OSAtMTAuMzIsLTYuODcgLTI1Ljc5LC0xNS40NjEgLTI3LjUsLTIwLjYyIC0zNy44MiwtMTM0LjA2MSAtMTIwLjMyLC0yNTIuNjYgLTIzMC4zMiwtMzM4LjU5MSBDIC0xMDYuNTYxLC01MjIuNSA0Ni4zOTksLTI2OS44NDEgMCwwIgogICAgICAgICAgICAgc3R5bGU9ImZpbGw6Izg5OWRjMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZSIKICAgICAgICAgICAgIGlkPSJwYXRoNTI5MzUiIC8+PC9nPjxnCiAgICAgICAgICAgaWQ9Imc1MjkzNyIKICAgICAgICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSg5OTEuMDQsMTcxMS4yNjYxKSI+PHBhdGgKICAgICAgICAgICAgIGQ9Ik0gMCwwIEMgMzkuNTMsNDYuNDA2IDI0LjA2LDExNS4xNTYgMjQuMDYsMTE1LjE1NiBMIC05MS4wOTQsLTU1IGMgLTEuNzE5LDAgNDEuMjUsLTEuNzE5IDkxLjA5NCw1NSIKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiMxYzMwNGQ7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmUiCiAgICAgICAgICAgICBpZD0icGF0aDUyOTM5IiAvPjwvZz48ZwogICAgICAgICAgIGlkPSJnNTI5NDEiCiAgICAgICAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNjA0LjMxOTgsNzgxLjQxOTkpIj48cGF0aAogICAgICAgICAgICAgZD0ibSAwLDAgMTMuNzUsNjguNzUgYyAwLDAgLTU2LjcxOSwtOTkuNjg5IC02MS44NzUsLTExMy40MzkgLTUuMTU2LC0xNS40NzEgMy40MzcsLTQyLjk3MSAyNS43ODEsLTQxLjI1IEMgMCwtODQuMjIgMjcuNSwtNTEuNTYgMjcuNSwtMjcuNSAyNy41LDMuNDQgMCwwIDAsMCIKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiMxYzMwNGQ7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmUiCiAgICAgICAgICAgICBpZD0icGF0aDUyOTQzIiAvPjwvZz48ZwogICAgICAgICAgIGlkPSJnNTI5NDUiCiAgICAgICAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTM4Mi45MTAyLDM3NS43OTk4KSI+PHBhdGgKICAgICAgICAgICAgIGQ9Im0gMCwwIGMgMTExLjcyLDg1LjkzMSAxOTcuNjYsMjA2LjI1IDIzNS40NywzNDIuMDMgMS43MTksNS4xNTEgMTcuMTksMTMuNzUgMjcuNSwyMC42MiAxNy4xOSw4LjYgMzQuMzgsMTguOTExIDM3LjgwOSwzMi42NjEgNi44ODEsMzkuNTI5IDEwLjMyMSw4MC43NzkgMTAuMzIxLDEyMi4wMjkgMCwxNS40NzEgLTE1LjQ3LDMwLjk0IC0zMC45NCw0Ni40IC04LjU5LDEwLjMyMSAtMjIuMzQsMjIuMzUgLTIyLjM0LDMwLjk0MSBDIDI0NC4wNyw3MjAuMTUgMTk0LjIyLDgzOC43NCAxMTYuODgsOTM2LjcxIDgyLjUsMTE0Mi45NiAtNTUsMTIwNi41NiAtNTguNDMsMTIwOC4yOCAtNTUsMTIwMy4xMiAzNC4zOCwxMDc0LjIxIC0yNy41LDkyMi45NiAtOTEuMDksNzY5Ljk5IC0yNTQuMzcsNzk0LjA2IC0yNjguMTIsNzkyLjM0IGMgLTEzLjc1LDAgLTY3LjAzLC02OC43NSAtMTM0LjA2LC0xOTUuOTQgLTguNiwzLjQ0IC00NC42OSwxMi4wMzEgLTg1Ljk0LDUuMTYxIDMwLjk0LDg1LjkzOSA3Ny4zNCwyMDcuOTY5IDg1Ljk0LDIxOC4yNzkgMy40MywzLjQ0IDI5LjIxLDEwLjMxIDQ2LjQsMTUuNDcgMzIuNjYsOC41OSA0OC4xMywxMy43NSA1My4yOCwyMC42MiAzLjQ0LDUuMTYgMjAuNjMsNDQuNjkgMzcuODIsODUuOTQgMTUuNDU5LDAgNTUsMy40NCA1OC40Myw1LjE2IDMuNDM5LDMuNDMgMzYuMSw4Ny42NSAzNi4xLDk3Ljk2IDAsOC42IC02Ny4wMywxNzUuMzIgLTkyLjgyMSwyMzguOTEgMTIuMDQxLDEzLjc1IDI0LjA3MSwzMC45NCAzNi4xMDEsNDkuODQgQyAxMjUuNDcsMTI5NS45MyA0MDAuNDcsOTk2Ljg3IDQwMC40Nyw2MzQuMjEgNDAwLjQ3LDM1NS43OCAyMzcuMTg5LDExMy40MzEgMCwwIG0gMTA4LjI3OSw2NzUuNDYgYyAxNy4xOTEsODcuNjYgMTguOTEsMTYzLjI4IDEzLjc1LDIyNS4xNiA2Ny4wNDEsLTg5LjM4IDEwOC4yOTEsLTE5Ny42NiAxMjAuMzIxLC0zMDkuMzcgMS43MiwtMTMuNzUgMTUuNDcsLTI3LjUgMjkuMjIsLTQxLjI1IDEyLjAzLC0xMC4zMTkgMjUuNzgsLTI0LjA2OSAyNS43OCwtMzIuNjYgMCwtMzkuNTI5IC0zLjQ0LC03OS4wNiAtMTAuMzIxLC0xMTguNiBDIDI4NS4zMiwzOTMuNTkgMjY5Ljg1LDM4NSAyNTcuODIsMzc4LjEyIDI0MC42MywzNjkuNTMgMjI1LjE2LDM2MC45MzEgMjIxLjcyLDM0Ny4xODEgMTgzLjkxLDIxNC44NCAxMDMuMTMsOTcuOTYgLTUuMTUsMTUuNDYgYyAxNTkuODM5LDE2Ni43MjEgMjM3LjE3OSw0NDEuNzIxIDExMy40MjksNjYwIE0gLTU3Ny41LDM0Ny4xODEgYyAxNS40NywyMi4zNDkgMTMuNzUsNjAuMTU5IDE1LjQ3LDcyLjE4OSAxLjcyLDEyLjAzIDUuMTYsMzQuMzggMTguOTEsMzcuODExIDEzLjc1LDMuNDM5IDQ2LjQxLC0xLjcyMSA0Ni40MSwtMjUuNzgxIDAsLTIyLjMzOSAtMjQuMDcsLTI3LjUgLTQxLjI1LC00Mi45NjkgLTEyLjA0LC0xMi4wMzEgLTM2LjEsLTM3LjgxMSAtMzkuNTQsLTQxLjI1IG0gMTYxLjU3LDIwLjYzIGMgLTYuODgsLTguNjAxIC0zNi4xLC0zNC4zOCAtNTYuNzIsLTUzLjI4MSAtMjAuNjMsLTE4LjkxIC00Mi45NywtMzcuODIgLTYwLjE2LC01NSAtNi44NywtNi44OCAtMjAuNjIsLTEwLjMyIC00MS4yNSwtMTAuMzIgaCAtNTEuNTYgYyAyNS43OCwzNC4zOCAxMDEuNDEsMTEzLjQ0IDEyNy4xOSwxMzAuNjMgMzAuOTMsMjAuNjIgNDYuNCw0MS4yNSAyNy41LDc3LjM0MSAtMTguOTEsMzYuMDk5IC02OC43NSwyNy41IC02OC43NSwyNy41IDAsMCAyOS4yMiwxMi4wMjkgNTUsMTIuMDI5IC0zMi42Niw4LjYwMSAtNzMuOTEsMCAtOTIuODIsLTE4Ljg5OSAtMjAuNjIsLTE4LjkxMSAtMTcuMTgsLTg1Ljk0MSAtMjUuNzgsLTEyOC45MTEgLTguNTksLTQ0LjY5IC0zNy44MSwtNjcuMDMgLTgyLjUsLTEwOC4yOCAtMjQuMDYsLTIyLjM0IC00MS4yNSwtMjkuMjIgLTU1LC0yOS4yMiAtMjkuMjIsNS4xNjEgLTYzLjU5LDEzLjc1IC04Ny42NSwyMi4zNSAtMTcuMTksMjIuMzQgLTQyLjk3LDk2LjI1IC00OS44NSwxMjcuMTgxIDUuMTYsMTcuMTg5IDI1Ljc4LDUzLjI3OSAzNi4xLDczLjkwOSAyMC42MiwzOS41MyAzMi42NSw2MS44NyAzNi4wOSw4Mi41IDYuODgsMjkuMjIxIDI5LjIyLDIwOS42OSAzNy44MSwyODUuMzEgMjIuMzUsLTI5LjIyIDUzLjI4LC03Ny4zNCA0Ni40MSwtMTA4LjI4IDQ5Ljg0LDcwLjQ3IDEzLjc1LDEzOS4yMiAtMy40NCwxNjYuNzIgLTE1LjQ3LDI3LjUgLTM2LjA5LDgyLjUgLTE4LjksMTQwLjk0IDE3LjE4LDU4LjQzIDc5LjA2LDIyMCA3OS4wNiwyMjAgMCwwIDIwLjYyLC0zNi4xIDQ5Ljg0LC0yOS4yMiAyOS4yMiw2Ljg3IDI2NC42OSwzNjAuOTMgMjY0LjY5LDM2MC45MyAwLDAgNjMuNTksLTEzOS4yMSAtMy40NCwtMjQwLjYyIC02OC43NSwtMTAxLjQxIC0xMzUuNzgsLTEyMC4zMSAtMTM1Ljc4LC0xMjAuMzEgMCwwIDk0LjUzLC0xNy4xOSAxODIuMTksNDYuNCAzNi4wODksLTg0LjIyIDcwLjQ3LC0xNzEuODcgNzIuMTc5LC0xODMuOSAtNS4xNDksLTEyLjAzIC03My44OTksLTE3Ny4wMyAtODAuNzc5LC0xODcuMzUgLTMuNDMsLTMuNDMgLTI3LjUsLTEwLjMxIC00NC42OCwtMTMuNzUgLTI5LjIyLC04LjU5IC00Ni40MSwtMTMuNzUgLTUzLjI4LC0xOC45IC0xMi4wNCwtMTAuMzIgLTY3LjA0LC0xNjEuNTYgLTkyLjgyLC0yMzUuNDcgLTMwLjkzLC04LjU5IC02MS44NywtMjUuNzc5IC04NC4yMiwtNjAuMTU5IDEyLjA0LDguNTk5IDQ5Ljg1LDEzLjc1IDc3LjM1LDE3LjE4OSAyNC4wNiwxLjcyIDk3Ljk3LC0zNy44MDkgMTE2Ljg3LC0xMTEuNzIgdiAtMy40NCBjIDMuNDQsLTI3LjUgLTUuMTUsLTUzLjI3OSAtMTguOSwtNzMuODk5IG0gLTY0Ny45Nyw4OTguODk5IGMgMTI3LjE4LC00NC42OCAyNTYuMDksLTkyLjgxIDI3Ni43MiwtMTAzLjEyIDAsMCAxLjcxLC0xLjcyIDMuNDMsLTMuNDQgMTAuMzIsLTguNTkgMjAuNjMsLTI0LjA2IDguNiwtNDYuNDEgLTYuODgsLTEyLjAzIC0xNy4xOSwtMTAuMzEgLTMyLjY2LC0zLjQzIC0yMC42MiwxMC4zMSAtMTQ0LjM3LDgyLjUgLTI1Ni4wOSwxNTYuNCBtIDEzMDYuMjUsMjMyLjAzIGMgMCwwIDg1LjkyOSwtMy40MyAxNzUuMzEsLTEzLjc1IC0yMDEuMDksMTU4LjEzIC0zOTEuODgxLDIwNC41NCAtNTQ2LjU2LDIwNC41NCAtMjEzLjEzLDAgLTM1Ny41LC04Ny42NiAtMzY2LjEsLTkyLjgyIGwgNjcuMDQsMTA2LjU3IGMgMCwwIC0yNjguMTMsMjUuNzggLTM2Mi42NiwtMjU3LjgyIC0yNC4wNiw2MC4xNiAtNDYuNDEsMTQ3LjgyIC00Ni40MSwxNDcuODIgMCwwIC0xMzkuMjIsLTEyMi4wNCAtNzMuOSwtMzI0Ljg1IC0xNTkuODUsNTguNDQgLTM4OC40NCwxMzkuMjIgLTM5Ny4wNCwxNDAuOTQgLTEyLjAzLDEuNzIgLTE1LjQ2LC0zLjQ0IC0xNS40NiwtMy40NCAwLDAgLTMuNDQsLTUuMTUgNi44NywtMTMuNzUgMTguOTEsLTE1LjQ3IDM3OS44NSwtMjgxLjg3IDQ1OC45MSwtMzMzLjQ0IC0xNy4xOSwtNjEuODcgLTE3LjE5LC05MS4wOSAwLC0xMjAuMzEgMjQuMDYsLTM5LjUzIDI1Ljc4LC02MC4xNSAyMi4zNCwtODkuMzcgLTMuNDQsLTI5LjIyIC0zNC4zNywtMjgxLjg3OSAtNDEuMjUsLTMxMi44MiAtNi44NywtMzAuOTI5IC03OS4wNiwtMTQwLjkyOSAtNzUuNjIsLTE3My41OSAzLjQzLC0zMi42NSA0OC4xMiwtMTcxLjg3IDg3LjY1LC0xODcuMzM5IEMgLTgzNS4zMSwxNjUgLTc2My4xMiwxNDIuNjUgLTcxNSwxNDIuNjUgYyAxNy4xOSwwIDMyLjY2LDMuNDQgMzkuNTQsMTAuMzEgMjkuMjEsMjUuNzkgMzcuODEsMzAuOTQgNTguNDMsMzAuOTQgaCA1LjE2IGMgOC41OSwwIDE4LjkxLC0xLjcxOSAzMC45NCwtMS43MTkgMjcuNSwwIDYzLjU5LDUuMTU5IDg5LjM3LDI5LjIxOSAzNy44MSwzNy44MSAxMDMuMTMsODkuMzggMTIzLjc1LDExMy40NCAyNS43OCwzMi42NiAzOS41Myw3Ny4zNDEgMzIuNjYsMTIyLjAzIC01LjE2LDQxLjI1IDE3LjE4LDc3LjM0IDQyLjk3LDExMy40NDEgMzIuNjUsNDIuOTY5IDkyLjgxLDEyMC4zMDkgOTIuODEsMTIwLjMwOSAxMTguNTksLTg5LjM4IDE5Mi41LC0yMjUuMTYgMTkyLjUsLTM3Ni40MSAwLC0yNjguMTIgLTIzMy43NSwtNDg0LjY4IC01MjIuNSwtNDg0LjY4IC00NC42OSwwIC04Ny42Niw1LjE1MSAtMTMwLjYzLDE1LjQ3IDEzMi4zNSwtNDYuNDEgMjQ0LjA3LC02MS44OCAzMzUuMTYsLTYxLjg4IDE5NC4yMiwwIDI5Ny4zNCw3MC40NyAyOTcuMzQsNzAuNDcgMCwwIC0zNi4wOSwtNDYuNDA5IC05NC41MywtOTkuNjkgaCAxLjcxOSBDIDIwMS4xLC0yMTEuNDEgMzU3LjUsNTMuMjggMzU3LjUsNTMuMjggYyAwLDAgLTEyLjAzLC04NS45NCAtMjcuNSwtMTQ0LjM4IDQyNy45NywzMjEuNDExIDM1NS43NzksNzIzLjU5IDM1NC4wNyw3MzcuMzQgMy40MywtNS4xNSA0Ni40LC01Ni43MSA2OC43NSwtODQuMjEgNjguNzUsNzA4LjEyIC01MTAuNDcsOTM2LjcxIC01MTAuNDcsOTM2LjcxIgogICAgICAgICAgICAgc3R5bGU9ImZpbGw6IzFjMzA0ZDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZSIKICAgICAgICAgICAgIGlkPSJwYXRoNTI5NDciIC8+PC9nPjxnCiAgICAgICAgICAgaWQ9Imc1Mjk0OSIKICAgICAgICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSg5MjUuNzI2MSwxMzAzLjkyMDkpIj48cGF0aAogICAgICAgICAgICAgZD0ibSAwLDAgYyAzMi42NTYsMzcuODEyIDE1LjQ2OSwxMDguMjgxIC00NC42ODgsMTIwLjMxMiAxNS40NjksMzYuMDk0IDM3LjgxMywxMDguMjgxIDM3LjgxMywxMDguMjgxIDAsMCAtMTc1LjMxMywtMjc1IC0xOTAuNzgxLC0yODAuMTU2IC0xNS40NjksLTUuMTU2IC0zMC45MzgsNTUgLTMwLjkzOCw1NSAtMzIuNjU2LC0xMjUuNDY4IDU1LC0xNDIuNjU4IDY1LjMxMywtMTAzLjEyNSBDIC0xMTUuMTU2LC04Ny42NTcgLTMyLjY1NiwtMzYuMDk0IDAsMCIKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiMxYzMwNGQ7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmUiCiAgICAgICAgICAgICBpZD0icGF0aDUyOTUxIiAvPjwvZz48ZwogICAgICAgICAgIGlkPSJnNTI5NTMiCiAgICAgICAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNzc2LjE5NDgsMTIzNi44ODYyKSI+PHBhdGgKICAgICAgICAgICAgIGQ9Im0gMCwwIDg5LjM3NSwxNTIuOTY5IGMgMCwwIDUxLjU2MywtMjUuNzgxIDI1Ljc4MSwtNjcuMDMxIEMgODIuNSwzNy44MTMgMCwwIDAsMCIKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNmOWQ3MjE7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmUiCiAgICAgICAgICAgICBpZD0icGF0aDUyOTU1IiAvPjwvZz48ZwogICAgICAgICAgIGlkPSJnNTI5NTciCiAgICAgICAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTc5My42ODk1LDEyNTUuNzk0OSkiPjxwYXRoCiAgICAgICAgICAgICBkPSJtIDAsMCBjIDguNjAxLC0xNS40NjkgMjIwLC00MDUuNjI1IC0xLjcyLC04MzEuODc1IDM0LjM4LDM0LjM4IDYzLjYwMSw3MC40NyA4OS4zOCwxMDQuODQgMTQ3LjgxMSwyMDYuMjUgMTI3LjE5MSw0ODkuODUgLTQyLjk3LDY3OC45MSBDIDMwLjk0LC0zMi42NTYgMTUuNDcxLC0xNS40NjkgMCwwIgogICAgICAgICAgICAgc3R5bGU9ImZpbGw6I2RlMjMyZTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZSIKICAgICAgICAgICAgIGlkPSJwYXRoNTI5NTkiIC8+PC9nPjxnCiAgICAgICAgICAgaWQ9Imc1Mjk2MSIKICAgICAgICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgyMDg3LjU5OTYsMTA3Ny4wNDk4KSI+PHBhdGgKICAgICAgICAgICAgIGQ9Im0gMCwwIGMgLTMuNDM5LDIxOS45OTYgLTc3LjM1LDQwOS4wNTggLTIyMCw1NjAuMzA4IC0xLjcyLDEuNzE5IC0xLjcyLDEuNzE5IC0zLjQzOSwzLjQzOCBDIC0zNDguOTEsNjk2LjA5IC01MzIuODEsNzU3Ljk2NSAtNzE1LDczMi4xODMgYyAtNjguNzUsLTguNTkzIC0xMzcuNSwtMjIuMzQzIC0xOTAuNzc5LC0zOS41MzEgLTguNjAxLC0xLjcxOSAtMTcuMTkxLC0zLjQzNyAtMjUuNzgxLC02Ljg3NSBoIDMuNDMgYyAxMy43NSwxLjcxOSAyNjQuNjkxLDEzLjc1IDQ5OC40NDEsLTE0MC45MzcgQyAtMTg3LjM1LDM4NC45OTYgLTc3LjM1LDIwNy45NjUgMCwwIgogICAgICAgICAgICAgc3R5bGU9ImZpbGw6I2RlMjMyZTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZSIKICAgICAgICAgICAgIGlkPSJwYXRoNTI5NjMiIC8+PC9nPjxnCiAgICAgICAgICAgaWQ9Imc1Mjk2NSIKICAgICAgICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxNjAyLjkxMDIsMTkyNy44MjMyKSI+PHBhdGgKICAgICAgICAgICAgIGQ9Im0gMCwwIGMgLTExMy40NCw1Ni43MTggLTIzMi4wMyw4NS45MzcgLTM1MC42Miw4Ny42NTYgLTQ0LjY5MSwwIC04NS45NDEsLTMuNDM4IC0xMjMuNzUsLTEwLjMxMyAtMTM0LjA2NiwtMjQuMDYyIC0yNDQuMDY2LC0xMjAuMzEyIC0yODEuODc5LC0yNDkuMjE4IDAsMCAyNy41LDQyLjk2OCA5Mi44MTMsODcuNjU2IDI0LjA2MiwzMi42NTYgNjEuODc2LDg0LjIxOSA5Ny45NjYsMTI3LjE4NyAxMC4zMSwtMjIuMzQzIDIyLjM1LC00OS44NDMgMjUuNzgsLTYzLjU5MyBDIC00MjEuMDksMjQuMDYyIC0yNTYuMDksNDguMTI1IDAsMCIKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNkZTIzMmU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmUiCiAgICAgICAgICAgICBpZD0icGF0aDUyOTY3IiAvPjwvZz48ZwogICAgICAgICAgIGlkPSJnNTI5NjkiCiAgICAgICAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNTI1LjI1NTksMTg1Mi4xOTgyKSI+PHBhdGgKICAgICAgICAgICAgIGQ9Ik0gMCwwIEMgLTMuNDM4LC0xMy43NSAtNzAuNDY5LC0yNDcuNSAxMzAuNjI1LC0yNTQuMzc1IDEzLjc1LC0xODMuOTA3IDAsLTEuNzE5IDAsMCIKICAgICAgICAgICAgIHN0eWxlPSJmaWxsOiNkZTIzMmU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmUiCiAgICAgICAgICAgICBpZD0icGF0aDUyOTcxIiAvPjwvZz48ZwogICAgICAgICAgIGlkPSJnNTI5NzMiCiAgICAgICAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoODUzLjUzNzEsMjAxOC45MTg5KSI+PHBhdGgKICAgICAgICAgICAgIGQ9Im0gMCwwIGMgLTEyLjAzMiwtNS4xNTYgLTI3MS41NjMsLTg5LjM3NSAtMTIzLjc1LC0zMTQuNTMxIDAsMCAtMzAuOTM4LDIyMS43MTggMTIzLjc1LDMxNC41MzEiCiAgICAgICAgICAgICBzdHlsZT0iZmlsbDojZGUyMzJlO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lIgogICAgICAgICAgICAgaWQ9InBhdGg1Mjk3NSIgLz48L2c+PC9nPjwvZz48L2c+PC9zdmc+"
                    />
                    <Card.Body>
                      <Card.Title>1inch Workshop</Card.Title>
                      <Card.Text>
                        Join a workshop on 1inch's Aggregation API and Limit
                        Order Protocol with the 1inch Network's Lead Blockchain
                        Engineer Kirill Kuznetcov. Dive deeper into how the API
                        and protocol work, explore their use cases, study the
                        documentation and learn about bounties you could
                        collect. Get your questions for Kirill ready!
                      </Card.Text>
                    </Card.Body>
                    <Card.Footer>
                      <small className="text-muted">
                        Skills required: meant for people just starting or
                        they’ve already set out in their Web3
                      </small>
                    </Card.Footer>
                    <Card.Footer>
                      <small className="text-muted">
                        Afternoon session: 1:30pm to 2:30pm with 1inch with
                        Kirill Kuznetcov
                      </small>
                    </Card.Footer>
                    <Card.Footer>
                      <small className="text-muted">
                        Only for people with Hackathon tickets
                      </small>
                    </Card.Footer>
                  </Card>
                </CardDeck>

                <CardDeck>
                  <Card>
                    <Card.Img
                      style={{
                        height: '80px',
                        margin: 'auto',
                        width: '80px',
                      }}
                      src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNjAwIiBoZWlnaHQ9IjY4MCIgdmlld0JveD0iMCAwIDYwMCA2ODAiIGZpbGw9Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxwYXRoIGQ9Ik0yMDIuMDQ0IDMxOS4zMDRDMjE2LjI0OCAzMTkuMzA0IDIyOS4zNiAzMTQuNTc3IDIzOS45MjMgMzA2LjM5N0wxNTMuMjM4IDIxOS44NzFDMTQ1LjA0MyAyMzAuMjMyIDE0MC4zMDggMjQzLjMyIDE0MC4zMDggMjU3LjY4MUMxNDAuMTI2IDI5MS42NzMgMTY3LjgwNyAzMTkuMzA0IDIwMi4wNDQgMzE5LjMwNFoiIGZpbGw9IiMwNDc5NUIiLz4KPHBhdGggZD0iTTQ1OS45MTMgMjU3LjQ5OUM0NTkuOTEzIDI0My4zMiA0NTUuMTc4IDIzMC4yMzIgNDQ2Ljk4MyAyMTkuNjg5TDM2MC4yOTggMzA2LjIxNUMzNzAuNjc5IDMxNC4zOTUgMzgzLjc5MSAzMTkuMTIxIDM5OC4xNzggMzE5LjEyMUM0MzIuMjMyIDMxOS4zMDMgNDU5LjkxMyAyOTEuNjczIDQ1OS45MTMgMjU3LjQ5OVoiIGZpbGw9IiMwNDc5NUIiLz4KPHBhdGggZD0iTTUwMy42MiAxNjMuNTIxTDQ2NS4xOTQgMjAxLjg3NkM0NzcuOTQyIDIxNy4xNDUgNDg1LjU5MSAyMzYuNDE0IDQ4NS41OTEgMjU3Ljg2M0M0ODUuNTkxIDMwNi4wMzQgNDQ2LjQzNyAzNDUuMTE3IDM5OC4xNzcgMzQ1LjExN0MzNzYuODcgMzQ1LjExNyAzNTcuMzg0IDMzNy40ODIgMzQyLjA4NyAzMjQuNzU4TDMwMC4wMTkgMzY2Ljc0OEwyNTcuOTUxIDMyNC43NThDMjQyLjY1NCAzMzcuNDgyIDIyMy4zNSAzNDUuMTE3IDIwMS44NjEgMzQ1LjExN0MxNTMuNjAxIDM0NS4xMTcgMTE0LjQ0NyAzMDYuMDM0IDExNC40NDcgMjU3Ljg2M0MxMTQuNDQ3IDIzNi41OTUgMTIyLjA5NiAyMTcuMTQ1IDEzNC44NDQgMjAxLjg3NkwxMTUuMTc2IDE4Mi4yNDRMOTYuNDE4MyAxNjMuNTIxQzc0LjU2NDkgMTk5LjUxMyA2MS45OTkzIDI0MS41MDQgNjEuOTk5MyAyODYuNTg0QzYxLjk5OTMgNDE3LjgyOCAxNjguNTM0IDUyMy45ODUgMjk5LjgzNyA1MjMuOTg1QzQzMS4xMzkgNTIzLjk4NSA1MzcuNjc0IDQxNy42NDYgNTM3LjY3NCAyODYuNTg0QzUzOC4wMzkgMjQxLjMyMiA1MjUuNDczIDE5OS4zMzEgNTAzLjYyIDE2My41MjFaIiBmaWxsPSIjMDQ3OTVCIi8+CjxwYXRoIGQ9Ik00NzIuMTE0IDEyMi42MkM0MjguOTU0IDc3LjM1NzQgMzY3Ljc2NCA0OS4wMDAxIDMwMC4wMTkgNDkuMDAwMUMyMzIuMjczIDQ5LjAwMDEgMTcxLjI2NiA3Ny4zNTc0IDEyNy45MjQgMTIyLjYyQzEyMi4wOTYgMTI4LjggMTE2LjQ1MSAxMzUuMzQ1IDExMS4xNjkgMTQyLjA3TDI5OS44MzcgMzMwLjM5Mkw0ODguNTA0IDE0MS44ODlDNDgzLjc2OSAxMzUuMzQ1IDQ3OC4xMjQgMTI4LjYxOSA0NzIuMTE0IDEyMi42MlpNMzAwLjAxOSA3OS45MDIzQzM1NS43NDUgNzkuOTAyMyA0MDcuNDY1IDEwMS4zNTIgNDQ2LjI1NSAxNDAuNDM0TDMwMC4wMTkgMjg2LjQwMUwxNTMuNzgzIDE0MC40MzRDMTkyLjc1NSAxMDEuMzUyIDI0NC4yOTMgNzkuOTAyMyAzMDAuMDE5IDc5LjkwMjNaIiBmaWxsPSIjMDQ3OTVCIi8+CjxwYXRoIGQ9Ik05NC4yMzQ3IDYxOS45NDZDMTAyLjg2IDYxOS45NDYgMTExLjMyOCA2MTcuMDQ1IDExNi40MjQgNjEzLjEyNVY1ODguMTEzSDkzLjA1ODZWNTk4LjkzM0gxMDMuODc5VjYwNS41MTlDMTAwLjY2NCA2MDcuMjQ0IDk3LjY4NDcgNjA3Ljk1IDk0LjIzNDcgNjA3Ljk1Qzg0Ljc0NzQgNjA3Ljk1IDc5LjE4MDQgNjAwLjU3OSA3OS4xODA0IDU5MS41NjJDNzkuMTgwNCA1ODEuMjEzIDg1Ljc2NjcgNTc1LjE3NSA5My42ODU5IDU3NS4xNzVDOTkuNjQ0OSA1NzUuMTc1IDEwNC4wMzYgNTc3Ljc2MyAxMDcuMDE1IDU4MC42NjRMMTE2LjI2NyA1NzIuODIzQzExMC41NDQgNTY2LjIzNyAxMDEuNzYyIDU2Mi44NjUgOTMuNjg1OSA1NjIuODY1Qzc1Ljg4NzMgNTYyLjg2NSA2NS40NTkxIDU3NS40MSA2NS40NTkxIDU5MS41NjJDNjUuNDU5MSA2MDkuNjc1IDc4LjE2MTEgNjE5Ljk0NiA5NC4yMzQ3IDYxOS45NDZaTTEyNC41MjQgNjE5LjMxOUgxMzYuNzU2VjYwMC4wMzFDMTM2Ljc1NiA1OTQuMDcyIDEzOS41NzggNTkxLjE3IDE0My44MTIgNTkxLjE3QzE0Ny44MTEgNTkxLjE3IDE0OS45MjggNTkyLjk3NCAxNDkuOTI4IDU5OS4zMjVWNjE5LjMxOUgxNjIuMTZWNTk2LjM0NUMxNjIuMTYgNTg0LjE5MiAxNTYuMzU4IDU4MC4yNzIgMTQ4LjQzOCA1ODAuMjcyQzE0My45NjkgNTgwLjI3MiAxMzkuODkyIDU4MS45OTcgMTM2LjkxMiA1ODUuNDQ3SDEzNi43NTZWNTgwLjk3N0gxMjQuNTI0VjYxOS4zMTlaTTE4OS4wNTYgNjIwLjAyNUMyMDEuMTMxIDYyMC4wMjUgMjA5Ljc1NiA2MTEuOTQ5IDIwOS43NTYgNjAwLjE4N0MyMDkuNzU2IDU4OC4zNDggMjAxLjEzMSA1ODAuMjcyIDE4OS4wNTYgNTgwLjI3MkMxNzYuNjY4IDU4MC4yNzIgMTY4LjI3OCA1ODguNTA1IDE2OC4yNzggNjAwLjE4N0MxNjguMjc4IDYxMS43OTIgMTc2LjY2OCA2MjAuMDI1IDE4OS4wNTYgNjIwLjAyNVpNMTg5LjA1NiA2MDkuMjA0QzE4NC4xOTUgNjA5LjIwNCAxODAuNjY2IDYwNS4yODQgMTgwLjY2NiA2MDAuMTg3QzE4MC42NjYgNTk0LjkzNCAxODQuMTE2IDU5MS4xNyAxODkuMDU2IDU5MS4xN0MxOTMuODM5IDU5MS4xNyAxOTcuMzY3IDU5NC44NTYgMTk3LjM2NyA2MDAuMTg3QzE5Ny4zNjcgNjA1LjQ0MSAxOTMuNjgyIDYwOS4yMDQgMTg5LjA1NiA2MDkuMjA0Wk0yMjguOTU1IDYyMC4wMjVDMjM5LjQ2MiA2MjAuMDI1IDI0NC41NTggNjE0Ljg1IDI0NC41NTggNjA3LjcxNEMyNDQuNTU4IDYwMC4xODcgMjM5LjkzMiA1OTcuOTkyIDIzMS40NjQgNTk1LjA5MUMyMjcuNjIyIDU5My43NTggMjI2LjIxMSA1OTIuODk1IDIyNi4yMTEgNTkxLjcxOUMyMjYuMjExIDU5MC4yMyAyMjcuODU3IDU4OS42MDIgMjI5LjI2OSA1ODkuNjAyQzIzMS44NTYgNTg5LjYwMiAyMzUuMTQ5IDU5MC45MzUgMjM3LjgxNSA1OTMuNjAxTDI0My42MTcgNTg1LjA1NUMyMzkuMTQ4IDU4MS43NjIgMjM0LjkxNCA1ODAuMjcyIDIyOS4xMTIgNTgwLjI3MkMyMjEuMTE0IDU4MC4yNzIgMjE0Ljc2MyA1ODQuNzQxIDIxNC43NjMgNTkyLjUwM0MyMTQuNzYzIDU5OS44NzQgMjIwLjQwOSA2MDMuMDEgMjI2LjgzOCA2MDQuOTdDMjMxLjYyMSA2MDYuNDYgMjMyLjc5NyA2MDcuMjQ0IDIzMi43OTcgNjA4LjQ5OUMyMzIuNzk3IDYwOS45MSAyMzEuNTQzIDYxMC43NzIgMjI5LjExMiA2MTAuNzcyQzIyNS41ODQgNjEwLjc3MiAyMjIuMjkxIDYwOS4zNjEgMjE5LjA3NiA2MDYuNDZMMjEzLjM1MiA2MTQuMzc5QzIxNy41ODYgNjE4LjE0MyAyMjIuOTE4IDYyMC4wMjUgMjI4Ljk1NSA2MjAuMDI1Wk0yNTcuMzczIDU3Ni4xOTVDMjYxLjYwNyA1NzYuMTk1IDI2NC43NDMgNTczLjEzNyAyNjQuNzQzIDU2OS4xMzhDMjY0Ljc0MyA1NjUuMjk2IDI2MS42MDcgNTYyLjIzOCAyNTcuMzczIDU2Mi4yMzhDMjUzLjEzOCA1NjIuMjM4IDI1MC4wODEgNTY1LjM3NCAyNTAuMDgxIDU2OS4xMzhDMjUwLjA4MSA1NzMuMTM3IDI1My4xMzggNTc2LjE5NSAyNTcuMzczIDU3Ni4xOTVaTTI1MS4yNTcgNjE5LjMxOUgyNjMuNDg4VjU4MC45NzdIMjUxLjI1N1Y2MTkuMzE5Wk0yODUuMDA1IDYyMC4wMjVDMjk1LjUxMSA2MjAuMDI1IDMwMC42MDggNjE0Ljg1IDMwMC42MDggNjA3LjcxNEMzMDAuNjA4IDYwMC4xODcgMjk1Ljk4MiA1OTcuOTkyIDI4Ny41MTQgNTk1LjA5MUMyODMuNjcyIDU5My43NTggMjgyLjI2IDU5Mi44OTUgMjgyLjI2IDU5MS43MTlDMjgyLjI2IDU5MC4yMyAyODMuOTA3IDU4OS42MDIgMjg1LjMxOCA1ODkuNjAyQzI4Ny45MDYgNTg5LjYwMiAyOTEuMTk5IDU5MC45MzUgMjkzLjg2NSA1OTMuNjAxTDI5OS42NjcgNTg1LjA1NUMyOTUuMTk4IDU4MS43NjIgMjkwLjk2MyA1ODAuMjcyIDI4NS4xNjEgNTgwLjI3MkMyNzcuMTY0IDU4MC4yNzIgMjcwLjgxMyA1ODQuNzQxIDI3MC44MTMgNTkyLjUwM0MyNzAuODEzIDU5OS44NzQgMjc2LjQ1OCA2MDMuMDEgMjgyLjg4NyA2MDQuOTdDMjg3LjY3IDYwNi40NiAyODguODQ2IDYwNy4yNDQgMjg4Ljg0NiA2MDguNDk5QzI4OC44NDYgNjA5LjkxIDI4Ny41OTIgNjEwLjc3MiAyODUuMTYxIDYxMC43NzJDMjgxLjYzMyA2MTAuNzcyIDI3OC4zNCA2MDkuMzYxIDI3NS4xMjUgNjA2LjQ2TDI2OS40MDEgNjE0LjM3OUMyNzMuNjM1IDYxOC4xNDMgMjc4Ljk2NyA2MjAuMDI1IDI4NS4wMDUgNjIwLjAyNVpNMzUzLjQyNCA2MjAuMDI1QzM2My4zMDQgNjIwLjAyNSAzNzEuMDY2IDYxNS4zMiAzNzUuODQ5IDYwOS40MzlMMzY2LjI4MyA2MDEuMjA3QzM2My4wNjkgNjA1LjA0OSAzNTguNTk5IDYwNy40NzkgMzUzLjQyNCA2MDcuNDc5QzM0NC4zMjkgNjA3LjQ3OSAzMzguNDQ5IDYwMC41MDEgMzM4LjQ0OSA1OTEuNTYyQzMzOC40NDkgNTgxLjM2OSAzNDUuMjcgNTc1LjQxIDM1My4zNDYgNTc1LjQxQzM1OC43NTYgNTc1LjQxIDM2My4yMjUgNTc4LjMxMiAzNjYuMjA1IDU4MS44NEwzNzUuNzcxIDU3My42MDdDMzY5LjY1NSA1NjYuMTU4IDM2MS4xODcgNTYyLjg2NSAzNTMuMzQ2IDU2Mi44NjVDMzM0Ljk5OSA1NjIuODY1IDMyNC42NDkgNTc1LjY0NiAzMjQuNjQ5IDU5MS41NjJDMzI0LjY0OSA2MTAuMDY3IDMzNy40MjkgNjIwLjAyNSAzNTMuNDI0IDYyMC4wMjVaTTM4MS44NzYgNjE5LjMxOUgzOTQuMTA4VjU5OS45NTJDMzk0LjEwOCA1OTMuOTkzIDM5Ni45MyA1OTEuMTcgNDAxLjI0MyA1OTEuMTdDNDA1LjU1NSA1OTEuMTcgNDA3LjY3MiA1OTMuMDUyIDQwNy42NzIgNTk5LjI0NlY2MTkuMzE5SDQxOS45MDRWNTk2LjE4OUM0MTkuOTA0IDU4NC4xMTQgNDE0LjAyMyA1ODAuMjcyIDQwNi4xMDQgNTgwLjI3MkM0MDEuNCA1ODAuMjcyIDM5Ny4yNDQgNTgxLjk5NyAzOTQuMjY1IDU4NS4zNjhMMzk0LjEwOCA1ODUuMjlWNTYxLjYxMUgzODEuODc2VjYxOS4zMTlaTTQ0My45NjggNjIwLjAyNUM0NDguNTk0IDYyMC4wMjUgNDUyLjA0NCA2MTguNDU2IDQ1NC41NTMgNjE1LjI0Mkw0NTQuNzEgNjE1LjMyVjYxOS4zMTlINDY2LjM5M1Y1ODAuOTc3SDQ1NC4wODNWNTg0Ljk3Nkw0NTMuOTI2IDU4NS4wNTVDNDUxLjI2IDU4MS45MTggNDQ3LjQxOCA1ODAuMjcyIDQ0My4zNDEgNTgwLjI3MkM0MzMuNDYyIDU4MC4yNzIgNDI1Ljg1NiA1ODguNDI2IDQyNS44NTYgNjAwLjE4N0M0MjUuODU2IDYxMi4wMjcgNDMzLjQ2MiA2MjAuMDI1IDQ0My45NjggNjIwLjAyNVpNNDQ2LjU1NiA2MDkuMjA0QzQ0MS42OTQgNjA5LjIwNCA0MzguMzIzIDYwNS41OTcgNDM4LjMyMyA2MDAuMTg3QzQzOC4zMjMgNTk0LjY5OSA0NDEuODUxIDU5MS4wOTIgNDQ2LjU1NiA1OTEuMDkyQzQ1MS4xMDMgNTkxLjA5MiA0NTQuNjMyIDU5NC45MzQgNDU0LjYzMiA2MDAuMTg3QzQ1NC42MzIgNjA1LjY3NiA0NTAuOTQ3IDYwOS4yMDQgNDQ2LjU1NiA2MDkuMjA0Wk00ODEuMTEgNTc2LjE5NUM0ODUuMzQ0IDU3Ni4xOTUgNDg4LjQ4MSA1NzMuMTM3IDQ4OC40ODEgNTY5LjEzOEM0ODguNDgxIDU2NS4yOTYgNDg1LjM0NCA1NjIuMjM4IDQ4MS4xMSA1NjIuMjM4QzQ3Ni44NzYgNTYyLjIzOCA0NzMuODE4IDU2NS4zNzQgNDczLjgxOCA1NjkuMTM4QzQ3My44MTggNTczLjEzNyA0NzYuODc2IDU3Ni4xOTUgNDgxLjExIDU3Ni4xOTVaTTQ3NC45OTUgNjE5LjMxOUg0ODcuMjI2VjU4MC45NzdINDc0Ljk5NVY2MTkuMzE5Wk00OTYuMTE5IDYxOS4zMTlINTA4LjM1VjYwMC4wMzFDNTA4LjM1IDU5NC4wNzIgNTExLjE3MyA1OTEuMTcgNTE1LjQwNyA1OTEuMTdDNTE5LjQwNiA1OTEuMTcgNTIxLjUyMyA1OTIuOTc0IDUyMS41MjMgNTk5LjMyNVY2MTkuMzE5SDUzMy43NTRWNTk2LjM0NUM1MzMuNzU0IDU4NC4xOTIgNTI3Ljk1MiA1ODAuMjcyIDUyMC4wMzMgNTgwLjI3MkM1MTUuNTY0IDU4MC4yNzIgNTExLjQ4NyA1ODEuOTk3IDUwOC41MDcgNTg1LjQ0N0g1MDguMzVWNTgwLjk3N0g0OTYuMTE5VjYxOS4zMTlaIiBmaWxsPSIjMDQ3OTVCIi8+Cjwvc3ZnPg=="
                    />
                    <Card.Body>
                      <Card.Title>
                        Tenderly and Gnosis Chain workshop
                      </Card.Title>
                      <Card.Text>
                        Tenderly and Gnosis Chain workshop. Build a game with
                        Tenderly on Gnosis Chain: step up your Smart Contract
                        development!
                        <br />
                        <br />
                        Building dApps can be challenging, but it shouldn’t be.
                        <br />
                        <br />
                        In this session, we’ll create a simple game that we’ll
                        be deploying to Gnosis Chain and use the Tenderly
                        tooling to debug, monitor, and automate it.
                        <br />
                        <br />
                        You will learn how to use Blockscout for that – an
                        invaluable open-source tool for exploring, confirming
                        and inspecting transactions on any EVM blockchain.
                        <br />
                        <br />
                        By the end of this session, you’ll know how to:
                        <br />
                        <br />
                        - Deploy your Smart Contract to Gnosis Chain
                        <br />
                        - Verify your Smart Contract on Blockscout
                        <br />
                        - Import that Smart Contract into Tenderly
                        <br />
                        - Monitor and debug the game in Tenderly
                        <br />- Use Web3 Actions to automate the interactions
                        with the game
                      </Card.Text>
                    </Card.Body>
                    <Card.Footer>
                      <small className="text-muted">
                        Skills required: meant for people just starting or
                        they’ve already set out in their Web3
                      </small>
                    </Card.Footer>
                    <Card.Footer>
                      <small className="text-muted">
                        Morning session: 9:30am to 11:30am with Tenderly and
                        Gnosis Chain team{' '}
                      </small>
                    </Card.Footer>
                    <Card.Footer>
                      <small className="text-muted">
                        <a
                          target="_blank"
                          href="https://www.eventbrite.com/e/tenderly-and-gnosis-chain-workshop-tickets-302836791997?aff=ebdsoporgprofile"
                        >
                          REGISTER FOR FREE HERE
                        </a>
                      </small>
                    </Card.Footer>
                  </Card>
                </CardDeck>
                <CardDeck>
                  <Card>
                    <Card.Img
                      style={{
                        height: '80px',
                        margin: 'auto',
                        width: '80px',
                      }}
                      src="data:image/svg+xml;base64,DQo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9Ijk5IiBoZWlnaHQ9Ijk5IiB2aWV3Qm94PSIwIDAgOTkgOTkiPg0KICA8ZyBpZD0iR3JvdXBfNTE1MiIgZGF0YS1uYW1lPSJHcm91cCA1MTUyIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMTkzLjI1NyAtNTcuMDA1KSI+DQogICAgPGNpcmNsZSBpZD0iRWxsaXBzZV80NyIgZGF0YS1uYW1lPSJFbGxpcHNlIDQ3IiBjeD0iNDkiIGN5PSI0OSIgcj0iNDkiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDE5My43NTcgNTcuNTA1KSIgZmlsbD0iIzM3NGRmYSIgc3Ryb2tlPSIjMzc0ZGZhIiBzdHJva2UtbGluZWNhcD0ic3F1YXJlIiBzdHJva2UtbGluZWpvaW49InJvdW5kIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHN0cm9rZS13aWR0aD0iMSIvPg0KICAgIDxnIGlkPSJHcm91cF81MTUxIiBkYXRhLW5hbWU9Ikdyb3VwIDUxNTEiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDI2OC42MjMgLTM5LjcwNSkiPg0KICAgICAgPHBhdGggaWQ9IlBhdGhfOTQ2MyIgZGF0YS1uYW1lPSJQYXRoIDk0NjMiIGQ9Ik0tMTkuMjY2LDE1MC42ODRsLTEuNi0xLjIzMmExLDEsMCwwLDAtMS40LjE4MiwxLDEsMCwwLDAsLjE4MiwxLjRsMS43MTcsMS4zMTdjLjA0OS4wMzEsNC44NzIsMy4xMDcsNC44NzIsOC4yOWExMC4yOTQsMTAuMjk0LDAsMCwxLTEwLjEsMTAuMTVjLTUuMzgsMC0xMC4xLTQuNC0xMC4xLTkuNDE2YTExLjksMTEuOSwwLDAsMSwzLjItNy40NWwuOTksNy45NTNhMSwxLDAsMCwwLC45OTEuODc3Ljg4Mi44ODIsMCwwLDAsLjEyNS0uMDA4LDEsMSwwLDAsMCwuODY4LTEuMTE1TC0zMC45LDE1MC41NDktNDIsMTUzLjA1NmExLDEsMCwwLDAtLjc1NSwxLjIsMSwxLDAsMCwwLDEuMTk1Ljc1NWw2Ljg1Ni0xLjU0N2ExMy4yMjcsMTMuMjI3LDAsMCwwLTIuOTkxLDcuOTE3YzAsNi4wODEsNS42NTQsMTEuNDE2LDEyLjExMywxMS40MTZBMTIuMzExLDEyLjMxMSwwLDAsMC0xMy41LDE2MC42NTJDLTEzLjUsMTU0LjU2My0xOC42NzMsMTUxLjA2NS0xOS4yNjYsMTUwLjY4NFoiIGZpbGw9IiNmZmYiLz4NCiAgICAgIDxwYXRoIGlkPSJQYXRoXzk0NjQiIGRhdGEtbmFtZT0iUGF0aCA5NDY0IiBkPSJNLTcuNzA2LDEzOC4zMzFhMSwxLDAsMCwwLTEuMjg0LS41OTNsLTYuMzcxLDIuMzQ4YTEzLjksMTMuOSwwLDAsMCwxLjg2NS02LjQyNGMwLTQuMDItMi45Mi0xMi4zMjItMTEuOTQ2LTEyLjMyMi05LjI1MiwwLTEyLjI0NSw4LjMtMTIuMjQ0LDEyLjI3M2ExMC4xOTMsMTAuMTkzLDAsMCwwLDIuOTE0LDcuNjM1bDUuNjIzLDQuMzQ1YS45ODcuOTg3LDAsMCwwLC42MDkuMjA4LDEsMSwwLDAsMCwuNzkzLS4zOSwxLDEsMCwwLDAtLjE4Mi0xLjRsLTUuNDY1LTQuMjEyYTguMjI1LDguMjI1LDAsMCwxLTIuMjkzLTYuMTM1YzAtMy4zNjgsMi41LTEwLjMyMiwxMC4yNDUtMTAuMzIyLDguMjU4LDAsOS45NDYsOC4xNDIsOS45NDYsMTAuMzIyYTEyLjYyLDEyLjYyLDAsMCwxLTEuOTgyLDYuMDk0bC0yLjI3OC03LjU4MkExLDEsMCwwLDAtMjEsMTMxLjVhMSwxLDAsMCwwLS42NywxLjI0NmwzLjE5LDEwLjYxN0wtOC4zLDEzOS42MTVBMSwxLDAsMCwwLTcuNzA2LDEzOC4zMzFaIiBmaWxsPSIjZmZmIi8+DQogICAgICA8cGF0aCBpZD0iUGF0aF85NDY1IiBkYXRhLW5hbWU9IlBhdGggOTQ2NSIgZD0iTS0yNC4xNTQsMTYzLjdWMTMwLjQwNmExLDEsMCwwLDAtMS0xLDEsMSwwLDAsMC0xLDFWMTYzLjdhMSwxLDAsMCwwLDEsMUExLDEsMCwwLDAtMjQuMTU0LDE2My43WiIgZmlsbD0iI2ZmZiIvPg0KICAgIDwvZz4NCiAgPC9nPg0KPC9zdmc+DQo="
                    />
                    <Card.Body>
                      <Card.Title>
                        Yearn strategies workshop with Facu Ameal from the Yearn
                        team
                      </Card.Title>
                      <Card.Text>
                        Yearn is the reference when it comes to yield
                        generation, it offers easy to use saving accounts for
                        regular users in order to earn interests on their
                        currencies. In this workshop, you will learn how to
                        write your own strategy and possibly make passive income
                        thanks to the fees generated.
                      </Card.Text>
                    </Card.Body>
                    <Card.Footer>
                      <small className="text-muted">
                        Skills required: basic Solidity level
                      </small>
                    </Card.Footer>
                    <Card.Footer>
                      <small className="text-muted">
                        Morning session: 9:30am to 12:30pm with Facu Ameal and
                        others to be announced soon
                      </small>
                    </Card.Footer>
                    <Card.Footer>
                      <small className="text-muted">
                        Afternoon session: 1:30pm to 4:30pm (instructors to be
                        announced soon)
                      </small>
                    </Card.Footer>
                  </Card>
                  <Card>
                    <Card.Img
                      variant="top"
                      src="data:image/svg+xml;base64,PHN2ZyBpZD0iTG9nbyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2aWV3Qm94PSIwIDAgODUwLjM5IDI5MS45NyI+PHBhdGggZD0iTTE5Ni44NSwyMzYuNmg0NC42NHY1LjMzaC0xOXY0Ny4xOWgtNi43MVYyNDEuOTNoLTE5WiIvPjxwYXRoIGQ9Ik0yNDguNywyMzYuNmg2LjQyVjI1N2guNzNjMS4yLTIuMDgsNC4zNC01LjgzLDEyLjIyLTUuODMsOC4yNCwwLDE0LDQuNDEsMTQsMTMuMzR2MjQuNjZoLTYuNDJ2LTI0YzAtNi4yNC0zLjcyLTktOS4xMi05LTYuMDksMC0xMS40NSwzLjgzLTExLjQ1LDEyLjIydjIwLjc1SDI0OC43WiIvPjxwYXRoIGQ9Ik0zMDguMSwyNTEuMDhjMTEsMCwxNy4xNSw2Ljg5LDE3LjE1LDE3LjgzdjNoLTI5Yy4xNSw4LjE3LDQuNTksMTMuMjQsMTEuODEsMTMuMjQsNi44NiwwLDEwLTQuMDgsMTAuNjItNy42Nmg1Ljc2djEuMWMtLjg4LDQuNTYtNS4yMiwxMS40MS0xNi4zLDExLjQxcy0xOC4wOS02LjgyLTE4LjA5LTE5LjQ3UzI5Ny4yLDI1MS4wOCwzMDguMSwyNTEuMDhabTExLDE2LjEyYy0uMDgtNi43NS0zLjgzLTExLjI3LTExLjA1LTExLjI3cy0xMS4xNiw0LjU2LTExLjY3LDExLjI3WiIvPjxwYXRoIGQ9Ik0zNTMuNjEsMjM2LjZIMzc2LjhjOS4xOSwwLDE2LjI3LDQuNDUsMTYuMjcsMTMuMTcsMCw4LjUzLTUuNjIsMTEtOC45LDExLjg1di43M2MzLjc2Ljg3LDkuODgsMy40Niw5Ljg4LDEyLjUxLDAsOS42My03LjU1LDE0LjI2LTE3LDE0LjI2SDM1My42MVptNi42NCw1LjMzdjE3LjgzSDM3NmM1Ljk0LDAsMTAuNzUtMi40NCwxMC43NS04Ljkzcy00LjgxLTguOS0xMC43NS04LjlabTAsMjMuMTV2MTguNzFoMTZjNi40MiwwLDExLjI3LTIuNjYsMTEuMjctOS4zN3MtNC44NS05LjM0LTExLjI3LTkuMzRaIi8+PHBhdGggZD0iTTQwOC43NCwyODkuMTJoLTYuNDFWMjM2LjZoNi40MVoiLz48cGF0aCBkPSJNNDM1LjY5LDI1MS4wOGMxMS4xMywwLDE4LjYsNi45MywxOC42LDE5LjQ3UzQ0Ni44MiwyOTAsNDM1LjY5LDI5MHMtMTguNTYtNi45My0xOC41Ni0xOS40NFM0MjQuNjEsMjUxLjA4LDQzNS42OSwyNTEuMDhabTAsMzMuOTJjNy4yNiwwLDEyLjA3LTUsMTIuMDctMTQuNDVzLTQuODEtMTQuNDctMTIuMDctMTQuNDctMTIuMDcsNS0xMi4wNywxNC40N1M0MjguNDcsMjg1LDQzNS42OSwyODVaIi8+PHBhdGggZD0iTTQ3OC44NywyNTEuMDhjMTEsMCwxNiw2Ljc1LDE2LjQ4LDEyLjU4djEuMWgtNmMtLjMzLTQtMy4xNC04LjY4LTEwLjM2LTguNjgtNy40LDAtMTIuMTQsNS4wNy0xMi4xNCwxNC40N1M0NzEuNTcsMjg1LDQ3OSwyODVzMTAtNC42LDEwLjQ3LTguOWg2djEuMDljLS41OCw2LjA2LTUuNDcsMTIuOC0xNi41OSwxMi44cy0xOC40OS03LTE4LjQ5LTE5LjQ0UzQ2Ny43NCwyNTEuMDgsNDc4Ljg3LDI1MS4wOFoiLz48cGF0aCBkPSJNNTE1Ljg0LDI2OS42OCw1MzYuNzQsMjg4djEuMWgtNy40OEw1MTAuNjIsMjcyLjZoLS43MnYxNi41MmgtNi40MlYyMzYuNmg2LjQyVjI2N2guNzJMNTI4Ljg5LDI1Mmg3LjM3djEuMDlaIi8+PHBhdGggZD0iTTU1OCwyNTEuMDhjMTEsMCwxNiw2Ljc1LDE2LjQ4LDEyLjU4djEuMWgtNmMtLjMzLTQtMy4xNC04LjY4LTEwLjM2LTguNjgtNy40LDAtMTIuMTQsNS4wNy0xMi4xNCwxNC40N1M1NTAuNjYsMjg1LDU1OC4xLDI4NXMxMC00LjYsMTAuNDctOC45aDZ2MS4wOUM1NzQsMjgzLjI1LDU2OS4wOCwyOTAsNTU4LDI5MHMtMTguNDktNy0xOC40OS0xOS40NFM1NDYuODQsMjUxLjA4LDU1OCwyNTEuMDhaIi8+PHBhdGggZD0iTTU4Mi41NywyMzYuNkg1ODlWMjU3aC43M2MxLjItMi4wOCw0LjM0LTUuODMsMTIuMjEtNS44Myw4LjI1LDAsMTQsNC40MSwxNCwxMy4zNHYyNC42NmgtNi40MnYtMjRjMC02LjI0LTMuNzEtOS05LjExLTktNi4wOSwwLTExLjQ1LDMuODMtMTEuNDUsMTIuMjJ2MjAuNzVoLTYuNDJaIi8+PHBhdGggZD0iTTYyNS42NywyNjMuNDF2LTEuMWMuNzctNi45Myw2LjcxLTExLjIzLDE2LjMtMTEuMjNzMTUuMjQsNC4zLDE1LjI0LDEzLjU3djI0LjQ3aC02LjA1di01LjMzaC0uNzNjLTEuNiwzLTUuMSw2LjEzLTEzLDYuMTNzLTEzLjQyLTQtMTMuNDItMTFjMC03LjIyLDUuNTgtMTAuMjUsMTMuMDktMTEuMkw2NTEsMjY2di0xLjY4YzAtNi0zLjE0LTguNDItOS4xOS04LjQycy05LjYzLDIuNDQtMTAuMTQsNy41NVptMTMsMjEuN2M2Ljc0LDAsMTIuMzYtMy44LDEyLjM2LTEyLjIydi0yLjIzbC0xMi43MywxLjY1Yy00Ljg5LjY1LTgsMi40LTgsNi40NVM2MzMuNTUsMjg1LjExLDYzOC42MiwyODUuMTFaIi8+PHBhdGggZD0iTTY2NywyMzguNTdoNy40NHY3SDY2N1pNNjczLjkxLDI1MnYzNy4xNkg2NjcuNVYyNTJaIi8+PHBhdGggZD0iTTY4NC41MiwyNTJoNi4yNHY1LjI1aC43M2MxLjMxLTIuMyw0LjQ4LTYuMDksMTIuNC02LjA5LDguMjQsMCwxNCw0LjQxLDE0LDEzLjM0djI0LjY2aC02LjQydi0yNGMwLTYuMjQtMy43Mi05LTkuMTItOS02LjA5LDAtMTEuNDUsMy44My0xMS40NSwxMi4yMnYyMC43NWgtNi40MloiLz48cGF0aCBkPSJNNzcxLjUzLDIzNS41OGMxNS41NywwLDI2LjE4LDkuODUsMjYuMTgsMjcuMjhzLTEwLjYxLDI3LjI4LTI2LjE4LDI3LjI4LTI2LjE4LTkuODUtMjYuMTgtMjcuMjhTNzU2LDIzNS41OCw3NzEuNTMsMjM1LjU4Wm0wLDQ5LjA5YzExLjc0LDAsMTkuMzYtNy45MiwxOS4zNi0yMS44MVM3ODMuMjcsMjQxLDc3MS41MywyNDFzLTE5LjQsNy45NS0xOS40LDIxLjg0Uzc1OS43OSwyODQuNjcsNzcxLjUzLDI4NC42N1oiLz48cGF0aCBkPSJNODI0Ljg0LDIzNS43NmMxMi4zNiwwLDIwLDUuNjIsMjAuNzEsMTUuNjV2MS4wOWgtNi4zMWMtLjQtOC4yNy02LjQxLTExLjU5LTE0LjI5LTExLjU5cy0xMy4xNywzLjMyLTEzLjE3LDksNC40Miw3LjkxLDExLjQ1LDkuMTFsNS45NSwxYzEwLjI1LDEuNzEsMTYuNjMsNS4zNiwxNi42MywxNC43N1M4MzguNTIsMjkwLDgyNS45MywyOTBzLTIxLjE4LTUuOC0yMi4wNi0xNnYtMS4wOWg2LjI3Yy45MSw4LjYsNy41MSwxMiwxNS42OCwxMnMxMy40Ni0zLjM1LDEzLjQ2LTkuNTktNC45Mi04LjQ5LTEyLTkuNjZsLTUuOTQtMWMtMTAuMjEtMS42OC0xNi4xMi01LjgtMTYuMTItMTQuNTJTODEyLjQ4LDIzNS43Niw4MjQuODQsMjM1Ljc2WiIvPjxwYXRoIGQ9Ik04NDQuNyw1My42OVYxOTMuNDJIODIxLjRWNTMuNjlaIi8+PHJlY3QgeD0iODE3LjA0IiB5PSIxLjgzIiB3aWR0aD0iMzEuOTMiIGhlaWdodD0iMjkuNDgiLz48cGF0aCBkPSJNNTg3LjY4LDQ5LjE2YzQwLjc0LDAsNjUuOSwyOC4zMiw2NS45LDY5LjYxdjEwLjM5SDU0Mi4xNGMuNTQsMjguNzEsMTguOCw1MC4wNiw0Ni4xNCw1MC4wNiwyNi4yNSwwLDM5LjQ3LTE3Ljc5LDQxLjkzLTMxLjc0aDIyLjJ2NC4xYy0zLjgzLDE3LjUtMjIuMDgsNDYuMS02NC4xOSw0Ni4xLTQwLjc0LDAtNjkuNDYtMjguMzItNjkuNDYtNzQuMjZTNTQ3LjQ4LDQ5LjE2LDU4Ny42OCw0OS4xNlptNDIuNTMsNjEuNjRjLS44Mi0yNC4wNi0xNS40Ni00My4xOC00Mi41My00My4xOC0yNi41MywwLTQzLjM2LDE5LjEyLTQ1LjU0LDQzLjE4WiIvPjxwYXRoIGQ9Ik03MzQuMjQsNDkuMTZjMzUuODIsMCw1Ni44OCwxNy4xMSw2MC40Myw0Mi4yNnY0LjExSDc3Mi41MmMtMS4zNi0xOC42LTE2LjQtMjcuOTEtMzguMjgtMjcuOTEtMjEuNiwwLTM1Ljc3LDktMzUuNzcsMjIuNDRzMTIuMjUsMTgsMjcuMjksMjAuNzhsMjIuNyw0LjFjMjcuODksNC45Miw0Ny4zMSwxNC40OSw0Ny4zMSwzOS4zOCwwLDI0LjYxLTIwLjc5LDQzLjM2LTU5LjA3LDQzLjM2cy02MS4yNS0xOC43NS02NS4zNi00NS44MnYtNC4xaDIyLjIxYzIuNDYsMjEuMzIsMTkuNjMsMzEuNDYsNDIuODgsMzEuNDYsMjMuNTEsMCwzNi4wOS05Ljg2LDM2LjA5LTIzLjUzLDAtMTMuMTMtMTEuMjEtMTcuNzgtMjkuNTMtMjEuMDZsLTIyLjctNC4xYy0yNi00LjY1LTQ0LTE1LjMyLTQ0LTM5LjM4QzY3Ni4yNyw2Ni4yNyw2OTguNDIsNDkuMTYsNzM0LjI0LDQ5LjE2WiIvPjxwYXRoIGQ9Ik00MzkuMDYsOC45M2gyMy4zOFY1My42OWg0MS4xNVY3NEg0NjIuNDR2OTYuMTlsMS44MSwyLjE5aDQwLjYydjIxLjA4SDQ3MC4yOWMtMTguODIsMC0zMS4yMy0xMS40OC0zMS4yMy0zNC4xOFoiLz48cGF0aCBkPSJNMTk3Ljc5LDk0LjQzYzQuNjQtMjYsMjcuNjItNDUuMjcsNjIuMDctNDUuMjdzNTguMjUsMTkuMjksNTguMjUsNTMuMnY5MS4wNkgyOTQuNzNWMTcyLjY0aC0xLjUxYy02LDkuODQtMjAuNTEsMjUtNDkuNDksMjUtMjkuODEsMC01MC44Ny0xNi41Ni01MC44Ny00MS40NSwwLTI2LDIxLjA2LTM4LjI4LDQ5LjUtNDFsNTMuMDUtNC40MXYtOWMwLTIyLjY5LTE0LjIyLTM0LjYtMzYuMS0zNC42LTIyLjE1LDAtMzYuODYsMTEuOTEtMzkuMzIsMzEuMzJoLTIyLjJabTUwLDg0Ljc5YzI1LjQzLDAsNDcuNjEtMTQuNjgsNDcuNjEtNDYuMTN2LTMuODNsLTQ5Ljc3LDMuNDZjLTE4LDEuNjQtMzAuNTcsNi4yOS0zMC41NywyMi40MkMyMTUuMDcsMTczLjI4LDIyOC45MywxNzkuMjIsMjQ3LjgsMTc5LjIyWiIvPjxwYXRoIGQ9Ik0zNjcuODYsNTMuNjlWNzRoMS44OWM0LjkzLTEwLjQsMTMuNC0yMC4yNiwzNC4xOS0yMC4yNmgxNC4yMlY3NGgtMTRjLTI2LjI1LDAtMzYuMzcsMTUtMzYuMzcsNDMuNDh2NzZIMzQ0LjQ4VjUzLjY5WiIvPjxwYXRoIGQ9Ik05MS44NywxLjgzYzUzLjQ0LDAsNzcuMTksMzMuODMsODAuNDgsNjAuMjh2NEgxNTAuMTRjLTIuMjMtMTkuMDgtMTcuNzMtNDQuOC01OC4yNy00NC44LTQwLjE0LDAtNjguMjUsMjkuMjctNjguMjUsNzguNDlzMjcuNjcsNzguNDksNjcuODIsNzguNDksNTYuNzMtMjYuNjMsNTkuNzYtNDUuODVoMjEuMTV2NGMtMy45NSwyNS42Ny0yNi41Miw2MS4zNC04MC40OCw2MS4zNC01OC43OSwwLTkwLjQ1LTQzLjQ0LTkwLjQ1LTk3LjkzQzEuNDIsNDAuMjQsMzguMTcsMS44Myw5MS44NywxLjgzWiIvPjwvc3ZnPg=="
                      style={{
                        margin: 'auto',
                        marginTop: 15,
                        width: '160px',
                      }}
                    />
                    <Card.Body>
                      <Card.Title>
                        Build your first blockchain application in Python with
                        The Blockchain OS.
                      </Card.Title>
                      <Card.Text>
                        Code with{' '}
                        <a href="https://cartesi.io/en/" target="_blank">
                          Cartesi
                        </a>
                        . Use your current programming/tech stack to build
                        blockchain applications. Open to all expertise levels of
                        developers.
                      </Card.Text>
                    </Card.Body>
                    <Card.Footer>
                      <small className="text-muted">
                        Skills required: Basic programming skills
                      </small>
                    </Card.Footer>
                    <Card.Footer>
                      <small className="text-muted">
                        Full-day session: 9:30am to 4:30pm with Cartesi team
                      </small>
                    </Card.Footer>
                  </Card>
                  <Card>
                    <Card.Img
                      src="data:image/svg+xml;base64,DQo8c3ZnIHdpZHRoPSI1OTAuNDg1IiBoZWlnaHQ9IjU5MC40ODUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggc3R5bGU9ImZpbGw6I2ZmZjtzdHJva2Utd2lkdGg6LjMwNDg5IiBkPSJNMCAwaDU5MC40ODV2NTkwLjQ4NUgweiIvPjxnIHN0cm9rZS1saW5lY2FwPSJzcXVhcmUiPjxwYXRoIGQ9Ik01NTguNDEzIDI0Ljc3MyAzMzAuNDMgMTk0LjA5OGw0Mi4xNi05OS45IDE4NS44MjItNjkuNDI1IiBzdHJva2U9IiNlMjc2MWIiIGZpbGw9IiNlMjc2MWIiIHN0cm9rZS13aWR0aD0iMi4yOTEyNyIvPjxnIHN0cm9rZT0iI2U0NzYxYiIgZmlsbD0iI2U0NzYxYiI+PHBhdGggZD0ibTMyLjEwNyAyNC43NzMgMjI2LjE0OSAxNzAuOTI5LTQwLjA5OC0xMDEuNTA0TDMyLjEwNyAyNC43NzNNNDc2LjM4NSA0MTcuMjY4bC02MC43MTkgOTMuMDI2IDEyOS45MTYgMzUuNzQ0IDM3LjM0Ny0xMjYuNzA4LTEwNi41NDQtMi4wNjJNOC4wNDkgNDE5LjMzbDM3LjExOCAxMjYuNzA4IDEyOS45MTUtMzUuNzQ0LTYwLjcxOC05My4wMjZMOC4wNDkgNDE5LjMzIiBzdHJva2Utd2lkdGg9IjIuMjkxMjciLz48cGF0aCBkPSJtMTY3Ljc1IDI2MC4wODYtMzYuMjAyIDU0Ljc2MiAxMjguOTk5IDUuNzI4LTQuNTgzLTEzOC42MjItODguMjE0IDc4LjEzMk00MjIuNzcgMjYwLjA4NmwtODkuMzYtNzkuNzM2LTIuOTggMTQwLjIyNiAxMjguNzctNS43MjgtMzYuNDMtNTQuNzYyTTE3NS4wODIgNTEwLjI5NGw3Ny40NDYtMzcuODA2LTY2LjkwNi01Mi4yNDEtMTAuNTQgOTAuMDQ3TTMzNy45OTIgNDcyLjQ4OGw3Ny42NzQgMzcuODA2LTEwLjc2OS05MC4wNDctNjYuOTA1IDUyLjI0IiBzdHJva2Utd2lkdGg9IjIuMjkxMjciLz48L2c+PGcgc3Ryb2tlPSIjZDdjMWIzIiBmaWxsPSIjZDdjMWIzIj48cGF0aCBkPSJtNDE1LjY2NiA1MTAuMjk0LTc3LjY3NC0zNy44MDYgNi4xODcgNTAuNjM3LS42ODggMjEuMzA5IDcyLjE3NS0zNC4xNE0xNzUuMDgyIDUxMC4yOTRsNzIuMTc2IDM0LjE0LS40NTktMjEuMzEgNS43MjktNTAuNjM2LTc3LjQ0NiAzNy44MDYiIHN0cm9rZS13aWR0aD0iMi4yOTEyNyIvPjwvZz48cGF0aCBkPSJtMjQ4LjQwMyAzODYuNzk0LTY0LjYxNC0xOS4wMTggNDUuNTk3LTIwLjg1IDE5LjAxNyAzOS44NjhNMzQyLjExNiAzODYuNzk0bDE5LjAxOC0zOS44NjggNDUuODI1IDIwLjg1LTY0Ljg0MyAxOS4wMTgiIHN0cm9rZT0iIzIzMzQ0NyIgZmlsbD0iIzIzMzQ0NyIgc3Ryb2tlLXdpZHRoPSIyLjI5MTI3Ii8+PGcgc3Ryb2tlPSIjY2Q2MTE2IiBmaWxsPSIjY2Q2MTE2Ij48cGF0aCBkPSJtMTc1LjA4MiA1MTAuMjk0IDEwLjk5OS05My4wMjYtNzEuNzE3IDIuMDYyIDYwLjcxOCA5MC45NjRNNDA0LjY2OCA0MTcuMjY4bDEwLjk5OCA5My4wMjYgNjAuNzItOTAuOTY0LTcxLjcxOC0yLjA2Mk00NTkuMiAzMTQuODQ4bC0xMjguNzcgNS43MjggMTEuOTE2IDY2LjIxOCAxOS4wMTctMzkuODY4IDQ1LjgyNiAyMC44NSA1Mi4wMTEtNTIuOTI4TTE4My43OSAzNjcuNzc2bDQ1LjgyNS0yMC44NSAxOC43ODggMzkuODY4IDEyLjE0NC02Ni4yMTgtMTI4Ljk5OS01LjcyOCA1Mi4yNDEgNTIuOTI4IiBzdHJva2Utd2lkdGg9IjIuMjkxMjciLz48L2c+PGcgc3Ryb2tlPSIjZTQ3NTFmIiBmaWxsPSIjZTQ3NTFmIj48cGF0aCBkPSJtMTMxLjU0OCAzMTQuODQ4IDU0LjA3NCAxMDUuMzk5LTEuODMzLTUyLjQ3LTUyLjI0LTUyLjkzTTQwNy4xODkgMzY3Ljc3NmwtMi4yOTIgNTIuNDdMNDU5LjIgMzE0Ljg0OWwtNTIuMDExIDUyLjkyOE0yNjAuNTQ3IDMyMC41NzZsLTEyLjE0NCA2Ni4yMTggMTUuMTIzIDc4LjEzMiAzLjQzNy0xMDIuODc4LTYuNDE2LTQxLjQ3Mk0zMzAuNDMgMzIwLjU3NmwtNi4xODYgNDEuMjQzIDIuNzUgMTAzLjEwNyAxNS4zNTItNzguMTMyLTExLjkxNS02Ni4yMTgiIHN0cm9rZS13aWR0aD0iMi4yOTEyNyIvPjwvZz48cGF0aCBkPSJtMzQyLjM0NiAzODYuNzk0LTE1LjM1MiA3OC4xMzIgMTAuOTk4IDcuNTYyIDY2LjkwNS01Mi4yNDEgMi4yOTItNTIuNDctNjQuODQzIDE5LjAxN00xODMuNzkgMzY3Ljc3NmwxLjgzMiA1Mi40NyA2Ni45MDYgNTIuMjQyIDEwLjk5OC03LjU2Mi0xNS4xMjMtNzguMTMyLTY0LjYxNC0xOS4wMTgiIHN0cm9rZT0iI2Y2ODUxYiIgZmlsbD0iI2Y2ODUxYiIgc3Ryb2tlLXdpZHRoPSIyLjI5MTI3Ii8+PHBhdGggZD0ibTM0My40OTEgNTQ0LjQzNC42ODgtMjEuMzEtNS43MjktNS4wNGgtODYuMzhsLTUuMjcgNS4wNC40NTggMjEuMzEtNzIuMTc2LTM0LjE0IDI1LjIwNCAyMC42MjEgNTEuMDk2IDM1LjUxNWg4Ny43NTZsNTEuMzI0LTM1LjUxNSAyNS4yMDQtMjAuNjIxLTcyLjE3NSAzNC4xNCIgc3Ryb2tlPSIjYzBhZDllIiBmaWxsPSIjYzBhZDllIiBzdHJva2Utd2lkdGg9IjIuMjkxMjciLz48cGF0aCBkPSJtMzM3Ljk5MiA0NzIuNDg4LTEwLjk5OC03LjU2MmgtNjMuNDY4bC0xMC45OTggNy41NjItNS43MjkgNTAuNjM3IDUuMjctNS4wNDFoODYuMzgxbDUuNzI5IDUuMDQtNi4xODctNTAuNjM2IiBzdHJva2U9IiMxNjE2MTYiIGZpbGw9IiMxNjE2MTYiIHN0cm9rZS13aWR0aD0iMi4yOTEyNyIvPjxnIHN0cm9rZT0iIzc2M2QxNiIgZmlsbD0iIzc2M2QxNiI+PHBhdGggZD0ibTU2OC4wMzYgMjA1LjA5NiAxOS40NzYtOTMuNDg0LTI5LjEtODYuODQtMjIwLjQyIDE2My41OTggODQuNzc3IDcxLjcxNiAxMTkuODM0IDM1LjA1NyAyNi41NzktMzAuOTMyLTExLjQ1Ny04LjI0OSAxOC4zMy0xNi43MjYtMTQuMjA1LTEwLjk5OCAxOC4zMy0xMy45NzctMTIuMTQ0LTkuMTY1TTMuMjM3IDExMS42MTJsMTkuNDc2IDkzLjQ4NC0xMi4zNzMgOS4xNjUgMTguMzMgMTMuOTc3LTEzLjk3NyAxMC45OTggMTguMzMgMTYuNzI2LTExLjQ1NiA4LjI0OSAyNi4zNSAzMC45MzIgMTE5LjgzMy0zNS4wNTcgODQuNzc4LTcxLjcxNkwzMi4xMDcgMjQuNzczbC0yOC44NyA4Ni44MzkiIHN0cm9rZS13aWR0aD0iMi4yOTEyNyIvPjwvZz48cGF0aCBkPSJtNTQyLjYwMyAyOTUuMTQzLTExOS44MzQtMzUuMDU3IDM2LjQzMSA1NC43NjItNTQuMzAzIDEwNS4zOTkgNzEuNDg4LS45MTdINTgyLjkzbC00MC4zMjYtMTI0LjE4N00xNjcuNzUgMjYwLjA4NiA0Ny45MTcgMjk1LjE0MyA4LjA0OSA0MTkuMzNoMTA2LjMxNWw3MS4yNTguOTE3LTU0LjA3NC0xMDUuNCAzNi4yMDItNTQuNzZNMzMwLjQzIDMyMC41NzZsNy41NjItMTMyLjIwNiAzNC44MjctOTQuMTcySDIxOC4xNmwzNC4zNjkgOTQuMTcyIDguMDE5IDEzMi4yMDYgMi43NSA0MS43MDEuMjI5IDEwMi42NWg2My40NjhsLjQ1OC0xMDIuNjUgMi45NzktNDEuNyIgc3Ryb2tlPSIjZjY4NTFiIiBmaWxsPSIjZjY4NTFiIiBzdHJva2Utd2lkdGg9IjIuMjkxMjciLz48L2c+PC9zdmc+"
                      style={{
                        height: '100px',
                        width: '100px',
                        margin: 'auto',
                      }}
                    />
                    <Card.Body>
                      <Card.Title>
                        Web3 workshop for frontend devs with the Metamask Team
                      </Card.Title>
                      <Card.Text>
                        You are a frontend developer and want to get started
                        with Web3? You have found the right place as the
                        MetaMask team itself will teach you how to level up your
                        frontend skills to build powerful dapps using all the
                        needed web3 tools.
                      </Card.Text>
                    </Card.Body>
                    <Card.Footer>
                      <small className="text-muted">
                        Skills required: JavaScript and generic frontend skills
                        only
                      </small>
                    </Card.Footer>
                    <Card.Footer>
                      <small className="text-muted">
                        Morning session: 9:30am to 12:30pm with Eric Bishard
                      </small>
                    </Card.Footer>
                  </Card>
                </CardDeck>
                <CardDeck>
                  <Card>
                    <Card.Title>
                      <span style={{ fontSize: '80px' }}>🍱</span>
                    </Card.Title>

                    <Card.Body>
                      <Card.Title>
                        BentoBox workshop with Sushi Core Trident Dev Sarang
                        Parikh
                      </Card.Title>
                      <Card.Text>
                        BentoBox 🍱 is a revolutionary technology built by
                        @Boring_Crypto. It is used by both Sushi and MIM Spell
                        and Sarang is a core dev at Sushi working on it.
                        "BentoBox is a token vault that can support a collection
                        of Dapps."
                        <Collapse in={openDesc1}>
                          <div className="hidden">
                            "Users can interact with these many Dapps in a
                            gas-efficient manner, by leveraging the benefits of
                            using a mutual token vault. On the other side of the
                            coin, developers can even take advantage of the
                            power of BentoBox, as it allows for the deployment
                            of contracts at a lower cost, thanks to its gas
                            efficient infrastructure, so developers can focus on
                            what really matters, building.
                          </div>
                        </Collapse>
                        <div>
                          <a
                            href="#"
                            onClick={(e) => {
                              e.preventDefault()
                              setOpenDesc1(!openDesc1)
                            }}
                            aria-controls="open-ticket-description"
                            aria-expanded={openDesc1}
                          >
                            read more
                          </a>
                        </div>
                      </Card.Text>
                    </Card.Body>
                    <Card.Footer>
                      <small className="text-muted">
                        Skills required: Basic solidity level
                      </small>
                    </Card.Footer>
                    <Card.Footer>
                      <small className="text-muted">
                        Morning session: 9:30am to 12:30pm with Sarang Parikh
                      </small>
                    </Card.Footer>
                  </Card>
                  <Card>
                    <Card.Img
                      variant="top"
                      src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA4NTIgODUyIiBzdHlsZT0id2lkdGg6ODBweDtoZWlnaHQ6ODBweCIgY2xhc3M9InNfbG9nb19fMTZlck4iPjxkZWZzPjwvZGVmcz48ZGVmcz48Y2xpcFBhdGggaWQ9ImEiPjxwYXRoIGQ9Ik0wIDBoODUydjg1MkgweiI+PC9wYXRoPjwvY2xpcFBhdGg+PC9kZWZzPjxnIGRhdGEtbmFtZT0iRnVlbCBsb2dvIj48ZyBjbGlwLXBhdGg9InVybCgjYSkiIGRhdGEtbmFtZT0ibG9nbyI+PHBhdGggZmlsbD0iIzU4YzA5YiIgZD0iTTYzOC43MzcgMzIxLjc0NWExNi43MzUgMTYuNzM1IDAgMDExNS4zMyA5LjYwNyAxNy4yODEgMTcuMjgxIDAgMDEtMS43MzcgMTguMTI3TDM2MC4yNjEgNzE1LjAwOGExOS4zNjggMTkuMzY4IDAgMDEtNi4yMjkgNC42ODQgMTYuNjI4IDE2LjYyOCAwIDAxLTE1LjExMy0uMTUxIDE3LjA1NyAxNy4wNTcgMCAwMS05LjE5Mi0xOS42MjNsNTIuNTY3LTIwMS4xOTEtMjE2LjI5NC42NjRhMTYuNzM1IDE2LjczNSAwIDAxLTE1LjMzLTkuNjA3Yy0yLjc1OC01Ljg4Ny0yLjA1Ni0xMy4zOCAxLjczNy0xOC4xMjdMNDQ0LjcwOCAxMDYuNjJhMTYuMTc4IDE2LjE3OCAwIDAxMjAuNjE4LTQuNzkzYzcuNDcgMy4xIDExLjM3NiAxMS40NDIgOS42ODYgMTkuMzk0bC01Mi41NjcgMjAxLjE5eiI+PC9wYXRoPjxwYXRoIGZpbGw9Im5vbmUiIHN0cm9rZT0iIzU4YzA5YiIgc3Ryb2tlLXdpZHRoPSI5IiBkPSJNNjM5LjUzNiAzMjguODE1YzYuNTg1LS4wNzEgNTguMjc5IDI3LjY1NSA2MS4wNCAzMy41NDFhMTcuMjkxIDE3LjI5MSAwIDAxLTEuNzQxIDE4LjEyOUw0MDYuNzY3IDc0Ni4wMTJhMTkuMzY2IDE5LjM2NiAwIDAxLTYuMjMgNC42ODQgMTYuNjI4IDE2LjYyOCAwIDAxLTE1LjExMy0uMTUxYy02Ljk3Ni0zLjMzMi01MC4xMzEtMzguNTctNDcuOTQ2LTQ2Ljc1MWw5NC4wNzctMTUyLjYxM0gyMjYuNjc5Yy02LjU4NS4wNzEtNzEuOTc0LTYyLjg0My02OC4xODEtNjcuNTlxMjA1LjM0OS0yNjIuNjkgMjkzLjIwOS0zNzIuNDE0YzkuNTczLTExLjk1NSA1Mi45MjIgMTcuODMxIDYwLjEyMiAyMS42NTQgNy40NyAzLjEgMTEuMzc2IDExLjQ0MiA5LjY4NiAxOS4zOTRsLTUyLjU2NyAyMDEuMTl6Ij48L3BhdGg+PC9nPjwvZz48L3N2Zz4="
                      style={{
                        height: '80px',
                        margin: 'auto',
                        width: '80px',
                      }}
                    />

                    <Card.Body>
                      <Card.Title>Build a Dapp using Sway on Fuel</Card.Title>
                      <Card.Text>
                        In this workshop, you will learn how to design, build,
                        deploy, and use a dapp on the Fuel network using the
                        Fuel toolchain. You will write your smart contract using
                        Fuel's Rust-inspired Sway language.
                      </Card.Text>
                    </Card.Body>
                    <Card.Footer>
                      <small className="text-muted">
                        Skills required: Basic solidity level
                      </small>
                    </Card.Footer>
                    <Card.Footer>
                      <small className="text-muted">
                        Morning session: 9:30am to 12:30pm with Mohammad Fawaz
                        from the Sway team
                      </small>
                    </Card.Footer>
                  </Card>

                  <Card>
                    <Card.Img
                      variant="top"
                      src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iR1JUIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDk2IDk2IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA5NiA5NjsiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPHN0eWxlIHR5cGU9InRleHQvY3NzIj4NCgkuc3Qwe2ZpbGw6IzY3NDdFRDt9DQoJLnN0MXtmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtmaWxsOiNGRkZGRkY7fQ0KPC9zdHlsZT4NCjxjaXJjbGUgY2xhc3M9InN0MCIgY3g9IjQ4IiBjeT0iNDgiIHI9IjQ4Ii8+DQo8ZyBpZD0iU3ltYm9scyI+DQoJPGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTg4LjAwMDAwMCwgLTUyLjAwMDAwMCkiPg0KCQk8cGF0aCBpZD0iRmlsbC0xOSIgY2xhc3M9InN0MSIgZD0iTTEzNS4zLDEwNi4yYy03LjEsMC0xMi44LTUuNy0xMi44LTEyLjhjMC03LjEsNS43LTEyLjgsMTIuOC0xMi44YzcuMSwwLDEyLjgsNS43LDEyLjgsMTIuOA0KCQkJQzE0OC4xLDEwMC41LDE0Mi40LDEwNi4yLDEzNS4zLDEwNi4yIE0xMzUuMyw3NC4yYzEwLjYsMCwxOS4yLDguNiwxOS4yLDE5LjJzLTguNiwxOS4yLTE5LjIsMTkuMmMtMTAuNiwwLTE5LjItOC42LTE5LjItMTkuMg0KCQkJUzEyNC43LDc0LjIsMTM1LjMsNzQuMnogTTE1My42LDExMy42YzEuMywxLjMsMS4zLDMuMywwLDQuNWwtMTIuOCwxMi44Yy0xLjMsMS4zLTMuMywxLjMtNC41LDBjLTEuMy0xLjMtMS4zLTMuMywwLTQuNWwxMi44LTEyLjgNCgkJCUMxNTAuMywxMTIuMywxNTIuNCwxMTIuMywxNTMuNiwxMTMuNnogTTE2MSw3Ny40YzAsMS44LTEuNCwzLjItMy4yLDMuMmMtMS44LDAtMy4yLTEuNC0zLjItMy4yczEuNC0zLjIsMy4yLTMuMg0KCQkJQzE1OS41LDc0LjIsMTYxLDc1LjYsMTYxLDc3LjR6Ii8+DQoJPC9nPg0KPC9nPg0KPC9zdmc+DQo="
                      style={{
                        height: '80px',
                        margin: 'auto',
                        width: '80px',
                      }}
                    />
                    <Card.Body>
                      <Card.Title>
                        Web3 &amp; Graph Protocol workshop with Nader Dabit
                      </Card.Title>
                      <Card.Text>
                        Writing your Smart Contract is only half the work, you
                        also need a Web3 frontend and a way to query this
                        contract easily. In this workshop, you will learn how to
                        index your contract in order to query it easily in you
                        Web3 app. This allows building powerful Web3 dapp with
                        great UX.
                      </Card.Text>
                    </Card.Body>
                    <Card.Footer>
                      <small className="text-muted">
                        Skills required: Basic solidity and frontend level
                      </small>
                    </Card.Footer>
                    <Card.Footer>
                      <small className="text-muted">
                        Afternoon session: 1:30pm to 4:30pm with Nader Dabit
                      </small>
                    </Card.Footer>
                  </Card>
                </CardDeck>

                <CardDeck
                  style={{ marginTop: '20px' }}
                  id="defi-initiation-gton-workshop"
                >
                  <Card>
                    <Card.Img
                      variant="top"
                      src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA0ODIgMjU2IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA0ODIgMjU2IiB4bWw6c3BhY2U9InByZXNlcnZlIj48Zz48cGF0aCBkPSJNMzkxLjUgNjkuNVY2OWgtLjdjLTIgMC0zLjYtMS41LTMuNy0zLjRWMzIuOEgzOThjMS44LjEgMy4zIDEuNSAzLjMgMy4zdjFoLjZ2LTYuOWMtMS4xLjQtNC40IDEtNi45IDFoLTE5LjRjLTIuNSAwLTUuOC0uNi02LjktMVYzN2guNnYtMWMwLTEuOCAxLjUtMy4yIDMuMy0zLjNoMTAuOXYzMi44Yy0uMSAxLjktMS43IDMuNC0zLjcgMy40aC0uN3YuNWgxMi40em0tMTIzLjYgMFY2OWgtLjVjLTEuNSAwLTMtMS4yLTIuMi0zLjRsMy45LTkuNGgxNS41bDMuNyA5LjNjLjggMi4yLS43IDMuNC0yLjIgMy40aC0uNXYuNWgxMi45VjY5aC0uNWMtMS44IDAtMy45LTEtNS4xLTMuNGwtMTUuNC0zNS4xaC0uNWMwIDEuMi0yLjEgNS42LTIuMSA1LjZsLTEzIDI5LjVjLTEuMiAyLjQtMy4zIDMuNC01IDMuNGgtLjV2LjVoMTEuNXptLTM2LjUuOGM4IDAgMTMuMS0zLjQgMTYuMi04LjJsLjctOC42aC0uNWMtMS4zIDExLjItOC4yIDE1LjEtMTYuNCAxNS4xLTkuNy0uMi0xNi4yLTcuNC0xNi4yLTE4LjQgMC0xMC45IDYuNC0xOC4yIDE2LjUtMTguMiA2LjkgMCAxMi41IDMuNCAxNC4zIDkuOGguNWwtMS4yLTcuN2MtMy40LTIuMS04LTMuNy0xMy42LTMuNy0xMi42IDAtMjAuOSA4LTIwLjkgMTkuOS4xIDEyLjEgOC42IDE5LjkgMjAuNiAyMHptMzguMy0xNS43aDE0LjJsLTctMTcuNi03LjIgMTcuNnptNDkuOSAxNC45VjY5aC0uN2MtMiAwLTMuNi0xLjUtMy43LTMuNFYzMi44aDYuNmM0LjkgMCA3LjYgMy40IDcuOCA4LjQgMCAxLjItLjEgMi41LS40IDMuMy0yLjYgNy44LTEyLjEgNS45LTEyLjEgNS45di41YzcuMiAzLjYgMTYuNy0uOCAxNi43LTkuMiAwLTYuNi00LjUtMTAuNi0xMS45LTEwLjZoLTE1di41aC43YzEuOSAwIDMuNSAxLjQgMy43IDMuMnYzMC42YzAgMS45LTEuNyAzLjQtMy43IDMuNGgtLjZ2LjVoMTIuNnptMzguNi0uNXYuNWgtMTIuNlY2OWguN2MyIDAgMy42LTEuNSAzLjctMy40VjM1LjFjLS4xLTEuOS0xLjctMy40LTMuNy0zLjRoLS43di0uNWgxMi42di41aC0uN2MtMiAwLTMuNiAxLjUtMy43IDMuNHYzMC40YzAgMS45IDEuNyAzLjQgMy43IDMuNGguN3ptNTcuNCAwdi41SDQwNFY2OWguNWMxLjcgMCAzLjktMSA1LTMuNGwxMy0yOS41czIuMS00LjQgMi4xLTUuNmguNWwxNS40IDM1LjFjMS4yIDIuNCAzLjMgMy40IDUuMSAzLjRoLjV2LjVoLTEyLjlWNjloLjVjMS41IDAgMy0xLjIgMi4yLTMuNGwtMy43LTkuM2gtMTUuNWwtMy45IDkuNGMtLjggMi4yLjcgMy40IDIuMiAzLjRoLjZ6bTE2LTE0LjRoLTE0LjJsNy4yLTE3LjYgNyAxNy42em01MC4zIDYuMS0zLjQgOC44aC0yMy44VjY5aC43YzIgMCAzLjYtMS41IDMuNy0zLjRWMzUuMmMwLTEuOS0xLjctMy40LTMuNy0zLjRoLS43di0uNWgxMi42di41aC0uN2MtMiAwLTMuNiAxLjUtMy43IDMuNHYzMi43aDguMmM2LjEgMCA4LjQtMyAxMC4yLTcuMmguNnoiIHN0eWxlPSJmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtmaWxsOiMwMjAyMDMiLz48bGluZWFyR3JhZGllbnQgaWQ9ImEiIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIiB4MT0iMS41MjYiIHkxPSIyNTUuOTk4IiB4Mj0iMS40NjYiIHkyPSIyNTYuOTk4IiBncmFkaWVudFRyYW5zZm9ybT0ibWF0cml4KDI1NiAwIDAgLTI1NiAtMjU1IDY1NzkzKSI+PHN0b3Agb2Zmc2V0PSIwIiBzdHlsZT0ic3RvcC1jb2xvcjojZTdiODRiIi8+PHN0b3Agb2Zmc2V0PSIuNDUiIHN0eWxlPSJzdG9wLWNvbG9yOiNmZWU4Y2YiLz48c3RvcCBvZmZzZXQ9IjEiIHN0eWxlPSJzdG9wLWNvbG9yOiNlN2I4NGIiLz48L2xpbmVhckdyYWRpZW50PjxwYXRoIGQ9Ik0yNTYuMSAxMjguMWMwIDIwLjItNC43IDM5LjMtMTMuMSA1Ni4zIDAgLjEtLjEuMS0uMS4yLS4yLjQtLjQuOC0uNCAxLS4xLjItLjIuMy0uMy41LS4xLjItLjIuMy0uMy41LTEwLjIgMTkuNi0yNC44IDM1LjYtNDIgNDcuMi0uMS4xLS4yLjItLjMuMi0uMi4xLS4zLjItLjUuMy0uNC4zLS44LjYtMS4yLjgtLjUuMy0uOS42LTEuNC45LS41LjMtLjkuNi0xLjQuOWwtLjEuMWMtMTkuNSAxMi00Mi40IDE4LjktNjYuOCAxOC45LTIuNCAwLTQuNy0uMS03LjEtLjJoLTFjLS44IDAtMS42LS4xLTIuNC0uMi0uMiAwLS40IDAtLjYtLjEtLjIgMC0uNS0uMS0uNy0uMS0uNy0uMS0xLjMtLjEtMi0uMi0uNSAwLS45LS4xLTEuNC0uMWgtLjJjLS4zIDAtLjYtLjEtLjktLjEtLjMgMC0uNi0uMS0uOS0uMS0uMyAwLS41LS4xLS44LS4xLS4zIDAtLjUtLjEtLjgtLjEtLjMgMC0uNi0uMS0uOS0uMS0uMyAwLS42LS4xLS45LS4xLS4zIDAtLjUtLjEtLjgtLjEtLjMgMC0uNS0uMS0uOC0uMS0uMy0uMS0uNi0uMS0uOS0uMi0uMy0uMS0uNy0uMS0xLS4yLS40LS4xLS44LS4xLTEuMi0uMmgtLjJjLS43LS4xLTEuNS0uMy0yLjItLjUtLjQtLjEtLjgtLjEtMS4xLS4yLTEuMS0uMy0yLjItLjUtMy4zLS44LS4xIDAtLjIgMC0uMy0uMS0uMSAwLS4xIDAtLjItLjEtLjktLjMtMS45LS41LTIuOC0uOC0uNC0uMS0uNy0uMi0xLS4zLS40LS4xLS44LS4yLTEuMS0uMy0uNC0uMS0uOC0uMi0xLjEtLjNsLS45LS4zYy0uMSAwLS4yLS4xLS4yLS4xLS43LS4yLTEuNC0uNS0yLjItLjctLjItLjEtLjQtLjEtLjYtLjItLjItLjEtLjQtLjEtLjYtLjItLjMtLjEtLjctLjMtMS0uNC0uNC0uMS0uNy0uMy0xLS40LS4yLS4xLS4zLS4xLS41LS4ycy0uNS0uMi0uNy0uM2MtLjctLjMtMS40LS41LTIuMS0uOC0uMi0uMS0uNC0uMi0uNi0uMi0uMi0uMS0uNC0uMi0uNi0uMi0uMy0uMS0uNi0uMy0uOS0uNC0uMy0uMS0uNi0uMy0uOS0uNC0xLjItLjUtMi40LTEtMy41LTEuNi0uMSAwLS4xLS4xLS4yLS4xcy0uMS0uMS0uMi0uMWMtLjMtLjEtLjYtLjMtLjgtLjQtLjMtLjEtLjYtLjMtLjktLjUtMS4yLS42LTIuNS0xLjMtMy44LTItLjktLjUtMS43LTEtMi42LTEuNC0uMS0uMS0uMi0uMS0uMy0uMmwtLjEtLjFjLTIyLjItMTIuOC0zOS44LTMxLjYtNTEtNTQuNS0uMS0uMi0uMi0uNS0uMy0uNy0uMS0uMi0uMi0uNS0uMy0uNy0uNC0uOC0uNy0xLjUtMS4xLTIuMy0uNy0xLjUtMS40LTMuMS0yLTQuNyAwLS4xLS4xLS4yLS4xLS4zIDAtLjEtLjEtLjItLjEtLjMtMi4zLTUuNy00LjItMTEuNy01LjYtMTcuOXYtLjJjLS40LTEuNy0uOC0zLjQtMS4xLTUuMS0uMS0uMy0uMS0uNy0uMi0xLS4xLS4zLS4xLS43LS4yLTEtLjItMS4xLS40LTIuMi0uNS0zLjItMi40LTE1LjgtMS43LTMxLjggMS45LTQ3LjQgMC0uMi4xLS4zLjEtLjVzLjEtLjMuMS0uNWMuNC0xLjUuNy0zIDEuMS00LjUuMy0xLjIuNy0yLjUgMS4xLTMuN2wuNi0xLjhjLjUtMS41IDEtMyAxLjYtNC41LjEtLjIuMS0uNC4yLS41LjEtLjIuMS0uNC4yLS41LjYtMS42IDEuMi0zLjIgMS45LTQuOS4xLS4yLjEtLjMuMi0uNXMuMS0uMy4yLS41Yy43LTEuNiAxLjQtMy4xIDIuMS00LjcgMC0uMS4xLS4yLjEtLjMgMC0uMS4xLS4yLjEtLjMuMS0uMy4zLS42LjUtLjkuNi0xLjMgMS4zLTIuNSAyLTMuOC4yLS40LjUtLjkuNy0xLjMuMi0uNC41LS45LjctMS4zIDAtLjEuMS0uMi4yLS4zbC4xLS4xQzMwIDQxLjggNDguOSAyNC4yIDcxLjggMTMuMWMuNS0uMyAxLS41IDEuNS0uNy40LS4yLjgtLjMgMS4xLS41LjQtLjIuOC0uNCAxLjEtLjUgMS41LS43IDMuMS0xLjQgNC43LTIgLjEgMCAuMi0uMS4zLS4xLjEgMCAuMi0uMS4zLS4xQzg2LjYgNi45IDkyLjYgNSA5OC43IDMuNmguMmMxLjctLjQgMy40LS44IDUuMi0xLjEuMy0uMS43LS4xIDEtLjIuMy0uMS42LS4xLjktLjIuNi0uMSAxLjEtLjIgMS43LS4zLjYtLjEgMS4xLS4yIDEuNy0uMyAyNS41LTMuOCA1MS40LjEgNzUgMTEuN2wtMTkuNyA0MC4yYy0xMS42LTUuNy0yNC4xLTguNS0zNi42LTguNS00LjIgMC04LjMuMy0xMi4zLjktLjIgMC0uMy4xLS41LjFzLS4zLjEtLjUuMWMtLjkuMS0xLjkuMy0yLjguNS0uMy4xLS42LjEtLjkuMi0uMy4xLS42LjEtLjkuMi0uMy4xLS42LjItMSAuMi0uMy4xLS42LjEtMSAuMi0uMSAwLS4zLjEtLjQuMS0uNy4yLTEuMy4zLTIgLjUtLjIuMS0uNS4xLS43LjItLjIuMS0uNS4xLS43LjItLjkuMy0xLjguNS0yLjYuOC0uMi4xLS41LjItLjcuMi0uMi4xLS40LjEtLjcuMi0uOC4zLTEuNS42LTIuMy45LS4xIDAtLjMuMS0uNC4yLS41LjItMSAuNC0xLjQuNi0uNC4yLS44LjMtMS4yLjUtLjEuMS0uMi4xLS40LjItLjkuNC0xLjguOC0yLjYgMS4yLS4xIDAtLjIuMS0uMi4xLS4xIDAtLjIuMS0uMi4xLTE1LjYgNy42LTI4LjYgMjAtMzYuOSAzNS4ydi4yYy0uNiAxLjEtMS4xIDIuMi0xLjcgMy4zbC0uMy42LS4zLjZjLS40LjgtLjggMS42LTEuMSAyLjUtLjEuMi0uMi41LS4zLjctLjEuMi0uMi41LS4zLjctLjEuMy0uMi41LS4zLjgtLjIuNS0uNSAxLjEtLjYgMS42IDAgLjEtLjEuMy0uMS40IDAgLjEtLjEuMy0uMS40LTMuMSA4LjctNC44IDE4LjEtNC44IDI3LjggMCA0LjIuMyA4LjMuOSAxMi40IDAgLjEgMCAuMy4xLjQgMCAuMSAwIC4zLjEuNC4yIDEgLjMgMiAuNSAyLjkuMS42LjIgMS4xLjQgMS43LjEuNi4yIDEuMS40IDEuNyAwIC4xLjEuMi4xLjMuMS41LjMgMS4xLjQgMS42LjEuMi4xLjUuMi43LjEuMy4yLjYuMi45LjEuMi4xLjQuMi42LjEuNS4zIDEuMS41IDEuNmwuMy45Yy4yLjUuMyAxIC41IDEuNC4zLjguNSAxLjUuOCAyLjIuMS4zLjMuNi40LjkuMS4zLjIuNi40LjkuMS4yLjIuNS4zLjcuMS4yLjIuNS4zLjdsMS4yIDIuN2MuMS4xLjIuMy4yLjQgNy42IDE1LjcgMjAgMjguNiAzNS4zIDM2LjkgMS4xLjYgMi4zIDEuMiAzLjQgMS44LjMuMi43LjMgMSAuNS4yLjEuMy4xLjUuMi44LjQgMS41LjcgMi4yIDEgLjYuMyAxLjIuNSAxLjguNy4yLjEuMy4xLjUuMmwxLjUuNmMxLjEuNCAyLjEuOCAzLjEgMS4xLjIuMS41LjIuOC4zIDEuMi40IDIuNS44IDMuOCAxLjFoLjJjMS4xLjMgMi4yLjUgMy4zLjguMiAwIC40LjEuNi4xaC4xYy4zLjEuNy4xIDEgLjIuMy4xLjcuMSAxIC4yLjMgMCAuNy4xIDEgLjIuMy4xLjcuMSAxIC4yLjYuMSAxLjIuMiAxLjguMi4zIDAgLjUuMS44LjEuNS4xIDEgLjEgMS41LjIuNCAwIC43LjEgMS4xLjEuMyAwIC43IDAgMSAuMS42LjEgMS4zLjEgMS45LjIuNiAwIDEuMi4xIDEuOC4xaDIuM2MxLjIgMCAyLjQgMCAzLjUtLjFoLjRjMTItLjUgMjMuOC0zLjYgMzQuNS05LjFoLjJjMS0uNSAyLTEgMy0xLjZsMS0uNmMuNy0uNCAxLjQtLjggMi0xLjJsLjktLjYuOS0uNmMuMi0uMS40LS4yLjUtLjQuMi0uMS40LS4yLjUtLjQuOS0uNiAxLjgtMS4yIDIuNy0xLjlsLjEtLjFzLjEgMCAuMS0uMWM5LjUtNy4xIDE3LjUtMTYuMyAyMy4zLTI2LjkgMC0uMS4xLS4yLjItLjMuMS0uMS4xLS4yLjItLjMgNi4zLTExLjcgOS44LTI1IDkuOC0zOS4xaDQ0Ljd6bS00NC44LTQ1LjdoLTQ1Ljd2NDUuN2g0NS43VjgyLjR6IiBzdHlsZT0iZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7ZmlsbDp1cmwoI2EpIi8+PC9nPjwvc3ZnPg=="
                      style={{
                        margin: 'auto',
                        marginTop: 15,
                        width: '160px',
                      }}
                    />
                    <Card.Body>
                      <Card.Title>
                        Initiation to DeFi with the GTON Capital Team (for
                        Traders, VCs and non-devs)
                      </Card.Title>
                      <Card.Text>
                        Non-technical workshop for traders, VCs, product
                        managers. We will review the most popular DeFi
                        applications and learn basic usage patterns, such as
                        withdrawing from CEX, taking a loan on AAVE, doing a
                        swap on Uniswap, investing in Yearn, and so on. The
                        workshop will cover six protocols: 1. AAVE: lending, 2.
                        Yearn: staking, 3. Univ3: liquidity provision, 4.
                        Sushiswap: trading, 5. Gearbox: leverage trading and
                        farming, 6. Curve: stablecoin swaps.
                      </Card.Text>
                    </Card.Body>
                    <Card.Footer>
                      <small className="text-muted">
                        Skills required: No programming skills required
                      </small>
                    </Card.Footer>
                    <Card.Footer>
                      <small className="text-muted">
                        Morning session: 9:30am to 12:30pm with Ilya Sapranidi |
                        GC and Alex P | GC from GTON Capital team
                      </small>
                    </Card.Footer>
                    <Card.Footer>
                      <small className="text-muted">
                        Afternoon session: 1:30pm to 4:30pm with Ilya Sapranidi
                        | GC
                      </small>
                    </Card.Footer>
                  </Card>
                  <Card>
                    <Card.Img
                      variant="top"
                      src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iODAiIGhlaWdodD0iNjguMzMzIiBmaWxsPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGQ9Ik04MzguNTc3IDU1MS4xOTggNTE5LjYwMiA4Ljg3NmExNy42NTMgMTcuNjUzIDAgMCAwLTYuNDg0LTYuNDY3QTE4LjA3OSAxOC4wNzkgMCAwIDAgNTA0LjIwNiAwYTE4LjA4IDE4LjA4IDAgMCAwLTguOTQyIDIuMjk3IDE3LjY1OCAxNy42NTggMCAwIDAtNi41NjcgNi4zODVsLTk1LjUzIDE2Mi41MDNhMzQuNDkyIDM0LjQ5MiAwIDAgMC00Ljc3NSAxNy40OTYgMzQuNDk3IDM0LjQ5NyAwIDAgMCA0Ljc3NSAxNy40OTZMNjAxLjE1OSA1NTkuOTdhMzUuMzM0IDM1LjMzNCAwIDAgMCAxMy4wNjMgMTIuODE2IDM2LjE1NSAzNi4xNTUgMCAwIDAgMTcuODQyIDQuNjhoMTkxLjA2YTE4LjEzNiAxOC4xMzYgMCAwIDAgOC45MDUtMi4zNjMgMTcuNzE2IDE3LjcxNiAwIDAgMCA2LjUxOS02LjQwOCAxNy4yOTYgMTcuMjk2IDAgMCAwIDIuMzk0LTguNzQ1Yy4wMDYtMy4wNy0uODEtNi4wODgtMi4zNjUtOC43NTJ6IiBmaWxsPSJ1cmwoI2EpIiBzdHlsZT0iZmlsbDp1cmwoI2EpIiB0cmFuc2Zvcm09InNjYWxlKC4wNzkzNykiLz48cGF0aCBkPSJtMi40NiA4MzQuNjM1IDMxOC45NzQtNTQyLjMyMWExNy42NjcgMTcuNjY3IDAgMCAxIDYuNTI0LTYuMzk2IDE4LjA2NiAxOC4wNjYgMCAwIDEgOC45MDQtMi4zNDFjMy4xMjYgMCA2LjE5OC44MDcgOC45MDYgMi4zNDFhMTcuNjU0IDE3LjY1NCAwIDAgMSA2LjUyMiA2LjM5Nmw5NS41OCAxNjIuMzU3YTM0LjU5MyAzNC41OTMgMCAwIDEgNC43NzQgMTcuNTJjMCA2LjE1LTEuNjQ2IDEyLjE5Mi00Ljc3NCAxNy41MkwyMzkuODc2IDg0My41MDRhMzUuMjIgMzUuMjIgMCAwIDEtMTMuMDMzIDEyLjgxOEEzNi4wNjYgMzYuMDY2IDAgMCAxIDIwOS4wMiA4NjFIMTcuOTEyYTE4LjA5MSAxOC4wOTEgMCAwIDEtOC45NzMtMi4zMjggMTcuNjcyIDE3LjY3MiAwIDAgMS02LjU2NC02LjQ0MkExNy4yNSAxNy4yNSAwIDAgMSAwIDg0My40MjFjLjAxNS0zLjA5Ljg2NC02LjEyMiAyLjQ2LTguNzg2WiIgZmlsbD0idXJsKCNiKSIgc3R5bGU9ImZpbGw6dXJsKCNiKSIgdHJhbnNmb3JtPSJzY2FsZSguMDc5MzcpIi8+PHBhdGggZD0iTTM1Mi4yNDUgODYwLjg4NGg2MzcuOTQ5YTE4LjA4MiAxOC4wODIgMCAwIDAgOC45MTItMi4zNTQgMTcuNjc4IDE3LjY3OCAwIDAgMCA2LjUxNC02LjQxMyAxNy4zMTMgMTcuMzEzIDAgMCAwIDIuMzgtOC43NTRjMC0zLjA3Mi0uODMtNi4wODktMi40LTguNzQ3bC05NS40MzMtMTYyLjQ1NGEzNS4zMzQgMzUuMzM0IDAgMCAwLTEzLjA2Mi0xMi44MTcgMzYuMTY4IDM2LjE2OCAwIDAgMC0xNy44NDMtNC42NzlINDYzLjI3NmEzNi4xNjIgMzYuMTYyIDAgMCAwLTE3Ljg0MiA0LjY3OSAzNS4zMjIgMzUuMzIyIDAgMCAwLTEzLjA2MyAxMi44MTdsLTk1LjUyOSAxNjIuNDU0YTE3LjI0NiAxNy4yNDYgMCAwIDAtMi40MDIgOC43NDcgMTcuMjQ4IDE3LjI0OCAwIDAgMCAyLjM3NSA4Ljc1NCAxNy42NjkgMTcuNjY5IDAgMCAwIDYuNTE5IDYuNDEzIDE4LjA3NiAxOC4wNzYgMCAwIDAgOC45MTEgMi4zNTR6IiBmaWxsPSJ1cmwoI2MpIiBzdHlsZT0iZmlsbDp1cmwoI2MpIiB0cmFuc2Zvcm09InNjYWxlKC4wNzkzNykiLz48ZGVmcz48bGluZWFyR3JhZGllbnQgaWQ9ImEiIHgxPSI1MDQiIHkxPSI2NCIgeDI9IjQxNS4wNDQiIHkyPSI4NjEuMDYxIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSI+PHN0b3Agc3RvcC1jb2xvcj0iIzA1RDVGRiIvPjxzdG9wIG9mZnNldD0iLjcyNCIgc3RvcC1jb2xvcj0iIzM2M0ZGOSIvPjxzdG9wIG9mZnNldD0iMSIgc3RvcC1jb2xvcj0iIzUzRiIvPjwvbGluZWFyR3JhZGllbnQ+PGxpbmVhckdyYWRpZW50IGlkPSJiIiB4MT0iNTA0IiB5MT0iNjQiIHgyPSI0MTUuMDQ0IiB5Mj0iODYxLjA2MSIgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiPjxzdG9wIHN0b3AtY29sb3I9IiMwNUQ1RkYiLz48c3RvcCBvZmZzZXQ9Ii43MjQiIHN0b3AtY29sb3I9IiMzNjNGRjkiLz48c3RvcCBvZmZzZXQ9IjEiIHN0b3AtY29sb3I9IiM1M0YiLz48L2xpbmVhckdyYWRpZW50PjxsaW5lYXJHcmFkaWVudCBpZD0iYyIgeDE9IjUwNCIgeTE9IjY0IiB4Mj0iNDE1LjA0NCIgeTI9Ijg2MS4wNjEiIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIj48c3RvcCBzdG9wLWNvbG9yPSIjMDVENUZGIi8+PHN0b3Agb2Zmc2V0PSIuNzI0IiBzdG9wLWNvbG9yPSIjMzYzRkY5Ii8+PHN0b3Agb2Zmc2V0PSIxIiBzdG9wLWNvbG9yPSIjNTNGIi8+PC9saW5lYXJHcmFkaWVudD48L2RlZnM+PC9zdmc+"
                      style={{
                        margin: 'auto',
                        marginTop: 15,
                        width: '80px',
                      }}
                    />
                    <Card.Body>
                      <Card.Title>
                        How to Code and Deploy a Generative NFT Collection with
                        Alchemy
                      </Card.Title>
                      <Card.Text>
                        In this workshop, you will learn how to design, code,
                        deploy, and view a 10,000-piece generative NFT
                        collection. We'll teach you how to write the smart
                        contract for minting, as well as a website for your
                        collectors to view their NFTs. All using the Alchemy
                        developer platform!
                      </Card.Text>
                    </Card.Body>
                    <Card.Footer>
                      <small className="text-muted">
                        Skills required: Basic solidity and frontend skills.
                      </small>
                    </Card.Footer>
                    <Card.Footer>
                      <small className="text-muted">
                        Afternoon session: 1:30pm to 4:30pm with Albert Hu from
                        the Alchemy team
                      </small>
                    </Card.Footer>
                  </Card>
                  <Card>
                    <Card.Img
                      variant="top"
                      src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzNy44IDQzLjYiPjxkZWZzPjxzdHlsZT4uY2xzLTF7ZmlsbDojMmE1YWRhO308L3N0eWxlPjwvZGVmcz48dGl0bGU+QXNzZXQgMTwvdGl0bGU+PGcgaWQ9IkxheWVyXzIiIGRhdGEtbmFtZT0iTGF5ZXIgMiI+PGcgaWQ9IkxheWVyXzEtMiIgZGF0YS1uYW1lPSJMYXllciAxIj48cGF0aCBjbGFzcz0iY2xzLTEiIGQ9Ik0xOC45LDBsLTQsMi4zTDQsOC42LDAsMTAuOVYzMi43TDQsMzVsMTEsNi4zLDQsMi4zLDQtMi4zTDMzLjgsMzVsNC0yLjNWMTAuOWwtNC0yLjNMMjIuOSwyLjNaTTgsMjguMVYxNS41TDE4LjksOS4ybDEwLjksNi4zVjI4LjFMMTguOSwzNC40WiIvPjwvZz48L2c+PC9zdmc+"
                      style={{
                        height: '80px',
                        margin: 'auto',
                        width: '80px',
                      }}
                    />
                    <Card.Body>
                      <Card.Title>
                        Chainlink your smart contracts: data feeds, vrf, keepers
                      </Card.Title>
                      <Card.Text>
                        In this workshop you will learn from the Chainlink team
                        how to use Chainlink oracles feeds and keepers.
                      </Card.Text>
                    </Card.Body>
                    <Card.Footer>
                      <small className="text-muted">
                        Skills required: Basic solidity skills
                      </small>
                    </Card.Footer>
                    <Card.Footer>
                      <small className="text-muted">
                        Afternoon session: 1:30pm to 4:30pm with Solange Gueiros
                        from the Chainlink Team
                      </small>
                    </Card.Footer>
                  </Card>
                </CardDeck>
              </Col>
            </Row>
          </Container>
          <hr />
          <h3
            style={{
              textDecoration: 'underline dashed',
              textUnderlineOffset: '5px',
            }}
            id="day3"
          >
            March 31st
          </h3>
          <h4 style={{ margin: '30px' }}>
            Listen and learn from the best builders and contributors in the
            Ethereum and DeFi ecosystem!
          </h4>
        </div>
        <div className="schedule_content">
          <div className="row">
            <div className="col-md-4">
              <ul className="nav nav-tabs" id="myTab" role="tablist">
                {schedule.map((day, i) => (
                  <li className="nav-item" key={i}>
                    <Link
                      className={`nav-link ${
                        currentScheduleTab === i ? 'active' : null
                      }`}
                      data-toggle="tab"
                      to={`/#day-${i + 1}`}
                      data-scroll-ignore="true"
                      role="tab"
                      aria-controls="schedule-wrapper"
                      onClick={(e) => {
                        setCurrentScheduleTab(i)
                        if (document) {
                          let dayd = document.getElementById(
                            'day-' + (i + 1)
                          ).offsetTop
                          let scrolldiv =
                            document.getElementById('schedule-scroll')
                          scrolldiv.scrollTop = dayd - 120
                        }
                        e.preventDefault()
                      }}
                    >
                      {' '}
                      <span key={i}>
                        {new Date(day.date).toLocaleString('default', {
                          month: 'long',
                        })}{' '}
                        {getDayActivityDate(i)}
                      </span>{' '}
                      - DAY {i + 1} {getDayActivity(i)}
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
            <div className="col-md-8">
              <div className="tab-content">
                <div
                  className="tab-pane active"
                  id="schedule-wrapper"
                  role="tabpanel"
                >
                  <div className="schedule_tab_box">
                    <div className="schedule_search">
                      <div className="search">
                        <input
                          type="text"
                          name="search"
                          placeholder="Search schedule"
                          value={scheduleQuery}
                          onChange={(e) => {
                            setScheduleQuery(e.target.value)
                          }}
                        />
                        <button>
                          <i className="fa fa-search" aria-hidden="true"></i>
                        </button>
                        <button
                          className="searchButton"
                          style={{ right: -60 }}
                          onClick={() => {
                            setScheduleQuery('track1')
                          }}
                        >
                          Track 1
                        </button>
                        <button
                          className="searchButton"
                          style={{ right: -125 }}
                          onClick={() => {
                            setScheduleQuery('track2')
                          }}
                        >
                          Track 2
                        </button>
                      </div>
                    </div>
                    <div className="tab_content_inner">
                      <div id="schedule-scroll" className="tab_scroller">
                        {event.groupedSchedule.map((day, i) => (
                          <>
                            <h3 id={'day-' + (i + 1)} key={i}>
                              {new Date(day.date).toLocaleString('default', {
                                weekday: 'long',
                              })}
                              , {getDayActivityDate(i)}{' '}
                              {new Date(day.date).toLocaleString('default', {
                                month: 'long',
                              })}
                            </h3>
                            {day.slots
                              .sort((a, b) =>
                                a.startDate + getTagsTrack(a) <
                                b.startDate + getTagsTrack(b)
                                  ? -1
                                  : 1
                              )
                              .map((slot, i) => {
                                let slots = day.slots
                                if (
                                  slots[i + 1] &&
                                  slots[i + 1].startDate ===
                                    slots[i].startDate &&
                                  slot.title.indexOf('[Discovery Track]') !== -1
                                ) {
                                  slot = slots[i + 1]
                                }
                                if (
                                  slots[i - 1] &&
                                  slots[i - 1].startDate ===
                                    slots[i].startDate &&
                                  slots[i - 1].title.indexOf(
                                    '[Discovery Track]'
                                  ) !== -1
                                ) {
                                  slot = slots[i - 1]
                                }
                                const slot_slug = `${slot.id}-${slot.title
                                  .toString()
                                  .toLowerCase()
                                  .trim()
                                  .replace(/&/g, '-and-')
                                  .replace(/[\s\W-]+/g, '-')}`
                                return scheduleQuery === '' ||
                                  slot.title
                                    .toLowerCase()
                                    .indexOf(scheduleQuery.toLowerCase()) !==
                                    -1 ||
                                  slot.description
                                    .toLowerCase()
                                    .indexOf(scheduleQuery.toLowerCase()) !==
                                    -1 ||
                                  slot.tags.indexOf(scheduleQuery) !== -1 ? (
                                  <div
                                    className="tab_text first-tab"
                                    id={slot_slug}
                                    key={i}
                                  >
                                    <div className="border_box_tab">
                                      <h5>
                                        {new Date(slot.startDate)
                                          .toLocaleTimeString('default', {
                                            hour12: false,
                                            timeZone: event.timezoneId,
                                          })
                                          .split(':')[0] +
                                          ':' +
                                          new Date(slot.startDate)
                                            .toLocaleTimeString('default', {
                                              hour12: false,
                                            })
                                            .split(':')[1]}
                                        {' - '}
                                        {new Date(
                                          new Date(slot.startDate).setMinutes(
                                            new Date(
                                              slot.startDate
                                            ).getMinutes() + slot.length
                                          )
                                        )
                                          .toLocaleTimeString('default', {
                                            hour12: false,
                                            timeZone: event.timezoneId,
                                          })
                                          .split(':')[0] +
                                          ':' +
                                          new Date(
                                            new Date(slot.startDate).setMinutes(
                                              new Date(
                                                slot.startDate
                                              ).getMinutes() + slot.length
                                            )
                                          )
                                            .toLocaleTimeString('default', {
                                              hour12: false,
                                            })
                                            .split(':')[1]}{' '}
                                        (Dubai time, UTC+4)
                                      </h5>
                                      <h4
                                        style={{
                                          backgroundColor:
                                            slot.title.indexOf(
                                              '[Discovery Track]'
                                            ) !== -1
                                              ? '#f5f3f3'
                                              : null,
                                        }}
                                      >
                                        <Link
                                          to={`#slot-${slot_slug}`}
                                          replace
                                          data-scroll-ignore
                                          onClick={(e) => {
                                            e.preventDefault()
                                            //   navigate(`#slot-${slot_slug}`)
                                          }}
                                        >
                                          {slot.title} {getTags(slot)}
                                        </Link>
                                      </h4>
                                      <ReactMarkdown
                                        children={slot.description}
                                      />
                                      {slot.speakers.map((speaker, i) => (
                                        <div
                                          className="tab_profile_inner_box"
                                          key={i}
                                        >
                                          <div className="row no-gutters">
                                            <div className="col-md-2">
                                              <div className="tab_profile_inner_box_image">
                                                <Img
                                                  fluid={
                                                    speaker.localFile
                                                      .childImageSharp.fluid
                                                  }
                                                />
                                              </div>
                                            </div>
                                            <div className="col-md-10">
                                              <div className="tab_profile_inner_box_content">
                                                <div className="name_icon">
                                                  <div className="name">
                                                    <h2>{speaker.name}</h2>
                                                  </div>
                                                  <div className="tab_icons">
                                                    <ul>
                                                      {speaker.twitter !==
                                                      '' ? (
                                                        <li>
                                                          <a
                                                            href={`https://twitter.com/${speaker.twitter}`}
                                                            className="icon-social-button-small"
                                                          >
                                                            <i className="fa fa-twitter icon-twitter"></i>
                                                          </a>
                                                        </li>
                                                      ) : null}
                                                      <li>
                                                        <a
                                                          href={`https://github.com/${speaker.github}`}
                                                          className="icon-social-button-small"
                                                        >
                                                          <i className="fa fa-github icon-github"></i>
                                                        </a>
                                                      </li>
                                                    </ul>
                                                  </div>
                                                </div>
                                                <ReactMarkdown
                                                  children={speaker.bio}
                                                />
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      ))}
                                    </div>
                                  </div>
                                ) : null
                              })}
                          </>
                        ))}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
