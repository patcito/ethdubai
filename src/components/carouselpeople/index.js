import React from 'react'
import { useState } from 'react'
import Img from 'gatsby-image'
import { useStaticQuery, graphql } from 'gatsby'

export default function CarouselPeople() {
  const [show, setShow] = useState(false)
  const [eventProps, setEventProps] = useState({
    title: '',
    date: '',
    description: '',
    hours: '',
    pictures: [],
  })

  const handleClose = () => setShow(false)

  const imgs = useStaticQuery(graphql`
    {
      quote: file(relativePath: { eq: "test_qoute.png" }) {
        childImageSharp {
          fixed(width: 30) {
            ...GatsbyImageSharpFixed_withWebp
          }
        }
      }
    }
  `)

  return (
    <>
      {/* <Modal show={show} onHide={handleClose} id="event_popup">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button
                onClick={handleClose}
                type="button"
                className="close"
                data-dismiss="modal"
              >
                &times;
              </button>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-md-5">
                  <div className="event_popup_content">
                    <h3 id="event-date-popup">{eventProps.date}</h3>
                    <h4>MAY</h4>
                    <h5 id="event-title-popup">{eventProps.title}</h5>
                    <h6>
                      {' '}
                      <span id="event-hours-popup">{eventProps.hours}</span>
                    </h6>
                    <p id="event-description-popup">{eventProps.description}</p>
                  </div>
                </div>
                <div className="col-md-7">
                  <div className="event_popup_images">
                    <ul>
                      <li>
                        <img
                          id="event-pic1-popup"
                          src={eventProps.pictures[0]}
                          alt=""
                        />
                      </li>
                      <li>
                        <img
                          id="event-pic2-popup"
                          src={eventProps.pictures[1]}
                          alt=""
                        />
                      </li>
                      <li>
                        <img
                          id="event-pic3-popup"
                          src={eventProps.pictures[2]}
                          alt=""
                        />
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal> */}
      <div className="horizontal-carousel">
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ethdubaiconf
                </a>
                - the smoothest conference I have been too! Highly recommend{' '}
                <a href="https://t.co/lYwSAdMUBH">pic.twitter.com/lYwSAdMUBH</a>
              </p>
              &mdash; albertorio.eth - (⛓ , 📖, 🤔 ) (@Tesla809){' '}
              <a href="https://twitter.com/Tesla809/status/1509617621704093707?ref_src=twsrc%5Etfw">
                March 31, 2022
              </a>
            </blockquote>{' '}
          </div>
        </div>

        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                Thank you{' '}
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ETHDubaiConf
                </a>{' '}
                for the awesome week. Very well organised event with fantastic
                speakers! Saying goodbye to Dubai for now 👋🌆{' '}
                <a href="https://t.co/0EInplOLNX">pic.twitter.com/0EInplOLNX</a>
              </p>
              &mdash; Jamie Hewitt (@Jamie_Hewitt_){' '}
              <a href="https://twitter.com/Jamie_Hewitt_/status/1509950555862540288?ref_src=twsrc%5Etfw">
                April 1, 2022
              </a>
            </blockquote>
          </div>
        </div>

        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                thanks for{' '}
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ETHDubaiConf
                </a>{' '}
                organizing such an amazing event{' '}
                <a href="https://twitter.com/hashtag/EthDubaiconf?src=hash&amp;ref_src=twsrc%5Etfw">
                  #EthDubaiconf
                </a>{' '}
                <a href="https://t.co/jpPYvd9D7p">pic.twitter.com/jpPYvd9D7p</a>
              </p>
              &mdash; lei.eth in California (@snow668899){' '}
              <a href="https://twitter.com/snow668899/status/1509555171524759558?ref_src=twsrc%5Etfw">
                March 31, 2022
              </a>
            </blockquote>
          </div>
        </div>

        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <p lang="uk" dir="ltr">
              🧑‍💻
              <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                @ETHDubaiConf
              </a>{' '}
              був надихаючим, показовим, пізнавальним і веселим!
              <br />
              🤝 Дякуємо, Андрій Пітіш, що приєднався до нас і був таким
              надійним партнером!
              <a href="https://twitter.com/hashtag/BLAST?src=hash&amp;ref_src=twsrc%5Etfw">
                #BLAST
              </a>{' '}
              <a href="https://twitter.com/hashtag/BwareLabs?src=hash&amp;ref_src=twsrc%5Etfw">
                #BwareLabs
              </a>{' '}
              <a href="https://twitter.com/hashtag/ETHDubai?src=hash&amp;ref_src=twsrc%5Etfw">
                #ETHDubai
              </a>{' '}
              <a href="https://t.co/wY8JizkJeg">pic.twitter.com/wY8JizkJeg</a>
            </p>
            &mdash; Bware Labs UA (@BwareLabsUa){' '}
            <a href="https://twitter.com/BwareLabsUa/status/1514643767646695430?ref_src=twsrc%5Etfw">
              April 14, 2022
            </a>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                We&#39;re sending{' '}
                <a href="https://twitter.com/YSoenggoro?ref_src=twsrc%5Etfw">
                  @YSoenggoro
                </a>{' '}
                off tonight. Have a safe flight bro! 🛫
                <br />
                Just a quick Moralis Crew snap after having a blast at{' '}
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ETHDubaiConf
                </a>
                . 🙌{' '}
                <a href="https://t.co/6In0dZVEHs">pic.twitter.com/6In0dZVEHs</a>
              </p>
              &mdash; Moralis Web3 - Ultimate Web3 Dev Workflow (@MoralisWeb3){' '}
              <a href="https://twitter.com/MoralisWeb3/status/1509575498975911937?ref_src=twsrc%5Etfw">
                March 31, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                <a href="https://twitter.com/hashtag/ETHDubai?src=hash&amp;ref_src=twsrc%5Etfw">
                  #ETHDubai
                </a>{' '}
                was based.{' '}
                <a href="https://twitter.com/MidhavVR?ref_src=twsrc%5Etfw">
                  @MidhavVR
                </a>{' '}
                can agree.{' '}
                <a href="https://t.co/UdswXg9nv9">pic.twitter.com/UdswXg9nv9</a>
              </p>
              &mdash; Vansh Wassan (@WassanVansh){' '}
              <a href="https://twitter.com/WassanVansh/status/1511706694518456321?ref_src=twsrc%5Etfw">
                April 6, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                Attended{' '}
                <a href="https://twitter.com/hashtag/IRL?src=hash&amp;ref_src=twsrc%5Etfw">
                  #IRL
                </a>{' '}
                events last week in Dubai at{' '}
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ETHDubaiConf
                </a>
                . Met some of the amazing people in the{' '}
                <a href="https://twitter.com/hashtag/web3community?src=hash&amp;ref_src=twsrc%5Etfw">
                  #web3community
                </a>{' '}
                and{' '}
                <a href="https://twitter.com/hashtag/Blockchain?src=hash&amp;ref_src=twsrc%5Etfw">
                  #Blockchain
                </a>{' '}
                space including{' '}
                <a href="https://twitter.com/iampbiswal?ref_src=twsrc%5Etfw">
                  @iampbiswal
                </a>{' '}
                <a href="https://twitter.com/acknoledger?ref_src=twsrc%5Etfw">
                  @acknoledger
                </a>{' '}
                and many more.{' '}
                <a href="https://t.co/qrS0Wj2uQx">pic.twitter.com/qrS0Wj2uQx</a>
              </p>
              &mdash; Eximius Ventures (@eximiusvc){' '}
              <a href="https://twitter.com/eximiusvc/status/1511583849347977216?ref_src=twsrc%5Etfw">
                April 6, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ETHDubaiConf
                </a>{' '}
                2022, thanks for bringing so many talented individuals, insights
                and great vibe{' '}
                <a href="https://twitter.com/chain_security?ref_src=twsrc%5Etfw">
                  @chain_security
                </a>{' '}
                <a href="https://twitter.com/Zircon_Finance?ref_src=twsrc%5Etfw">
                  @Zircon_Finance
                </a>{' '}
                <a href="https://twitter.com/MixBytes?ref_src=twsrc%5Etfw">
                  @MixBytes
                </a>{' '}
                <a href="https://twitter.com/Nexo?ref_src=twsrc%5Etfw">@Nexo</a>{' '}
                <a href="https://twitter.com/iearnfinance?ref_src=twsrc%5Etfw">
                  @iearnfinance
                </a>{' '}
                <a href="https://twitter.com/GtonCapital?ref_src=twsrc%5Etfw">
                  @GtonCapital
                </a>{' '}
                <a href="https://twitter.com/ClearpoolFin?ref_src=twsrc%5Etfw">
                  @ClearpoolFin
                </a>{' '}
                <a href="https://twitter.com/acknoledger?ref_src=twsrc%5Etfw">
                  @acknoledger
                </a>{' '}
                <a href="https://twitter.com/geek_cartel?ref_src=twsrc%5Etfw">
                  @geek_cartel
                </a>{' '}
                <a href="https://twitter.com/HoldexIo?ref_src=twsrc%5Etfw">
                  @HoldexIo
                </a>{' '}
                <a href="https://twitter.com/LBank_Exchange?ref_src=twsrc%5Etfw">
                  @LBank_Exchange
                </a>{' '}
                <a href="https://twitter.com/PeanutTrade?ref_src=twsrc%5Etfw">
                  @PeanutTrade
                </a>{' '}
                <a href="https://twitter.com/beondeck?ref_src=twsrc%5Etfw">
                  @beondeck
                </a>{' '}
                <a href="https://twitter.com/soken_team?ref_src=twsrc%5Etfw">
                  @soken_team
                </a>{' '}
                and others{' '}
                <a href="https://t.co/X92hlfyvgv">pic.twitter.com/X92hlfyvgv</a>
              </p>
              &mdash; Asan (@atulemis1){' '}
              <a href="https://twitter.com/atulemis1/status/1511115463220113409?ref_src=twsrc%5Etfw">
                April 4, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                🍣 It&#39;s a wrap 🌯 <br />
                Thanks to{' '}
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ETHDubaiConf
                </a>{' '}
                for hosting a great conference. We also want to thank everyone
                that&#39;s attended the Sushi keynote and workshop! <br />
                Special shout-out to{' '}
                <a href="https://twitter.com/sarangparikh22?ref_src=twsrc%5Etfw">
                  @sarangparikh22
                </a>{' '}
                and{' '}
                <a href="https://twitter.com/grod220?ref_src=twsrc%5Etfw">
                  @grod220
                </a>{' '}
                for representing Sushi on stage 👏🏻 <br />
                📖 More at{' '}
                <a href="https://t.co/mC5ic8YOF0">https://t.co/mC5ic8YOF0</a>
              </p>
              &mdash; SushiSwap (@SushiSwap){' '}
              <a href="https://twitter.com/SushiSwap/status/1510904515981107205?ref_src=twsrc%5Etfw">
                April 4, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                amazing to see you all at{' '}
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ETHDubaiConf
                </a>
                ,{' '}
                <a href="https://twitter.com/austingriffith?ref_src=twsrc%5Etfw">
                  @austingriffith
                </a>{' '}
                <a href="https://twitter.com/dabit3?ref_src=twsrc%5Etfw">
                  @dabit3
                </a>{' '}
                and{' '}
                <a href="https://twitter.com/kempsterrrr?ref_src=twsrc%5Etfw">
                  @kempsterrrr
                </a>{' '}
                😄 until next time✌️{' '}
                <a href="https://t.co/gE9XQp7wal">pic.twitter.com/gE9XQp7wal</a>
              </p>
              &mdash; joshcs.eth ⚓ Consensus 🇺🇦 (🧱,🚀)ᵍᵐ (@JoshCStein){' '}
              <a href="https://twitter.com/JoshCStein/status/1510398955246530560?ref_src=twsrc%5Etfw">
                April 2, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                Minted a lot of new core memories during{' '}
                <a href="https://twitter.com/hashtag/ETHDubai?src=hash&amp;ref_src=twsrc%5Etfw">
                  #ETHDubai
                </a>
                . It was an absolute blast ❤️
                <br />
                Saw some colleagues from the{' '}
                <a href="https://twitter.com/search?q=%24sushi&amp;src=ctag&amp;ref_src=twsrc%5Etfw">
                  $sushi
                </a>{' '}
                team for the first time and met loads of new smart and nice
                peeps. Thank you for being there and see ya next year 🙌🏽
              </p>
              &mdash; CHILLI 🍣 (@chillichelli){' '}
              <a href="https://twitter.com/chillichelli/status/1510707525133418508?ref_src=twsrc%5Etfw">
                April 3, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                awesome poolside conversations at{' '}
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ETHDubaiConf
                </a>
                <br />
                caught after{' '}
                <a href="https://twitter.com/saltyfacu?ref_src=twsrc%5Etfw">
                  @saltyfacu
                </a>{' '}
                and{' '}
                <a href="https://twitter.com/flashfish0x?ref_src=twsrc%5Etfw">
                  @flashfish0x
                </a>{' '}
                got away{' '}
                <a href="https://t.co/HICeFjkc5K">pic.twitter.com/HICeFjkc5K</a>
              </p>
              &mdash; JosΞph 🦇🔊 (@joseph_k89){' '}
              <a href="https://twitter.com/joseph_k89/status/1509128855516323842?ref_src=twsrc%5Etfw">
                March 30, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                The{' '}
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ETHDubaiConf
                </a>{' '}
                is truly worth a visit! Lots of stuff happening. <br />
                ☑️
                <a href="https://twitter.com/kristofgazso?ref_src=twsrc%5Etfw">
                  @kristofgazso
                </a>{' '}
                talking about smart contract wallets and EIP-4337
                <br />
                ☑️
                <a href="https://twitter.com/grod220?ref_src=twsrc%5Etfw">
                  @grod220
                </a>{' '}
                talking about doing great frontend with Web3
                <br />
                ☑️
                <a href="https://twitter.com/austingriffith?ref_src=twsrc%5Etfw">
                  @austingriffith
                </a>{' '}
                doing live coding on{' '}
                <a href="https://twitter.com/hashtag/web3?src=hash&amp;ref_src=twsrc%5Etfw">
                  #web3
                </a>{' '}
                for frontend devs
                <a href="https://twitter.com/hashtag/ETHDubai?src=hash&amp;ref_src=twsrc%5Etfw">
                  #ETHDubai
                </a>{' '}
                <a href="https://twitter.com/hashtag/EthDubaiconf?src=hash&amp;ref_src=twsrc%5Etfw">
                  #EthDubaiconf
                </a>{' '}
                <a href="https://t.co/PkXqrUK9WC">pic.twitter.com/PkXqrUK9WC</a>
              </p>
              &mdash; Igor Stadnyk (🇺🇦,🇺🇦) (@IgorStadnykINC4){' '}
              <a href="https://twitter.com/IgorStadnykINC4/status/1509529779279568918?ref_src=twsrc%5Etfw">
                March 31, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                I never expected to see a local event (in ME region) with many
                valuable ethereum community members. This is a dream come true.
              </p>
              &mdash; Mutawa.eth @ETHDubai (♢,♢) 🇪🇹🦇🔊 (@MutawaAhmed){' '}
              <a href="https://twitter.com/MutawaAhmed/status/1510553481568002050?ref_src=twsrc%5Etfw">
                April 3, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                Goodbye{' '}
                <a href="https://twitter.com/hashtag/ETHDubai?src=hash&amp;ref_src=twsrc%5Etfw">
                  #ETHDubai
                </a>
                . Met a lot of great people, learned a ton of new stuff, and
                this conference got me more excited than I already was about{' '}
                <a href="https://twitter.com/hashtag/blockchain?src=hash&amp;ref_src=twsrc%5Etfw">
                  #blockchain
                </a>{' '}
                and{' '}
                <a href="https://twitter.com/hashtag/web3?src=hash&amp;ref_src=twsrc%5Etfw">
                  #web3
                </a>
                . See ya{' '}
                <a href="https://twitter.com/EFDevconnect?ref_src=twsrc%5Etfw">
                  @EFDevconnect
                </a>{' '}
                Amsterdam in a few weeks!💻{' '}
                <a href="https://t.co/zOrpJj2Fou">pic.twitter.com/zOrpJj2Fou</a>
              </p>
              &mdash; dornex.eth (@dornex_eth){' '}
              <a href="https://twitter.com/dornex_eth/status/1510533635211988994?ref_src=twsrc%5Etfw">
                April 3, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                It’s very hard to say bye 👋 🥲
                <br />
                Dubai is definitely one of the best places to be if you want to
                network and make connections. Thanks{' '}
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ETHDubaiConf
                </a>{' '}
                for such amazing experience. 💥💥🥳{' '}
                <a href="https://t.co/Xx2gEhrmFC">pic.twitter.com/Xx2gEhrmFC</a>
              </p>
              &mdash; saiteja.eth (@0xsaiteja){' '}
              <a href="https://twitter.com/0xsaiteja/status/1510170522549694466?ref_src=twsrc%5Etfw">
                April 2, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                Had a great day in{' '}
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ETHDubaiConf
                </a>{' '}
                yatch party and after party. <br />
                Met with some really great people.{' '}
                <a href="https://twitter.com/arnau_eth?ref_src=twsrc%5Etfw">
                  @arnau_eth
                </a>{' '}
                <a href="https://twitter.com/dapplion?ref_src=twsrc%5Etfw">
                  @dapplion
                </a>{' '}
                and many others. <br />
                Here&#39;s team{' '}
                <a href="https://twitter.com/xorddotcom?ref_src=twsrc%5Etfw">
                  @xorddotcom
                </a>{' '}
                <a href="https://t.co/pME0aH6PDP">pic.twitter.com/pME0aH6PDP</a>
              </p>
              &mdash; shakeib.eth (@shakeib98){' '}
              <a href="https://twitter.com/shakeib98/status/1508900543464357888?ref_src=twsrc%5Etfw">
                March 29, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                So{' '}
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ETHDubaiConf
                </a>{' '}
                decided to get us one of the biggest party yachts in 📍Dubai
                yesterday 👾{' '}
                <a href="https://t.co/EMnZZLr53P">pic.twitter.com/EMnZZLr53P</a>
              </p>
              &mdash; harshilanand.eth 👾 (@harshilanand39){' '}
              <a href="https://twitter.com/harshilanand39/status/1509035684585263105?ref_src=twsrc%5Etfw">
                March 30, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                I had a great time with{' '}
                <a href="https://twitter.com/austingriffith?ref_src=twsrc%5Etfw">
                  @austingriffith
                </a>{' '}
                <a href="https://twitter.com/saltyfacu?ref_src=twsrc%5Etfw">
                  @saltyfacu
                </a>{' '}
                <a href="https://twitter.com/patcito?ref_src=twsrc%5Etfw">
                  @patcito
                </a>{' '}
                and others! Great to see you guys at{' '}
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ETHDubaiConf
                </a>{' '}
                <a href="https://t.co/sVKZO9vUuJ">pic.twitter.com/sVKZO9vUuJ</a>
              </p>
              &mdash; Mutawa.eth @ETHDubai (♢,♢) 🇪🇹🦇🔊 (@MutawaAhmed){' '}
              <a href="https://twitter.com/MutawaAhmed/status/1508905038130855948?ref_src=twsrc%5Etfw">
                March 29, 2022
              </a>
            </blockquote>
          </div>
        </div>

        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                When Lunchtime becomes the core hub of discussions &amp;
                conversations!💬🗣️
                <a href="https://twitter.com/hashtag/ETHDubai?src=hash&amp;ref_src=twsrc%5Etfw">
                  #ETHDubai
                </a>{' '}
                kicking off with great participation and anticipation. 🎉
                <br />
                The{' '}
                <a href="https://twitter.com/hashtag/Hypersign?src=hash&amp;ref_src=twsrc%5Etfw">
                  #Hypersign
                </a>{' '}
                team at{' '}
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ETHDubaiConf
                </a>{' '}
                with great minds and projects! 🚀💥
                <br />
                Stay tuned for more! 🦾
                <a href="https://twitter.com/hashtag/ETHDubai2022?src=hash&amp;ref_src=twsrc%5Etfw">
                  #ETHDubai2022
                </a>{' '}
                <a href="https://twitter.com/search?q=%24HID&amp;src=ctag&amp;ref_src=twsrc%5Etfw">
                  $HID
                </a>{' '}
                <a href="https://t.co/BQwtBrOcA5">pic.twitter.com/BQwtBrOcA5</a>
              </p>
              &mdash; Hypersign | $HiD (@hypersignchain){' '}
              <a href="https://twitter.com/hypersignchain/status/1509109434433552384?ref_src=twsrc%5Etfw">
                March 30, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                Making a cameo at the end of{' '}
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ETHDubaiConf
                </a>{' '}
                with the one and only{' '}
                <a href="https://twitter.com/JosephRoccisan0?ref_src=twsrc%5Etfw">
                  @JosephRoccisan0
                </a>{' '}
                and Nick from{' '}
                <a href="https://twitter.com/TokensoftInc?ref_src=twsrc%5Etfw">
                  @TokensoftInc
                </a>{' '}
                . Great people, great times, great memories in{' '}
                <a href="https://twitter.com/hashtag/Dubai?src=hash&amp;ref_src=twsrc%5Etfw">
                  #Dubai
                </a>{' '}
                🚀 <a href="https://t.co/HG6KxLgnZp">https://t.co/HG6KxLgnZp</a>
              </p>
              &mdash; Austin Piazza (@iAmAustinPiazza){' '}
              <a href="https://twitter.com/iAmAustinPiazza/status/1509823763239649317?ref_src=twsrc%5Etfw">
                April 1, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                🧑‍💻 The{' '}
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ETHDubaiConf
                </a>{' '}
                was inspiring, revealing, educative, fun and so much more!
                <br />
                🤝 Thank you, Andrei Pitiș, for joining us and being such a
                reliable partner!
                <a href="https://twitter.com/hashtag/BLAST?src=hash&amp;ref_src=twsrc%5Etfw">
                  #BLAST
                </a>{' '}
                <a href="https://twitter.com/hashtag/BwareLabs?src=hash&amp;ref_src=twsrc%5Etfw">
                  #BwareLabs
                </a>{' '}
                <a href="https://twitter.com/hashtag/ETHDubai?src=hash&amp;ref_src=twsrc%5Etfw">
                  #ETHDubai
                </a>{' '}
                <a href="https://t.co/1Y4aOMU72d">pic.twitter.com/1Y4aOMU72d</a>
              </p>
              &mdash; Bware Labs (@BwareLabs){' '}
              <a href="https://twitter.com/BwareLabs/status/1509830095908880410?ref_src=twsrc%5Etfw">
                April 1, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                <a href="https://twitter.com/graphprotocol?ref_src=twsrc%5Etfw">
                  @graphprotocol
                </a>{' '}
                serves over 1 billion queries per day - It&#39;s right at the
                centre of the Web3 ecosystem. Fascinating workshop by{' '}
                <a href="https://twitter.com/dabit3?ref_src=twsrc%5Etfw">
                  @dabit3
                </a>{' '}
                at{' '}
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ETHDubaiConf
                </a>{' '}
                <a href="https://t.co/Z03GAPgHgL">pic.twitter.com/Z03GAPgHgL</a>
              </p>
              &mdash; Jamie Hewitt (@Jamie_Hewitt_){' '}
              <a href="https://twitter.com/Jamie_Hewitt_/status/1509118256820412420?ref_src=twsrc%5Etfw">
                March 30, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                Thank you to{' '}
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ETHDubaiConf
                </a>
                ,{' '}
                <a href="https://twitter.com/patcito?ref_src=twsrc%5Etfw">
                  @patcito
                </a>
                ,{' '}
                <a href="https://twitter.com/impranavm_?ref_src=twsrc%5Etfw">
                  @impranavm_
                </a>
                ,{' '}
                <a href="https://twitter.com/developer_dao?ref_src=twsrc%5Etfw">
                  @developer_dao
                </a>
                , &amp; everyone who attended the{' '}
                <a href="https://twitter.com/graphprotocol?ref_src=twsrc%5Etfw">
                  @graphprotocol
                </a>{' '}
                workshop yesterday 🐐 <br />
                was legit blown away not only by this event, but how beautiful
                the city is
                <br />
                workshop material open sourced here:
                <a href="https://t.co/vDDl4DKShU">
                  https://t.co/vDDl4DKShU
                </a>{' '}
                <a href="https://t.co/HxHvCimsRr">pic.twitter.com/HxHvCimsRr</a>
              </p>
              &mdash; nader dabit (🧱, 🚀) | sha.eth @ Graph Day 2022 (@dabit3){' '}
              <a href="https://twitter.com/dabit3/status/1509596624196096008?ref_src=twsrc%5Etfw">
                March 31, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                Being with you all was an incredible experience.
                <br />
                All this credit goes to{' '}
                <a href="https://twitter.com/hashtag/ETHDubai?src=hash&amp;ref_src=twsrc%5Etfw">
                  #ETHDubai
                </a>
                .😍
                <a href="https://twitter.com/hashtag/Web3?src=hash&amp;ref_src=twsrc%5Etfw">
                  #Web3
                </a>{' '}
                <a href="https://twitter.com/hashtag/Blockchain?src=hash&amp;ref_src=twsrc%5Etfw">
                  #Blockchain
                </a>{' '}
                <a href="https://twitter.com/hashtag/DeFi?src=hash&amp;ref_src=twsrc%5Etfw">
                  #DeFi
                </a>{' '}
                <a href="https://twitter.com/hashtag/Metaverse?src=hash&amp;ref_src=twsrc%5Etfw">
                  #Metaverse
                </a>{' '}
                <a href="https://t.co/sgTbSWeGPy">https://t.co/sgTbSWeGPy</a>
              </p>
              &mdash; Veronica Drake (@Veronicadrakee){' '}
              <a href="https://twitter.com/Veronicadrakee/status/1509504335482265606?ref_src=twsrc%5Etfw">
                March 31, 2022
              </a>
            </blockquote>
          </div>
        </div>
        <div>
          <div className="testimonial_box">
            <Img fixed={imgs.quote.childImageSharp.fixed} />
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                Opening night of{' '}
                <a href="https://twitter.com/ETHDubaiConf?ref_src=twsrc%5Etfw">
                  @ETHDubaiConf
                </a>{' '}
                getting off to an awesome start. Really enjoyable meeting so
                many great people in the{' '}
                <a href="https://twitter.com/hashtag/crypto?src=hash&amp;ref_src=twsrc%5Etfw">
                  #crypto
                </a>{' '}
                industry{' '}
                <a href="https://t.co/LnwrH0OtCf">pic.twitter.com/LnwrH0OtCf</a>
              </p>
              &mdash; Jamie Hewitt (@Jamie_Hewitt_){' '}
              <a href="https://twitter.com/Jamie_Hewitt_/status/1508897344980496408?ref_src=twsrc%5Etfw">
                March 29, 2022
              </a>
            </blockquote>
          </div>
        </div>
      </div>
    </>
  )
}
