import React, { useState, useEffect } from 'react'
import { graphql } from 'gatsby'
import Img from 'gatsby-image'
import get from 'lodash/get'
import loadable from '@loadable/component'
import { Carousel } from 'react-bootstrap'

const Caroussel = loadable(() => import('components/caroussel'))
const CarouselPeople = loadable(() => import('components/carouselpeople'))

import Meta from 'components/meta'
import Layout from 'components/layout'
import ReactMarkdown from 'react-markdown'
import Hero from 'components/hero'

import ScheduleSection from 'components/scheduleSection'
import SpeakersSection from 'components/speakersSection'
import InlineTicketsSection from 'components/inlineTicketsSection'
import CollaboratorsSection from 'components/collaboratorsSection'
import SponsorsSection from 'components/sponsorsSection'

function ZeitLogo(props) {
  return (
    <svg width={140} viewBox="0 0 230 46" {...props}>
      <linearGradient
        id="prefix__a"
        x1="114.721%"
        x2="39.54%"
        y1="181.283%"
        y2="100%"
      >
        <stop offset={0} stopColor="#fff" />
        <stop offset={1} />
      </linearGradient>
      <g fill="none">
        <path d="M25.492 0l25.491 45.226H0z" fill="url(#prefix__a)" />
        <path
          d="M85.75 34h20.45v-3.55H90.85l15.1-21.55V5.8H86v3.55h14.85L85.75 30.9zm41.85 0h18.35v-3.55h-14.2V21.4h12.35v-3.55h-12.35v-8.5h14.2V5.8H127.6zm40.45 0h17.9v-3.55h-6.85V9.35h6.85V5.8h-17.9v3.55h6.9v21.1h-6.9zm47.35 0h4.15V9.35h9.6V5.8H205.9v3.55h9.5z"
          fill="#333"
        />
      </g>
    </svg>
  )
}

export default function IndexPage({ data, location }) {
  //TODO: separate logic into components
  const [event, setEvent] = useState(data.eventlama.events[0])
  const [faq, setFaq] = useState(null)
  const [index, setIndex] = useState(0)
  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex)
  }
  const [schedule, setSchedule] = useState(
    data.eventlama.events[0].groupedSchedule
  )
  React.useEffect(() => {
    let tg = window.top.location.hash.substr(1)
    if (tg === 'tg') {
      fetch('https://api.eventlama.com/geoip')
        .then((res) => res.json())
        .then((json) => {
          if (json.CountryCode !== 'US') {
            window.location = 'https://t.me/ETHDubai'
          }
        })
        .catch((err) => {})
    }
  }, [])
  console.log('data', data)
  //TODO: useCheckoutListener
  if (typeof window !== 'undefined') {
    window.addEventListener('message', (message) => {
      if (message.data && message.data.checkoutUrl) {
        window.location = message.data.checkoutUrl
      }
    })
  }

  return (
    <>
      <Meta site={get(data, 'site.meta')} />
      <Hero banner={data.banner.childImageSharp.fluid} />

      <section className="conference" id="conference">
        <div className="container">
          <h2>2022's Edition</h2>

          <iframe
            width="100%"
            class="ytiframe"
            src="https://www.youtube.com/embed/videoseries?list=PLVDXrfCK6ZXIn6_5fT5JsN86whDXyWbvu&amp;index=0"
            frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          ></iframe>
          <iframe
            width="100%"
            class="ytiframe"
            src="https://www.youtube.com/embed/videoseries?list=PLVDXrfCK6ZXJJ78TCxBdc0J0C3azByOGL&amp;index=0"
            frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          ></iframe>

          <div className="headings" id="header-lead">
            <Img
              className="logo"
              fluid={data.heading_logo.childImageSharp.fluid}
            />
            <h2>Conference Events</h2>
            <ReactMarkdown
              children={
                'ETHDubai is the conference for passionate devs and contributors on anything related to ' +
                ' **Ethereum, DeFi, EVM and more** with a focus on decentralization and **community projects such as Yearn and its ecosystem**. ' +
                'Expect **great speakers, talks, workshops for both experts and beginners and tons of great social events** you will not forget. ' +
                'Dubai is one of the easiest place on Earth to get to **without a visa and without a vaccine passport (only PCR test)** wherever you are from, with ' +
                'beautiful pristine sand beaches, great weather, affordable accommodation and endless entertainment for all.'
              }
            />
            <p></p>
            <ReactMarkdown
              children={
                "Last but not least, if you are a frontend developer that wants to learn web3 but don't know where to start," +
                ' we will have workshops with experts so that you can get started writing your first dapp with Solidity,' +
                ' Typescript and basic React knowledge. **We will also provide more advanced workshops for people already ' +
                'familiar with web3 technologies**. We will post more details soon so make sure to [subscribe to our mailing list ' +
                'for more details](https://docs.google.com/forms/d/e/1FAIpQLSeg8F30N_NSKbupPydQc0L9SWT60PZpcZBjiS9ToMhsTE-nlw/viewform).'
              }
            />
          </div>
        </div>
      </section>
      {
        <section
          className="peaople_said"
          id="people_said"
          style={{ backgroundColor: '#FFF' }}
        >
          <div className="container">
            <div className="headings">
              <div style={{ width: 80, margin: '0 auto' }}>
                <Img fixed={data.testimonial.childImageSharp.fixed} />
              </div>
              <h2 style={{ textAlign: 'center' }}>What People Said</h2>
              <p>What our previous attendees had to say about ETHDubai 2022.</p>
            </div>
            <div className="testimonials-slider">
              <CarouselPeople />
            </div>
          </div>
        </section>
      }

      <SpeakersSection
        speakers={event.speakers /*.sort((a, b) => (a.id < b.id ? 1 : -1))*/}
      />
      <section className="event_moments">
        <div className="container">
          <div className="headings">
            <h2>A preview of Dubai</h2>
          </div>
        </div>
        <div className="events_images d-sm-none">
          <ul>
            <li>
              <Img fluid={data.our_journey_mobile.childImageSharp.fluid} />
            </li>
          </ul>
        </div>
        <div className="events_images d-none d-sm-block">
          <Img
            className="no-animation"
            fluid={data.mosaic.childImageSharp.fluid}
          />
        </div>
      </section>

      <ScheduleSection
        schedule={schedule}
        setSchedule={setSchedule}
        event={event}
        speakers={event.speakers}
      />
      {/*
      <div style={{ display: 'none' }}>
        <TicketsSection />
      </div>
      */}

      <InlineTicketsSection event={event} />

      <SponsorsSection sponsors={event.sponsors} />
      <section
        className="support"
        id="social-events-partners"
        style={{ backgroundColor: 'white' }}
      >
        <div className="container" id="side-events">
          <div className="headings">
            <h2>2022 Edition's Social Events Partners</h2>
            <p>
              <a
                href="mailto:sponsoring@ethdubaiconf.org?subject=sponsoring ETHDubai 2022"
                className="spr-link"
                target="_blank"
              >
                <i className="fa fa-envelope"></i>&nbsp;Want to sponsor a side
                event? Let's get in touch!
              </a>
            </p>
          </div>
          <div className="supporters_logo">
            <ul>
              <li className="side-events">
                <div>
                  <div>
                    <a href="https://gton.capital/" target="_blank">
                      <Img
                        fixed={data.gton.childImageSharp.fixed}
                        title="GTON CAPITAL (𝔾ℂ) is a DAO building DeFi infrastructure and an ecosystem of products for advancing digital capital markets."
                      />
                    </a>
                  </div>
                  <div style={{ textAlign: 'center' }}>
                    <p>
                      <a href="#schedule" target="_blank">
                        GTON
                      </a>{' '}
                      - March 29th Opening Bar Night GTON Capital Party
                    </p>
                  </div>
                </div>
              </li>

              <li className="side-events">
                <div>
                  <div>
                    <a href="https://syscoin.org/" target="_blank">
                      <Img
                        fixed={data.syscoin.childImageSharp.fixed}
                        title="The best of Bitcoin, Ethereum, and ZK-Rollups, brought together in a plug-and-play network for an ultra-fast, scalable, low gas platform that is secure."
                      />
                    </a>
                  </div>
                  <div style={{ textAlign: 'center' }}>
                    <p>
                      <a href="#schedule" target="_blank">
                        Syscoin
                      </a>{' '}
                      - March 29th Yacht Party co-host
                    </p>
                  </div>
                </div>
              </li>
              <li className="side-events">
                <div>
                  <div>
                    <a href="https://www.unore.io/" target="_blank">
                      <Img
                        fixed={data.unore.childImageSharp.fixed}
                        title="Building the first reinsurance risk-trading platform in the world."
                      />
                    </a>
                  </div>
                  <div style={{ textAlign: 'center' }}>
                    <p>
                      <a href="#schedule" target="_blank">
                        Uno Re
                      </a>{' '}
                      - March 29th Yacht Party co-host
                    </p>
                  </div>
                </div>
              </li>
              <li className="side-events">
                <div>
                  <div>
                    <a href="https://ezil.me/" target="_blank">
                      <Img
                        fixed={data.ezil.childImageSharp.fixed}
                        title="Ezil.me"
                      />
                    </a>
                  </div>
                  <div style={{ textAlign: 'center' }}>
                    <p>
                      <a href="#schedule" target="_blank">
                        Ezil
                      </a>{' '}
                      - March 30th Conference Pre-Party co-host
                    </p>
                  </div>
                </div>
              </li>
              <li className="side-events">
                <div>
                  <div>
                    <a href="https://octusbridge.io" target="_blank">
                      <Img
                        fixed={data.octobridge.childImageSharp.fixed}
                        title="Ezil.me"
                      />
                    </a>
                  </div>
                  <div style={{ textAlign: 'center' }}>
                    <p>
                      <a href="https://octusbridge.io/bridge" target="_blank">
                        OctoBridge
                      </a>{' '}
                      - March 30th Conference Pre-Party co-host
                    </p>
                  </div>
                </div>
              </li>
              <li>
                <div>
                  <div>
                    <a href="https://polygon.technology/" target="_blank">
                      <Img
                        fixed={data.matic.childImageSharp.fixed}
                        title="Polygon is the leading platform for Ethereum scaling and infrastructure development. Its growing suite of products offers developers easy access to all major scaling and infrastructure solutions: L2 solutions (ZK Rollups and Optimistic Rollups), sidechains, hybrid solutions, stand-alone and enterprise chains, data availability solutions, and more. Polygon’s scaling solutions have seen widespread adoption with 7000+ applications hosted, 1B+ total transactions processed, ~100M+ unique user addresses, and $5B+ in assets secured."
                      />
                    </a>
                  </div>
                  <div style={{ textAlign: 'center' }}>
                    <p>
                      <a
                        href="https://polygon.technology/careers/#all-roles"
                        target="_blank"
                      >
                        Polygon
                      </a>{' '}
                      - March 31st Party
                    </p>
                  </div>
                </div>
              </li>
            </ul>

            <div>
              <div style={{ textAlign: 'center' }}>
                <a href="#schedule" target="_blank">
                  <Img fluid={data.lbank.childImageSharp.fluid} title="lbank" />
                </a>
              </div>

              <div style={{ textAlign: 'center', fontSize: '20px' }}>
                <p>
                  <a href="https://www.lbank.info/" target="_blank">
                    LBANK
                  </a>{' '}
                  Meetup on April 1st at 7pm
                  <strong>
                    <a href="https://defidubai.vercel.app/">
                      You need to RSVP here!
                    </a>
                  </strong>
                </p>
              </div>
            </div>

            <div>
              <div style={{ textAlign: 'center' }}>
                <a href="#schedule" target="_blank">
                  <Img
                    fluid={data.certik.childImageSharp.fluid}
                    title="certik"
                  />
                </a>
              </div>
              <div style={{ textAlign: 'center', fontSize: '20px' }}>
                <p>
                  <a href="https://www.certik.com/" target="_blank">
                    Certik
                  </a>{' '}
                  Party at Warehouse @ Le Meridien Conference Center Hotel
                  8pm-12am on March 30th{' '}
                  <strong>
                    You need a conference or workshop ticket to attend!
                  </strong>
                </p>
              </div>
            </div>
            <div>
              <div style={{ textAlign: 'center' }}>
                <a href="#schedule" target="_blank">
                  <Img
                    fluid={data.ezilParty.childImageSharp.fluid}
                    title="ezil"
                  />
                </a>
              </div>
              <div style={{ textAlign: 'center', fontSize: '20px' }}>
                <p>
                  <a
                    href="https://www.ethdubaiconf.org/#slot-2573-conference-pre-party-sponsored-by-ezil-and-octobridge"
                    target="_blank"
                  >
                    Ezil and OctoBridge
                  </a>{' '}
                  Party at Beer Garden @ Le Meridien Conference Center Hotel,
                  6pm-11pm on March 30th{' '}
                  <strong>
                    You need a conference or workshop ticket to attend!
                  </strong>
                </p>
              </div>
            </div>

            <div>
              <div style={{ textAlign: 'center' }}>
                <a href="#schedule" target="_blank">
                  <Img
                    fluid={data.gtonparty.childImageSharp.fluid}
                    title="gtonparty"
                  />
                </a>
              </div>
              <div style={{ textAlign: 'center', fontSize: '20px' }}>
                <p>
                  Opening Bar Night sponsored by{' '}
                  <a href="https://gton.capital/" target="_blank">
                    GTON
                  </a>
                  ,{' '}
                  <a href="https://comdex.one/" target="_blank">
                    Comdex
                  </a>
                  ,{' '}
                  <a href="https://bru.finance/" target="_blank">
                    Brú.Finance
                  </a>
                  , and{' '}
                  <a href="https://www.hodl.nl/" target="_blank">
                    HODL.nl
                  </a>{' '}
                  <a
                    href="https://www.eventbrite.com/e/ethdubai-opening-bar-night-party-by-gton-capital-and-partners-tickets-289564454077"
                    target="_blank"
                  >
                    Register here (only if you have a Combo ticket with GTON
                    Open Bar Party)
                  </a>{' '}
                  - Opening Bar Night GTON Capital Party (7pm-11:30pm)
                </p>
              </div>
            </div>
            <div>
              <div style={{ textAlign: 'center' }}>
                <a href="#schedule" target="_blank">
                  <Img
                    fluid={data.i1inch.childImageSharp.fluid}
                    title="1inch"
                  />
                </a>
              </div>
              <div style={{ textAlign: 'center', fontSize: '20px' }}>
                <p>
                  1inch Party 5.0 // Dubai Edition{' '}
                  <a href="https://1inch.io/" target="_blank">
                    1inch
                  </a>
                  <p>
                    <a
                      href="https://1inchpartydubai.eventbrite.co.uk/"
                      target="_blank"
                    >
                      <strong>Register here</strong>
                    </a>{' '}
                  </p>
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section
        className="support"
        id="vc-partners"
        style={{ backgroundColor: 'white' }}
      >
        <div className="container">
          <div className="headings">
            <h2>VC Partners</h2>
            <p>
              <a href="/#tickets" className="spr-link" target="_blank">
                Want to be a VC partner at the event? Get a VC pass!
              </a>
            </p>
          </div>
          <div className="supporters_logo">
            <ul>
              <li>
                <div>
                  <div>
                    <a href="https://psquare.capital/" target="_blank">
                      <Img
                        fixed={data.squarecapital.childImageSharp.fixed}
                        title="psquare capital"
                      />
                    </a>
                  </div>
                </div>
              </li>
              <li>
                <div>
                  <div>
                    <a href="https://www.owlventures.co.uk/" target="_blank">
                      <Img
                        fixed={data.owlventures.childImageSharp.fixed}
                        title="owlventures"
                      />
                    </a>
                  </div>
                </div>
              </li>
              <li style={{ marginLeft: 100 }}>
                <div>
                  <div>
                    <a href="https://www.serafund.co/" target="_blank">
                      <Img
                        fixed={data.serafund.childImageSharp.fixed}
                        title="serafund"
                      />
                    </a>
                  </div>
                </div>
              </li>
              <li style={{ marginLeft: 100 }}>
                <div>
                  <div>
                    <a href="https://3one4capital.com/" target="_blank">
                      <Img
                        fixed={data.t3one4.childImageSharp.fixed}
                        title="t3one4"
                      />
                    </a>
                  </div>
                </div>
              </li>
              <li style={{ marginLeft: 100 }}>
                <div>
                  <div>
                    <a href="https://chiron.partners/" target="_blank">
                      <Img
                        fixed={data.chiron.childImageSharp.fixed}
                        title="chiron"
                      />
                    </a>
                  </div>
                </div>
              </li>
              <li style={{ marginLeft: 100 }}>
                <div>
                  <div>
                    <a href="https://www.publishmeglobal.com/" target="_blank">
                      <Img
                        fixed={data.publishme.childImageSharp.fixed}
                        title="publishme"
                      />
                    </a>
                  </div>
                </div>
              </li>
              <li style={{ marginLeft: 100 }}>
                <div>
                  <div>
                    <a href="https://www.palisadecrypto.com/" target="_blank">
                      <Img
                        fixed={data.palisade.childImageSharp.fixed}
                        title="palisade"
                      />
                    </a>
                  </div>
                </div>
              </li>
              <li style={{ marginLeft: 100 }}>
                <div>
                  <div>
                    <a href="https://wowswap.io/" target="_blank">
                      <Img
                        fixed={data.wowswap.childImageSharp.fixed}
                        title="wowswap"
                      />
                    </a>
                  </div>
                </div>
              </li>
              <li style={{ marginLeft: 100 }}>
                <div>
                  <div>
                    <a href="https://portico.vc" target="_blank">
                      <Img
                        fixed={data.portico.childImageSharp.fixed}
                        title="portico"
                      />
                    </a>
                  </div>
                </div>
              </li>
              <li style={{ marginLeft: 100 }}>
                <div>
                  <div>
                    <a href="https://40acresdao.netlify.app/" target="_blank">
                      <Img
                        fixed={data.acresdao.childImageSharp.fixed}
                        title="acresdao"
                      />
                    </a>
                  </div>
                </div>
              </li>
              <li style={{ marginLeft: 100 }}>
                <div>
                  <div>
                    <a href="https://www.hashed.com/" target="_blank">
                      <Img
                        fixed={data.hashed.childImageSharp.fixed}
                        title="hashed"
                      />
                    </a>
                  </div>
                </div>
              </li>
              <li style={{ marginLeft: 100 }}>
                <div>
                  <div>
                    <a href="https://hypra.fund/" target="_blank">
                      <Img
                        fixed={data.hypra.childImageSharp.fixed}
                        title="hypra"
                      />
                    </a>
                  </div>
                </div>
              </li>
              <li style={{ marginLeft: 100 }}>
                <div>
                  <div>
                    <a href="https://www.genshards.com/" target="_blank">
                      <Img
                        fixed={data.genesisshard.childImageSharp.fixed}
                        title="genshards"
                      />
                    </a>
                  </div>
                </div>
              </li>
              <li style={{ marginLeft: 100 }}>
                <div>
                  <div>
                    <a href="http://momentum6.com/venture" target="_blank">
                      <Img
                        fixed={data.m6.childImageSharp.fixed}
                        title="mgenshardsm66"
                      />
                    </a>
                  </div>
                </div>
              </li>
              <li style={{ marginLeft: 100 }}>
                <div>
                  <div>
                    <a href="https://www.matrixpartners.in/" target="_blank">
                      <Img
                        fixed={data.matrixindia.childImageSharp.fixed}
                        title="matrixindia"
                      />
                    </a>
                  </div>
                </div>
              </li>
              <li style={{ marginLeft: 100 }}>
                <div>
                  <div>
                    <a href="https://silta.finance/" target="_blank">
                      <Img
                        fixed={data.sita.childImageSharp.fixed}
                        title="sita"
                      />
                    </a>
                  </div>
                </div>
              </li>
              <li style={{ marginLeft: 100 }}>
                <div>
                  <div>
                    <a href="https://criterionvc.com/" target="_blank">
                      <Img
                        fixed={data.criterion.childImageSharp.fixed}
                        title="criterion"
                      />
                    </a>
                  </div>
                </div>
              </li>
              <li style={{ marginLeft: 100 }}>
                <div>
                  <div>
                    <a href="https://alphemy.capital/" target="_blank">
                      <Img
                        fixed={data.alphemy.childImageSharp.fixed}
                        title="alphemy"
                      />
                    </a>
                  </div>
                </div>
              </li>
              <li style={{ marginLeft: 100 }}>
                <div>
                  <div>
                    <a href="https://www.crtlabs.io/" target="_blank">
                      <Img
                        fixed={data.crtlabs.childImageSharp.fixed}
                        title="crtlabs"
                      />
                    </a>
                  </div>
                </div>
              </li>
              <li style={{ marginLeft: 100 }}>
                <div>
                  <div>
                    <a href="https://www.altonomy.com/#/" target="_blank">
                      <Img
                        fixed={data.altventures.childImageSharp.fixed}
                        title="altventures"
                      />
                    </a>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </section>
      <section
        className="support"
        id="nft-droppers"
        style={{ backgroundColor: 'white', display: 'none' }}
      >
        <div className="container">
          <div className="headings">
            <h2>NFT Partners</h2>
            <p>
              <a
                href="mailto:sponsoring@ethdubaiconf.org?subject=sponsoring ETHDubai 2022"
                className="spr-link"
                target="_blank"
              >
                <i className="fa fa-envelope"></i>&nbsp;Want to drop an NFT at
                the conference? Let's get in touch!
              </a>
            </p>
          </div>
          <div className="supporters_logo">
            <ul>
              <li>
                <a href="https://epoch.hysek.swiss/" target="_blank">
                  <Img fixed={data.hysek.childImageSharp.fixed} />
                </a>
              </li>
              <li>
                <a href="https://www.niftyrocks-studios.com/" target="_blank">
                  <Img fixed={data.niftyrocks.childImageSharp.fixed} />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </section>
      <section className="support" id="supporters">
        <div className="container">
          <div className="headings">
            <h2>Supporters</h2>
          </div>
          <div className="supporters_logo">
            <ul>
              <li>
                <a href="https://eventlama.com/" target="_blank">
                  <Img fixed={data.support2.childImageSharp.fixed} />
                </a>
              </li>
              <li>
                <a href="https://www.meetup.com/defi-dubai/" target="_blank">
                  <Img fixed={data.support7.childImageSharp.fixed} />
                </a>
              </li>
              <li style={{ marginLeft: 100 }}>
                <a href="https://www.optimism.io/" target="_blank">
                  <Img fixed={data.optimism.childImageSharp.fixed} />
                </a>
              </li>
              <li style={{ marginLeft: 100 }}>
                <a href="https://nethermind.io/" target="_blank">
                  <Img fixed={data.nethermind.childImageSharp.fixed} />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </section>
      <section className="location" id="location">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="location_content">
                <div className="location_heading">
                  <Img fixed={data.location.childImageSharp.fixed} />
                  <h2>Location</h2>
                </div>

                <div className="office_address">
                  <h4>
                    Venue Address: Le Meridien Dubai Hotel &amp; Conference
                    Centre - Airport Rd - Garhoud - Dubai - United Arab Emirates
                  </h4>
                  <h3>
                    <a
                      href="https://www.marriott.com/hotels/travel/dxbmd-le-m%C3%A9ridien-dubai-hotel-and-conference-centre/?scid=bb1a189a-fec3-4d19-a255-54ba596febe2&y_source=1_Mjc4MTkyMC03MTUtbG9jYXRpb24uZ29vZ2xlX3dlYnNpdGVfb3ZlcnJpZGU%3D"
                      target="_blank"
                    >
                      Le Meridien Dubai Hotel &amp; Conference Centre
                    </a>{' '}
                    @ Dubai, UAE
                  </h3>
                  <Carousel activeIndex={index} onSelect={handleSelect}>
                    <Carousel.Item>
                      <Img fluid={data.inside2.childImageSharp.fluid} />
                    </Carousel.Item>
                    <Carousel.Item>
                      <Img fluid={data.inside3.childImageSharp.fluid} />
                    </Carousel.Item>
                    <Carousel.Item>
                      <Img fluid={data.pool.childImageSharp.fluid} />
                    </Carousel.Item>
                    <Carousel.Item>
                      <Img fluid={data.ballroom.childImageSharp.fluid} />
                    </Carousel.Item>
                    <Carousel.Item>
                      <Img fluid={data.confroom.childImageSharp.fluid} />
                    </Carousel.Item>
                    <Carousel.Item>
                      <Img fluid={data.meridien.childImageSharp.fluid} />
                    </Carousel.Item>
                    <Carousel.Item>
                      <Img fluid={data.fitness.childImageSharp.fluid} />
                    </Carousel.Item>
                  </Carousel>
                </div>

                <div className="airport"></div>
                <div className="airport train"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <CollaboratorsSection collaborators={event.collaborators} />
      <section className="subscribe_bottom">
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <div className="subscribe_img">
                <Img fluid={data.subscribe.childImageSharp.fluid} />
              </div>
            </div>
            <div className="col-md-8">
              <div className="subscribe_right" id="subscribe">
                <div className="headings">
                  <h2>Subscribe</h2>
                  <p>
                    Subscribe to our{' '}
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSeg8F30N_NSKbupPydQc0L9SWT60PZpcZBjiS9ToMhsTE-nlw/viewform">
                      mailing list to get the latest news here
                    </a>
                  </p>
                </div>
                <div className="subscibe_form d-none">
                  <button href="">Subscribe</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <footer className="site_footer">
        <div className="container">
          <div className="footer_top">
            <div className="footer_social">
              <ul>
                <li>
                  <a
                    href="https://www.facebook.com/ETHDubaiConfl/"
                    target="_blank"
                  >
                    <Img fixed={data.facebook_icon.childImageSharp.fixed} />
                  </a>
                </li>
                <li>
                  <a href="https://twitter.com/ETHDubaiConf" target="_blank">
                    <Img fixed={data.twitter_icon.childImageSharp.fixed} />
                  </a>
                </li>
                {/*<li>
                  <a
                    href="https://www.youtube.com/c/ETHDubaiConfOrgConf"
                    target="_blank"
                  >
                    <Img fixed={data.youtube_icon.childImageSharp.fixed} />
                  </a>
                </li>*/}
                <li>
                  <a href="https://medium.com/@ETHDubaiConf" target="_blank">
                    <Img fixed={data.medium_icon.childImageSharp.fixed} />
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.linkedin.com/company/ethdubai"
                    target="_blank"
                  >
                    <Img fixed={data.linkedin_icon.childImageSharp.fixed} />
                  </a>
                </li>
                <li>
                  <a href="https://lenster.xyz/u/ethdubai.lens" target="_blank">
                    <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjgiIGhlaWdodD0iMjciIHZpZXdCb3g9IjAgMCAyOCAyNyIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48cGF0aCBkPSJNMjcuMDM4IDI0LjE1NjRMMjYuNzU2NiAyNC4yODY4QzI1LjA1MjkgMjUuMDYzMiAyMy4xNDE4IDI1LjI2MDggMjEuMzE1MiAyNC44NDk0QzIwLjM0MTEgMjQuNjM1NSAxOS40MTIxIDI0LjI1MjMgMTguNTcwNSAyMy43MTcyQzIwLjIzODQgMjQuMTQ4NCAyMi4wMDcgMjMuOTQyOSAyMy41MzE2IDIzLjE0MDlMMjMuNzUxMiAyMy4wMjQyTDI0LjA0NjIgMjIuODU5NUwyMy4yMjI4IDIxLjQxMTdMMjIuOTI3NyAyMS41ODMyTDIyLjc2MzEgMjEuNjcyNEMyMS45NjAzIDIyLjA5ODcgMjEuMDYwMyAyMi4zMDgzIDIwLjE1MTggMjIuMjgwNkMxOS4yNDMzIDIyLjI1MjkgMTguMzU3NyAyMS45ODg5IDE3LjU4MjQgMjEuNTE0NkMxNi44OTQ3IDIxLjEwMzIgMTYuMzA3NSAyMC41NDM1IDE1Ljg2MzUgMTkuODc2NEMxNS40MTk1IDE5LjIwOTMgMTUuMTMwMSAxOC40NTE1IDE1LjAxNjEgMTcuNjU4M0MxOS4wMDAzIDE3LjI4OTMgMjIuNzM5OCAxNS41NzMzIDI1LjYxNzYgMTIuNzkzM0wyNS45MTk1IDEyLjQ3NzZDMjYuNDI1NiAxMS45NzMgMjYuODE3OCAxMS4zNjYgMjcuMDY5OSAxMC42OTczQzI3LjMyMiAxMC4wMjg2IDI3LjQyODEgOS4zMTM3NSAyNy4zODEgOC42MDA2N0MyNy4yNjQyIDcuMzIwMjIgMjYuNjk1IDYuMTIzMzEgMjUuNzc1NCA1LjIyNDY3QzI0Ljg3NzggNC4zMDM1OCAyMy42ODAzIDMuNzM0MDQgMjIuMzk5NCAzLjYxOTAxQzIxLjI4MjggMy41NDAzOSAyMC4xNzMzIDMuODQ4MDcgMTkuMjU2NyA0LjQ5MDQ1QzE5LjA2NiAzLjM4NjY5IDE4LjQ5NTYgMi4zODQyNSAxNy42NDQxIDEuNjU2NTJDMTYuNjU4OCAwLjgzMDE4MSAxNS40MDk5IDAuMzg0NjQ1IDE0LjEyNCAwLjQwMDgxNkMxMi44NDggMC4zODUwMDQgMTEuNjA3OSAwLjgyMjY5OSAxMC42MjQ1IDEuNjM1OTNDOS43NzcwMyAyLjM2Njk3IDkuMjA3NTMgMy4zNjc4NiA5LjAxMiA0LjQ2OTg3QzguMDk1NjUgMy44MjY5NyA2Ljk4NTg1IDMuNTE5MjIgNS44NjkyOCAzLjU5ODRDNC41ODg4MyAzLjcxNTIxIDMuMzkxOTEgNC4yODQ0OSAyLjQ5MzI4IDUuMjA0MDlDMS41NzI3IDYuMTAxMjYgMS4wMDUyNiA3LjI5OTQyIDAuODk0NDYzIDguNTgwMDlDMC44NDY1MzIgOS4yOTMyNSAwLjk1MjIwMSAxMC4wMDg0IDEuMjA0MzQgMTAuNjc3MkMxLjQ1NjQ4IDExLjM0NjEgMS44NDkyMyAxMS45NTMgMi4zNTYwMyAxMi40NTdDMi40NTIxIDEyLjU2NjggMi41NTUwMyAxMi42Njk3IDIuNjU3OTUgMTIuNzcyN0M1LjUzMzM0IDE1LjU1MjIgOS4yNzA0OSAxNy4yNjgzIDEzLjI1MjYgMTcuNjM3N0MxMy4xNCAxOC40MzE0IDEyLjg1MTEgMTkuMTg5NyAxMi40MDcgMTkuODU3QzExLjk2MjkgMjAuNTI0NCAxMS4zNzQ5IDIxLjA4MzcgMTAuNjg2MyAyMS40OTRDOS45MTIyNCAyMS45Njg0IDkuMDI3NzMgMjIuMjMyNSA4LjEyMDMxIDIyLjI2MDJDNy4yMTI4OSAyMi4yODc4IDYuMzEzOTcgMjIuMDc4MiA1LjUxMjQ1IDIxLjY1MTlMNS4zNDA5MSAyMS41NjI3TDUuMDQ1ODUgMjEuMzkxMUw0LjIyMjQzIDIyLjgzODlMNC41MjQzNSAyMy4wMDM2QzQuNTkyOTUgMjMuMDQ2NiA0LjY2Mzk3IDIzLjA4NTUgNC43MzcwNiAyMy4xMjAzQzYuMjYxNjUgMjMuOTIyMyA4LjAzMDMyIDI0LjEyNzggOS42OTgxOCAyMy42OTY3QzguODU3MjMgMjQuMjMzIDcuOTI3OTkgMjQuNjE2MyA2Ljk1MzQ2IDI0LjgyODlDNS4xMjY4NSAyNS4yNDAyIDMuMjE1NzggMjUuMDQyNiAxLjUxMiAyNC4yNjYyTDEuMjMwNjkgMjQuMTM1OEwwLjQwMDM5MSAyNS41NzY4TDAuNzM2NjU1IDI1LjczNDZDMi43OTU2OCAyNi42NzgzIDUuMTA3NjkgMjYuOTE5NCA3LjMxNzExIDI2LjQyMDhDOS41NjU2MSAyNS45MzY0IDExLjU4MDQgMjQuNjk2NSAxMy4wMjYxIDIyLjkwNzZMMTMuMjg2OSAyMi41NzEzVjI2LjczNjRIMTQuOTQ3NFYyMi41NTc2QzE1LjAyOTggMjIuNjgxMSAxNS4xMTkgMjIuNzk3OCAxNS4yMDgyIDIyLjkwNzZDMTYuNjQ1OSAyNC43MjA4IDE4LjY2NDUgMjUuOTgzMyAyMC45MjQxIDI2LjQ4MjZDMjEuNjYwNSAyNi42NDkyIDIyLjQxMjkgMjYuNzM0NCAyMy4xNjc5IDI2LjczNjRDMjQuNjY5IDI2LjczNDUgMjYuMTUxNSAyNi40MDQzIDI3LjUxMTQgMjUuNzY4OUwyNy44NDA4IDI1LjYxMTFMMjcuMDM4IDI0LjE1NjRaTTEwLjU2OTYgNi4xNTc4N0MxMC41Njk2IDYuMDU0OTQgMTAuNTY5NiA1Ljk1ODg4IDEwLjU2OTYgNS44NjI4MUMxMC41Njk2IDUuNzY2NzUgMTAuNTY5NiA1LjYwMjA3IDEwLjU2OTYgNS40Nzg1NUMxMC41NjkyIDUuMDE2MjIgMTAuNjYyMyA0LjU1ODU2IDEwLjg0MzMgNC4xMzMxQzExLjAyNDIgMy43MDc2NCAxMS4yODkzIDMuMzIzMTQgMTEuNjIyNSAzLjAwMjY5QzExLjk1NTggMi42ODIyMyAxMi4zNTA0IDIuNDMyNDYgMTIuNzgyNiAyLjI2ODM0QzEzLjIxNDggMi4xMDQyMiAxMy42NzU4IDIuMDI5MTUgMTQuMTM3OCAyLjA0NzY1QzE0LjU5OTcgMi4wMjkxNSAxNS4wNjA3IDIuMTA0MjIgMTUuNDkyOSAyLjI2ODM0QzE1LjkyNTEgMi40MzI0NiAxNi4zMTk3IDIuNjgyMjMgMTYuNjUzIDMuMDAyNjlDMTYuOTg2MiAzLjMyMzE0IDE3LjI1MTMgMy43MDc2NCAxNy40MzIzIDQuMTMzMUMxNy42MTMyIDQuNTU4NTYgMTcuNzA2MyA1LjAxNjIyIDE3LjcwNTkgNS40Nzg1NUMxNy43MDU5IDUuNjAyMDcgMTcuNzA1OSA1LjczMjQ0IDE3LjcwNTkgNS44NjI4MUMxNy43MDU5IDUuOTkzMTkgMTcuNzA1OSA2LjA2MTgxIDE3LjcwNTkgNi4xNjQ3NEwxNy42NTEgOC40MDE2N0wxOS4yMDg2IDYuNzU0ODRMMTkuNDAwOCA2LjU2MjcxTDE5LjY3NTIgNi4yODgyNEMyMC4wMDI2IDUuOTYzMiAyMC4zOTIyIDUuNzA3NTEgMjAuODIwNiA1LjUzNjQ4QzIxLjI0OTEgNS4zNjU0NCAyMS43MDc2IDUuMjgyNTYgMjIuMTY4OSA1LjI5MjgxQzIyLjYzMDEgNS4zMDMwNiAyMy4wODQ1IDUuNDA2MjQgMjMuNTA0OSA1LjU5NjE0QzIzLjkyNTMgNS43ODYwNCAyNC4zMDMyIDYuMDU4NzcgMjQuNjE1OCA2LjM5ODA0QzI0Ljk1NSA2LjcxMDY0IDI1LjIyNzggNy4wODg0NiAyNS40MTc3IDcuNTA4ODlDMjUuNjA3NiA3LjkyOTMyIDI1LjcxMDcgOC4zODM3MyAyNS43MjEgOC44NDQ5NEMyNS43MzEyIDkuMzA2MTYgMjUuNjQ4MyA5Ljc2NDcxIDI1LjQ3NzMgMTAuMTkzMkMyNS4zMDYzIDEwLjYyMTYgMjUuMDUwNiAxMS4wMTEyIDI0LjcyNTUgMTEuMzM4NkwyNC40NTc5IDExLjYxOTlDMjEuNjYxMiAxNC4yNzAzIDE4LjAxMjcgMTUuODM5MSAxNC4xNjUyIDE2LjA0NTdDMTAuMzE3MSAxNS44NDE1IDYuNjY3ODIgMTQuMjcyMyAzLjg3MjQ3IDExLjYxOTlMMy41OTgwMiAxMS4zMzg2QzMuMjcyOTcgMTEuMDExMiAzLjAxNzI5IDEwLjYyMTYgMi44NDYyNSAxMC4xOTMyQzIuNjc1MjIgOS43NjQ3MSAyLjU5MjM0IDkuMzA2MTYgMi42MDI1OSA4Ljg0NDk0QzIuNjEyODQgOC4zODM3MyAyLjcxNTk5IDcuOTI5MzIgMi45MDU4OSA3LjUwODg5QzMuMDk1NzkgNy4wODg0NiAzLjM2ODUyIDYuNzEwNjQgMy43MDc3OSA2LjM5ODA0QzQuMzg0NjIgNS42OTk4IDUuMzA4ODEgNS4yOTU2MiA2LjI4MDk3IDUuMjcyNjlDNy4xNzI5NyA1LjI4NjUgOC4wMjM1OCA1LjY1MTQgOC42NDgzIDYuMjg4MjRMOC45Mjk2NiA2LjU1NTg2TDkuMTM1NTEgNi43NzU0M0wxMC42Nzk0IDguNDAxNjdMMTAuNTY5NiA2LjE1Nzg3WiIgZmlsbD0iIzAwNTAxRSI+PC9wYXRoPjwvc3ZnPg==" />
                  </a>
                </li>
                <li>
                  <a href="https://lenstube.xyz/ethdubai.lens" target="_blank">
                    <img
                      width="24px"
                      src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNjciIGhlaWdodD0iNzMiIHZpZXdCb3g9IjAgMCA2NyA3MyIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTEyLjQwNDIgNzAuODE5NUMxMy4zMjMxIDcwLjI0MjIgMTQuMTE3OCA2OS40ODc1IDE0Ljc0MTcgNjguNTk5NUMxNS4zNjU2IDY3LjcxMTYgMTUuODA2MiA2Ni43MDgxIDE2LjAzNzkgNjUuNjQ3OUMxNi45ODEzIDYxLjU5MTUgMTkuNDkyIDU4LjA3MzUgMjMuMDIxMyA1NS44NjI1QzI2LjU1MDcgNTMuNjUxNiAzMC44MTE3IDUyLjkyNzYgMzQuODczMiA1My44NDg2QzM2LjQ0MyA1NC4yMDI3IDM4LjA4MjQgNTQuMDg1MSAzOS41ODU2IDUzLjUxMDVDNDEuMDg4OCA1Mi45MzU5IDQyLjM4ODcgNTEuOTMgNDMuMzIyIDUwLjYxOTFDNDQuMjU1MyA0OS4zMDgxIDQ0Ljc4MDQgNDcuNzUwNiA0NC44MzE1IDQ2LjE0MjFDNDQuODgyNSA0NC41MzM3IDQ0LjQ1NzIgNDIuOTQ2MSA0My42MDg5IDQxLjU3ODZDNDIuNzYwNSA0MC4yMTExIDQxLjUyNzEgMzkuMTI0OCA0MC4wNjM0IDM4LjQ1NkMzOC41OTk2IDM3Ljc4NzMgMzYuOTcxIDM3LjU2NTkgMzUuMzgxOSAzNy44MTk4QzMzLjc5MjggMzguMDczNiAzMi4zMTQxIDM4Ljc5MTQgMzEuMTMxNiAzOS44ODI5QzI5Ljk0OTEgNDAuOTc0MyAyOS4xMTU0IDQyLjM5MDkgMjguNzM1NCA0My45NTQ2QzI4LjI3OTEgNDUuOTc3NiAyNy40MjggNDcuODkwNiAyNi4yMzA5IDQ5LjU4NEMyNS4wMzM5IDUxLjI3NzQgMjMuNTE0MyA1Mi43MTc5IDIxLjc1OTUgNTMuODIyOUMyMC4wMDQ2IDU0LjkyNzkgMTguMDQ4OSA1NS42NzU3IDE2LjAwNDUgNTYuMDIzNEMxMy45NjAxIDU2LjM3MTIgMTEuODY3MSA1Ni4zMTIgOS44NDU2IDU1Ljg0OTNDOC41NDI0IDU1LjU3NTMgNy4xOTIgNTUuNjI0MSA1LjkxMjEgNTUuOTkxNkM0LjYzMjIgNTYuMzU5MSAzLjQ2MTYgNTcuMDM0MSAyLjUwMjIgNTcuOTU3N0MxLjU0MjkgNTguODgxMyAwLjgyMzk5OSA2MC4wMjU0IDAuNDA4MTk5IDYxLjI5MDVDLTAuMDA3NjAwNjkgNjIuNTU1NiAtMC4xMDc2MDUgNjMuOTAzMSAwLjExNjY5NSA2NS4yMTU3QzAuMzQwOTk1IDY2LjUyODQgMC44ODMwMDEgNjcuNzY2MiAxLjY5NTMgNjguODIxNEMyLjUwNzcgNjkuODc2NiAzLjU2NTcgNzAuNzE3IDQuNzc3MyA3MS4yNjk2QzUuOTg5IDcxLjgyMjEgNy4zMTcyOSA3Mi4wNyA4LjY0NjYgNzEuOTkxNkM5Ljk3NTk5IDcxLjkxMzEgMTEuMjY2IDcxLjUxMDggMTIuNDA0MiA3MC44MTk1WiIgZmlsbD0iIzYzNjZmMSIgZmlsbC1vcGFjaXR5PSIwLjk0Ii8+CjxwYXRoIGQ9Ik0yMS40NjgxIDIwLjYxNDRDMjAuNjg4NiAyMi4wNDI5IDIwLjM1MzYgMjMuNjcxNiAyMC41MDYyIDI1LjI5MTdDMjAuNjU4OCAyNi45MTE5IDIxLjI5MiAyOC40NDk0IDIyLjMyNDUgMjkuNzA3MkMyMy4zNTcxIDMwLjk2NSAyNC43NDE5IDMxLjg4NTYgMjYuMzAxMiAzMi4zNTA4QzI3Ljg2MDYgMzIuODE2MSAyOS41MjM0IDMyLjgwNDggMzEuMDc2MyAzMi4zMTg0QzMzLjA1NjUgMzEuNzAxOSAzNS4xMzg4IDMxLjQ4MjMgMzcuMjA0IDMxLjY3MjJDMzkuMjY5MiAzMS44NjIxIDQxLjI3NjYgMzIuNDU3NiA0My4xMTExIDMzLjQyNDlDNDQuOTQ1NiAzNC4zOTIxIDQ2LjU3MTIgMzUuNzExOSA0Ny44OTQ3IDM3LjMwODZDNDkuMjE4MiAzOC45MDUzIDUwLjIxMzQgNDAuNzQ3NSA1MC44MjM1IDQyLjcyOTZDNTEuMzA1IDQ0LjI2NjkgNTIuMjMwNSA0NS42MjczIDUzLjQ4MzQgNDYuNjM5OEM1NC43MzYzIDQ3LjY1MjMgNTYuMjYwNyA0OC4yNzE2IDU3Ljg2NDggNDguNDE5N0M1OS40Njg5IDQ4LjU2NzkgNjEuMDgxIDQ4LjIzODMgNjIuNDk4MSA0Ny40NzI0QzYzLjkxNTMgNDYuNzA2NSA2NS4wNzQzIDQ1LjUzODUgNjUuODI5MiA0NC4xMTU1QzY2LjU4NDEgNDIuNjkyNCA2Ni45MDEzIDQxLjA3NzkgNjYuNzQwNyAzOS40NzUxQzY2LjU4MDIgMzcuODcyMiA2NS45NDkyIDM2LjM1MjYgNjQuOTI3IDM1LjEwNzVDNjMuOTA0OSAzMy44NjI0IDYyLjUzNzMgMzIuOTQ3NSA2MC45OTY0IDMyLjQ3NzhDNTkuNDU1NSAzMi4wMDgyIDU3LjgxMDEgMzIuMDA0OCA1Ni4yNjczIDMyLjQ2ODFDNTIuMjg0MyAzMy42ODQzIDQ3Ljk4MTYgMzMuMjczIDQ0LjMwMTEgMzEuMzI0NEM0MC42MjA2IDI5LjM3NTcgMzcuODYyIDI2LjA0ODQgMzYuNjI5IDIyLjA3MDZDMzYuMTczNiAyMC41MDAzIDM1LjI1NzQgMTkuMTAyOCAzMy45OTg4IDE4LjA1OTFDMzIuNzQwMiAxNy4wMTU0IDMxLjE5NzMgMTYuMzczNSAyOS41Njk5IDE2LjIxNjZDMjcuOTQyNCAxNi4wNTk4IDI2LjMwNTMgMTYuMzk1MSAyNC44NzA2IDE3LjE3OTJDMjMuNDM1OCAxNy45NjMzIDIyLjI2OTUgMTkuMTYgMjEuNTIyNiAyMC42MTQ0SDIxLjQ2ODFaIiBmaWxsPSIjNjM2NmYxIiBmaWxsLW9wYWNpdHk9IjAuOTQiLz4KPHBhdGggZD0iTTIyLjgwMiA0MS42MjczQzIyLjc2MzkgNDAuNTQxNiAyMi41MDk1IDM5LjQ3NDUgMjIuMDUzOCAzOC40ODgzQzIxLjU5ODEgMzcuNTAyMSAyMC45NTAzIDM2LjYxNjggMjAuMTQ4MSAzNS44ODQxQzE3LjEwNzggMzMuMDQxNiAxNS4zMTY2IDI5LjExMDkgMTUuMTY2MSAyNC45NTE1QzE1LjAxNTYgMjAuNzkyIDE2LjUxODIgMTYuNzQyMSAxOS4zNDUyIDEzLjY4NzNDMjAuMDg1OCAxMi44OTggMjAuNjYyMSAxMS45Njk0IDIxLjA0MDYgMTAuOTU1NEMyMS40MTkxIDkuOTQxNTEgMjEuNTkyMiA4Ljg2MjQxIDIxLjU0OTkgNy43ODA5MUMyMS40ODcxIDYuNDQ5NzEgMjEuMDk5NCA1LjE1NDAxIDIwLjQyMDcgNC4wMDcxMUMxOS43NDIgMi44NjAxMSAxOC43OTI5IDEuODk2ODEgMTcuNjU2MyAxLjIwMTExQzE2LjUxOTYgMC41MDUzMDYgMTUuMjI5OSAwLjA5ODQwNTggMTMuODk5OCAwLjAxNTcwNThDMTIuNTY5NiAtMC4wNjY4OTQyIDExLjIzOTUgMC4xNzcyMDYgMTAuMDI1NCAwLjcyNjkwNkM4LjgxMTQyIDEuMjc2NjEgNy43NTAzMiAyLjExNTAxIDYuOTM0ODIgMy4xNjkxMUM2LjExOTMyIDQuMjIzMjEgNS41NzQzMyA1LjQ2MDggNS4zNDcyMyA2Ljc3NEM1LjEyMDEzIDguMDg3MiA1LjIxNzgyIDkuNDM2MTEgNS42MzE5MiAxMC43MDI4QzYuMDQ2MDIgMTEuOTY5NiA2Ljc2MzgyIDEzLjExNTcgNy43MjI4MiAxNC4wNDExQzkuMjQ3MDIgMTUuNDQ4OSAxMC40Nzg1IDE3LjE0MzUgMTEuMzQ2NyAxOS4wMjc5QzEyLjIxNDkgMjAuOTEyNCAxMi43MDI5IDIyLjk0OTYgMTIuNzgyNSAyNS4wMjI5QzEyLjg2MjEgMjcuMDk2MiAxMi41MzIgMjkuMTY0OSAxMS44MTA5IDMxLjExMDRDMTEuMDg5NyAzMy4wNTU5IDkuOTkxOTIgMzQuODQgOC41ODAyMiAzNi4zNjA1QzcuNjg1NzIgMzcuMzUwNCA3LjA0NzUyIDM4LjU0NDQgNi43MjEzMiAzOS44MzhDNi4zOTUwMiA0MS4xMzE3IDYuMzkwNzIgNDIuNDg1NSA2LjcwODcyIDQzLjc4MTJDNy4wMjY3MiA0NS4wNzY5IDcuNjU3MzIgNDYuMjc0OCA4LjU0NTQyIDQ3LjI3MDRDOS40MzM1MiA0OC4yNjYgMTAuNTUyMSA0OS4wMjg4IDExLjgwMzIgNDkuNDkyMkMxMy4wNTQzIDQ5Ljk1NTUgMTQuMzk5OCA1MC4xMDUyIDE1LjcyMjIgNDkuOTI4M0MxNy4wNDQ1IDQ5Ljc1MTMgMTguMzAzMyA0OS4yNTMgMTkuMzg4NiA0OC40NzY5QzIwLjQ3MzggNDcuNzAwOSAyMS4zNTIzIDQ2LjY3MDkgMjEuOTQ3NCA0NS40NzY4QzIyLjU0MjQgNDQuMjgyNyAyMi44MzU5IDQyLjk2MSAyMi44MDIgNDEuNjI3M1oiIGZpbGw9IiM2MzY2ZjEiIGZpbGwtb3BhY2l0eT0iMC45NCIvPgo8L3N2Zz4K"
                    />
                  </a>
                </li>
              </ul>
            </div>
            <div className="footer_nav">
              <ul>
                <li>
                  <a className="nav-link" href="#conference">
                    Events
                  </a>
                </li>
                <li>
                  <a className="nav-link" href="#speakers">
                    Speakers
                  </a>
                </li>
                <li>
                  <a className="nav-link" href="#schedule">
                    Schedule
                  </a>
                </li>
                <li>
                  <a className="nav-link" href="#sprs">
                    Sponsors
                  </a>
                </li>
                <li>
                  <a className="nav-link" href="#location">
                    Location
                  </a>
                </li>
                <li>
                  <a className="nav-link" href="#people-behind">
                    Organizers
                  </a>
                </li>
                <li>
                  <a
                    className="nav-link"
                    href="https://medium.com/@ETHDubai"
                    target="_blank"
                  >
                    Blog
                  </a>
                </li>
                {/*<li>
                  <a
                    className="nav-link"
                    href="https://discord.gg/TS87qg2Ema"
                    target="_blank"
                  >
                    Discord
                  </a>
              </li>
                <li>
                  <a
                    className="nav-link"
                    href="https://t.me/ETHDubai"
                    target="_blank"
                  >
                    Telegram
                  </a>
                </li>{' '}
*/}
                <li>
                  <a
                    className="nav-link"
                    href="mailto:sponsoring@ethdubaiconf.org?subject=ETHDubai 2022"
                  >
                    Contact
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-sm-8">
              <h4>FAQ</h4>
              <div className="accordion" id="accordionExample">
                <div className="card">
                  <div className="card-header" id="headingOne">
                    <h2 className="mb-0">
                      <button
                        id="why-attend"
                        className="btn btn-link"
                        type="button"
                        data-toggle="collapse"
                        data-target="#collapseOne"
                        aria-expanded="true"
                        aria-controls="collapseOne"
                        onClick={() => setFaq(faq === 0 ? null : 0)}
                      >
                        Why attend?
                      </button>
                    </h2>
                  </div>

                  <div
                    id="collapseOne"
                    className={faq === 0 ? 'collapse show' : 'collapse'}
                    aria-labelledby="headingOne"
                    data-parent="#accordionExample"
                  >
                    <div className="card-body">
                      <div className="panel-body-faq">
                        <h3 className="panel-body-title">The Sessions</h3>
                        <ul>
                          <li>Inspiring Keynotes</li>
                          <li>Expert Panel Discussions</li>
                          <li>New Announcements</li>
                          <li>Hands-On Workshops</li>
                        </ul>
                        <h3 className="panel-body-title">The People</h3>
                        <ul>
                          <li>
                            <b>Exchange</b> best practices with peers.
                          </li>
                          <li>
                            <b>Interact</b> with our amazing speakers.
                          </li>
                          <li>
                            <b>Network</b> and meet new people.
                          </li>
                          <li>
                            <b>Engage</b> experts in their respective fields.
                          </li>
                        </ul>
                        <h3 className="panel-body-title">The Topics</h3>
                        <ul>
                          <li>Authoring Web3 dApps</li>
                          <li>Cutting-edge smart contracts apps techniques</li>
                          <li>
                            Improving security and decentralization in your apps
                          </li>
                          <li>Querying your data theGraph and other systems</li>
                          <li>
                            Learn about how NFTs, Defi, Web3 and smart contract
                            in general on Ethereum, layer 2 and EVM compatible
                            chains.
                          </li>
                          <li>Understanding design methods</li>
                          <li>Smart tools for creation</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="card">
                  <div className="card-header" id="headingTwo">
                    <h2 className="mb-0">
                      <button
                        className="btn btn-link collapsed"
                        type="button"
                        data-toggle="collapse"
                        data-target="#collapseTwo"
                        aria-expanded="false"
                        aria-controls="collapseTwo"
                        onClick={() => setFaq(faq === 1 ? null : 1)}
                      >
                        Do you do refunds?
                      </button>
                    </h2>
                  </div>
                  <div
                    id="collapseTwo"
                    className={faq === 1 ? 'collapse show' : 'collapse'}
                    aria-labelledby="headingTwo"
                    data-parent="#accordionExample"
                  >
                    <div className="card-body">
                      <h3 className="panel-body-title">Ticket Refund</h3>
                      <p>
                        We do full refund upon request until December, 15th
                        2021, then we will charge a 15% fee until March 10.
                        After March 20 we will do no more refunds.
                      </p>
                      <p>
                        In case of a lockdown or travel ban related to covid-19,
                        we will do a full refund regardless of the date or a
                        ticket transfer to next year's edition.
                      </p>
                      <h3 className="panel-body-title">Ticket Transfer</h3>
                      <p>It is possible to transfer your ticket at anytime.</p>
                    </div>
                  </div>
                </div>
                <div className="card">
                  <div className="card-header" id="headingThree">
                    <h2 className="mb-0">
                      <button
                        className="btn btn-link collapsed"
                        type="button"
                        data-toggle="collapse"
                        data-target="#collapseThree"
                        aria-expanded="false"
                        aria-controls="collapseThree"
                        onClick={() => setFaq(faq === 2 ? null : 2)}
                      >
                        What do I get for each conference ticket?
                      </button>
                    </h2>
                  </div>
                  <div
                    id="collapseThree"
                    aria-labelledby="headingThree"
                    data-parent="#accordionExample"
                    className={faq === 2 ? 'collapse show' : 'collapse'}
                  >
                    <div className="card-body">
                      Full access to the talks on the 31st for regular
                      conference ticket and both workshops and talks for Full
                      Access tickets on March 29-31st.
                    </div>
                  </div>
                </div>
                <div className="card">
                  <div className="card-header" id="headingThree">
                    <h2 className="mb-0">
                      <button
                        className="btn btn-link collapsed"
                        type="button"
                        data-toggle="collapse"
                        data-target="#collapseThree"
                        aria-expanded="false"
                        aria-controls="collapseThree"
                        onClick={() => setFaq(faq === 3 ? null : 3)}
                      >
                        What are your covid policies?
                      </button>
                    </h2>
                  </div>
                  <div
                    id="collapseThree"
                    aria-labelledby="headingThree"
                    data-parent="#accordionExample"
                    className={faq === 3 ? 'collapse show' : 'collapse'}
                  >
                    <div className="card-body">
                      <p>
                        As of today, you will need a negative PCR test to enter
                        the country and you will be required by law to wear a
                        mask inside the venue. We will notify you by mail and
                        update the website if the situation changes. In case of
                        event cancellation or travel ban, you will get a full
                        refund or the possibility to transfer your ticket to
                        next year's edition.
                      </p>
                      <p>
                        During the conference, we will take all necessary
                        measures to keep the venue clean and sanitized as per
                        local requirements. We will also be providing sanitizer
                        dispenser stations inside the venue.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-4 coc-diversity">
              <h4 className="footer-title">Code of Conduct</h4>
              <p>
                Yes, we want everyone to have a safe, productive, enjoyable time
                at the conference.{' '}
                <a href="/coc.html" target="_blank">
                  Here's the Code of Conduct for ETHDubai
                </a>
                .
              </p>
              <h4 className="footer-title">Scholarship program</h4>
              <p>
                You can apply for a diversity scholarship that will give you
                free access to the conference, learn more about how to apply{' '}
                <a href="https://bit.ly/ethdubai-scholarship" target="_blank">
                  here
                </a>
                .
              </p>
            </div>
          </div>
        </div>
        <div className="footer_bottom">
          <div className="container">
            <p>
              Copyrights © 2021 - Design by{' '}
              <a href="https://eventlama.com" target="_blank">
                EventLama
              </a>{' '}
              | Credits for 2 pictures under CC license{' '}
              <a
                href="https://www.flickr.com/photos/twohungrydudes/5380195330"
                target="_blank"
              >
                1
              </a>{' '}
              and{' '}
              <a
                href="https://www.flickr.com/photos/stuckincustoms/19708859789"
                target="_blank"
              >
                2
              </a>{' '}
              | Schedule is subject to change and not final
            </p>
          </div>
        </div>
      </footer>
    </>
  )
}

export const pageQuery = graphql`
  {
    site {
      meta: siteMetadata {
        title
        description
        url: siteUrl
        author
        twitter
      }
    }
    eventlama {
      events(slug: "ethdubai") {
        id
        description
        websiteUrl
        name
        venueName
        venueCountry
        venueCity
        cocUrl
        twitterHandle
        offset
        startDate
        endDate
        timezoneId
        slug
        tickets {
          id
          price
          priceWithVat
          priceWithoutVat
          eventId
          maxPerOrder
          customEventEndDate
          customEventStartDate
          customEventVenueCity
          allowStudentDiscount
          allowTwitterDiscount
          busy
          childrenIds
          customEventVenueAddress
          customEventVenueCountry
          customEventVenueLatitude
          customEventVenueLongitude
          customEventVenueName
          customEventVenuePostalCode
          description
          displayOrder
          endDate
          geoRestriction
          githubDiscountsEnabled
          iconType
          includeVat
          isExclusive
          isForCustomEvent
          isSponsor
          name
          parents
          private
          quantity
          quantityLeft
          showDaysLeft
          showTicketsBeforeStart
          showTicketsLeft
          showTicketsPriceBeforeStart
          showVat
          soldOut
          sponsorType
          startDate
          studentDiscountPercentage
          studentDiscountQuantity
          twitterDiscountPercentage
          twitterDiscountQuantity
        }
        collaborators {
          id
          firstName
          lastName
          twitter
          github
          url
          role
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
          avatarUrl
        }
        speakers {
          id
          name
          twitter
          github
          avatarUrl
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
                ...GatsbyImageSharpFluid
              }
            }
          }
          bio
          shortBio
          talks {
            id
            title
            type
            description
            length
            startDate
          }
        }
        groupedSchedule {
          title
          date
          slots {
            id
            title
            likes
            description
            length
            startDate
            youtubeUrl
            youtubeId
            tags
            type
            room
            talk
            keynote
            speakers {
              id
              name
              twitter
              github
              localFile {
                childImageSharp {
                  fluid {
                    ...GatsbyImageSharpFluid_withWebp
                  }
                }
              }
              avatarUrl
              bio
              shortBio
            }
          }
        }
        sponsors {
          diamond {
            id
            name
            description
            url
            logoUrl
            localFile {
              childImageSharp {
                fluid(maxHeight: 100) {
                  ...GatsbyImageSharpFluid_withWebp
                }
                fixed(width: 236) {
                  ...GatsbyImageSharpFixed_withWebp
                }
              }
            }
            jobUrl
          }
          platinum {
            id
            name
            description
            url
            logoUrl
            localFile {
              childImageSharp {
                fluid(maxHeight: 100) {
                  ...GatsbyImageSharpFluid_withWebp
                }
                fixed(width: 236) {
                  ...GatsbyImageSharpFixed_withWebp
                }
              }
            }
            jobUrl
          }
          gold {
            id
            name
            description
            url
            logoUrl
            localFile {
              childImageSharp {
                fluid(maxHeight: 100) {
                  ...GatsbyImageSharpFluid_withWebp
                }
                fixed(width: 236) {
                  ...GatsbyImageSharpFixed_withWebp
                }
              }
            }
            jobUrl
          }
          silver {
            id
            name
            description
            url
            logoUrl
            localFile {
              childImageSharp {
                fluid(maxWidth: 200, maxHeight: 200) {
                  ...GatsbyImageSharpFluid_withWebp
                }
                fixed(width: 160) {
                  ...GatsbyImageSharpFixed_withWebp
                }
              }
            }
            jobUrl
          }
          bronze {
            id
            name
            description
            url
            logoUrl
            localFile {
              childImageSharp {
                fluid(maxWidth: 200, maxHeight: 200) {
                  ...GatsbyImageSharpFluid_withWebp
                }
                fixed(width: 160) {
                  ...GatsbyImageSharpFixed_withWebp
                }
              }
            }
            jobUrl
          }
          partner {
            id
            name
            description
            url
            logoUrl
            localFile {
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
            jobUrl
          }
        }
      }
    }
    banner: file(relativePath: { eq: "banner-image.png" }) {
      childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    heading_logo: file(relativePath: { eq: "ethdubailogo.png" }) {
      childImageSharp {
        fluid(maxWidth: 200) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    testimonial: file(relativePath: { eq: "testimonial.png" }) {
      childImageSharp {
        fixed(width: 80) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    mosaic: file(relativePath: { eq: "mosaic.png" }) {
      childImageSharp {
        fluid(maxWidth: 2000) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    our_journey_mobile: file(relativePath: { eq: "mosaic.png" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    support2: file(relativePath: { eq: "eventlama.png" }) {
      childImageSharp {
        fixed(width: 80) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    nethermind: file(relativePath: { eq: "nethermind.png" }) {
      childImageSharp {
        fixed(width: 200) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }

    hysek: file(relativePath: { eq: "hyseksquare.png" }) {
      childImageSharp {
        fixed(width: 120) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    niftyrocks: file(relativePath: { eq: "niftyrocks.png" }) {
      childImageSharp {
        fixed(width: 120) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    gtonparty: file(relativePath: { eq: "gtonparty.png" }) {
      childImageSharp {
        fluid(maxWidth: 620) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    lbank: file(relativePath: { eq: "lbank.png" }) {
      childImageSharp {
        fluid(maxWidth: 320) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    i1inch: file(relativePath: { eq: "i1inch.png" }) {
      childImageSharp {
        fluid(maxWidth: 320) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    certik: file(relativePath: { eq: "certik.jpeg" }) {
      childImageSharp {
        fluid(maxWidth: 920) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    ezilParty: file(relativePath: { eq: "ezilParty.png" }) {
      childImageSharp {
        fluid(maxWidth: 920) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    matic: file(relativePath: { eq: "matic-token-icon.png" }) {
      childImageSharp {
        fixed(width: 120) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    syscoin: file(relativePath: { eq: "syscoin.png" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    octobridge: file(relativePath: { eq: "octobridge.png" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    ezil: file(relativePath: { eq: "ezil.png" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    gton: file(relativePath: { eq: "gton.png" }) {
      childImageSharp {
        fixed(width: 200) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    unore: file(relativePath: { eq: "unorere.png" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }

    squarecapital: file(relativePath: { eq: "squarecapital.jpeg" }) {
      childImageSharp {
        fixed(width: 120) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    matrixindia: file(relativePath: { eq: "matrixindia.png" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    owlventures: file(relativePath: { eq: "owlventures.jpeg" }) {
      childImageSharp {
        fixed(width: 120) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    serafund: file(relativePath: { eq: "serafund.png" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    t3one4: file(relativePath: { eq: "t3one4.png" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    chiron: file(relativePath: { eq: "chiron.png" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    publishme: file(relativePath: { eq: "publishme.png" }) {
      childImageSharp {
        fixed(width: 120) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    comdex: file(relativePath: { eq: "comdex.png" }) {
      childImageSharp {
        fixed(width: 120) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    altventures: file(relativePath: { eq: "altventures.jpeg" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    portico: file(relativePath: { eq: "portico.png" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    acresdao: file(relativePath: { eq: "40acres.jpeg" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    hashed: file(relativePath: { eq: "hashed.png" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    hypra: file(relativePath: { eq: "hypra.jpg" }) {
      childImageSharp {
        fixed(width: 180) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    wowswap: file(relativePath: { eq: "wowswap.png" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    palisade: file(relativePath: { eq: "palisade.jpg" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    genesisshard: file(relativePath: { eq: "genesisshard.png" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    m6: file(relativePath: { eq: "m6.png" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    sita: file(relativePath: { eq: "sita.png" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }

    criterion: file(relativePath: { eq: "criterion.png" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }

    alphemy: file(relativePath: { eq: "alphemy.png" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }

    crtlabs: file(relativePath: { eq: "crtlabs.png" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }

    support6: file(relativePath: { eq: "expo.png" }) {
      childImageSharp {
        fixed(width: 120) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    support7: file(relativePath: { eq: "defidubai.png" }) {
      childImageSharp {
        fixed(width: 120) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    immunefi: file(relativePath: { eq: "immunefi.png" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    optimism: file(relativePath: { eq: "optimism.png" }) {
      childImageSharp {
        fixed(width: 220) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    finalForm: file(relativePath: { eq: "final-form.png" }) {
      childImageSharp {
        fixed(width: 140) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    mobx: file(relativePath: { eq: "mobx.png" }) {
      childImageSharp {
        fixed(width: 100) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    facebook: file(relativePath: { eq: "fb.png" }) {
      childImageSharp {
        fixed(height: 50) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }

    location: file(relativePath: { eq: "location.png" }) {
      childImageSharp {
        fixed(width: 80, height: 80) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    people: file(relativePath: { eq: "people.png" }) {
      childImageSharp {
        fixed(width: 80, height: 80) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    location_map: file(relativePath: { eq: "map-montreuil.png" }) {
      childImageSharp {
        fluid(maxWidth: 500) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    image003: file(relativePath: { eq: "image003.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    pb1: file(relativePath: { eq: "pb1.png" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    pb2: file(relativePath: { eq: "pb2.png" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    pb3: file(relativePath: { eq: "pb3.png" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    pb4: file(relativePath: { eq: "pb4.png" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    subscribe: file(relativePath: { eq: "subscribe_img.png" }) {
      childImageSharp {
        fluid(maxWidth: 500) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    facebook_icon: file(relativePath: { eq: "facebook.png" }) {
      childImageSharp {
        fixed(height: 24) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    twitter_icon: file(relativePath: { eq: "twitter.png" }) {
      childImageSharp {
        fixed(height: 24) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    youtube_icon: file(relativePath: { eq: "youtube.png" }) {
      childImageSharp {
        fixed(height: 24) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    medium_icon: file(relativePath: { eq: "m.png" }) {
      childImageSharp {
        fixed(height: 24) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    linkedin_icon: file(relativePath: { eq: "linkedin.png" }) {
      childImageSharp {
        fixed(height: 24) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }

    pool: file(relativePath: { eq: "pool.png" }) {
      childImageSharp {
        fluid(maxWidth: 1600) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    inside: file(relativePath: { eq: "inside.png" }) {
      childImageSharp {
        fluid(maxWidth: 1600) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    meridien: file(relativePath: { eq: "meridien.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1600) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    inside2: file(relativePath: { eq: "inside2.webp" }) {
      childImageSharp {
        fluid(maxWidth: 1600) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    inside3: file(relativePath: { eq: "inside3.webp" }) {
      childImageSharp {
        fluid(maxWidth: 1600) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    ballroom: file(relativePath: { eq: "ballroom.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1600) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    confroom: file(relativePath: { eq: "confroom.webp" }) {
      childImageSharp {
        fluid(maxWidth: 1600) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    fitness: file(relativePath: { eq: "fitness.webp" }) {
      childImageSharp {
        fluid(maxWidth: 1600) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`
